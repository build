
VERSION=0.1
PACKAGE=build

PODIR=./po
POTFILE=$(PODIR)/$(PACKAGE).pot
POFILES=$(wildcard $(PODIR)/*.po)
MOFILES=$(patsubst $(PODIR)/%.po,$(PODIR)/locale/%/LC_MESSAGES/$(PACKAGE).mo,$(POFILES))

all: src/parse.c src/scan.c build $(MOFILES)

install:
	@install -m 0755 ./build /usr/local/bin

CPPFLAGS=-DENABLE_NLS=0 -DPACKAGE="\"$(PACKAGE)\"" -DLOCALEDIR="\"./po/locale\"" \
	 -D_POSIX_C_SOURCE=200809L -D_DEFAULT_SOURCE=1 -D_FORTIFY_SOURCE=3

CFLAGS= -fstrict-aliasing -fstrict-overflow \
	-pipe -fshort-enums -fstrict-aliasing \
#	-std=gnu11 -ggdb3 -g3 -Og # -fanalyzer #-pg -Og -march=athlon64

LDFLAGS=-lpthread #-lgdbm

WARN_FLAGS=				\
	-Wall				\
	-Wextra				\
	-Wpedantic			\
	-Warith-conversion		\
	-Wdate-time			\
	-Wdisabled-optimization		\
	-Wdouble-promotion		\
	-Wduplicated-cond		\
	-Wformat-signedness		\
	-Winit-self			\
	-Winvalid-pch			\
	-Wlogical-op			\
	-Wmissing-declarations		\
	-Wmissing-include-dirs		\
	-Wmissing-prototypes		\
	-Wnested-externs		\
	-Wnull-dereference		\
	-Wold-style-definition		\
	-Wopenmp-simd			\
	-Wpacked			\
	-Wpointer-arith			\
	-Wstrict-prototypes		\
	-Wsuggest-final-methods		\
	-Wsuggest-final-types		\
	-Wtrampolines			\
	-Wuninitialized			\
	-Wunknown-pragmas		\
	-Wvariadic-macros		\
	-Wvector-operation-performance	\
	-Wwrite-strings			\
	-Warray-bounds=1		\
	-Wattribute-alias=2		\
	-Wimplicit-fallthrough=2	\
	-Wshift-overflow=2		\
	-Wvla-larger-than=4031		\
	-Wredundant-decls		\
	-Wshadow			\
	-Wenum-conversion		\
	-Wsign-conversion		\
	-Wconversion			\
	-Wstrict-prototypes		\
	-Wmissing-prototypes		\
	-Wwrite-strings			\
	-Wabi=2				\
	-Wmissing-attributes		\
	-Wmissing-braces		\
	-Wunused-const-variable=2	\
	-Wuninitialized			\
	-Wstrict-overflow=5		\
	-Wstring-compare		\
	-Wsuggest-attribute=const	\
	-Wsuggest-attribute=noreturn	\
	-Wmissing-noreturn		\
	-Wsuggest-attribute=format	\
	-Wsuggest-attribute=cold	\
	-Wmissing-format-attribute	\
	-Wsuggest-attribute=malloc	\
	-Wsuggest-attribute=pure	\
	-Wno-unused-parameter		\
	-Wcast-qual			\
	-Wno-format -fdiagnostics-color=always
#	-Wformat=2			\
	-Wformat-truncation=2

WARN=					\
-Wbool-compare				\
-Wbool-operation			\
-Wbuiltin-declaration-mismatch		\
-Wbuiltin-macro-redefined		\
-Wc11-c2x-compat			\
-Wcast-align				\
-Wcast-align=strict			\
-Wcast-function-type			\
-Wcast-qual				\
-Wchar-subscripts			\
-Wclobbered				\
-Wcomment				\
-Wconversion				\
-Wcoverage-mismatch			\
-Wcpp					\
-Wdangling-else				\
-Wdate-time				\
-Wdeprecated				\
-Wdeprecated-declarations		\
-Wdesignated-init			\
-Wdisabled-optimization			\
-Wdiscarded-array-qualifiers		\
-Wdiscarded-qualifiers			\
-Wdiv-by-zero				\
-Wdouble-promotion			\
-Wduplicated-branches			\
-Wduplicated-cond			\
-Wempty-body				\
-Wendif-labels				\
-Wenum-compare				\
-Wenum-conversion			\
-Wexpansion-to-defined			\
-Wfatal-errors				\
-Wfloat-conversion			\
-Wfloat-equal				\
-Wformat				\
-Wformat=2				\
-Wformat-contains-nul			\
-Wformat-extra-args			\
-Wformat-nonliteral			\
-Wformat-overflow=2			\
-Wformat-security			\
-Wformat-signedness			\
-Wformat-truncation=2			\
-Wformat-y2k				\
-Wframe-address				\
-Wno-frame-larger-than			\
-Wfree-nonheap-object			\
-Wif-not-aligned			\
-Wignored-attributes			\
-Wignored-qualifiers			\
-Wincompatible-pointer-types		\
-Wimplicit				\
-Wimplicit-fallthrough			\
-Wimplicit-fallthrough=2		\
-Wimplicit-function-declaration		\
-Wimplicit-int				\
-Winfinite-recursion			\
-Winit-self				\
-Wno-inline				\
-Wint-conversion			\
-Wint-in-bool-context			\
-Wint-to-pointer-cast			\
-Winvalid-memory-model			\
-Winvalid-pch				\
-Wjump-misses-init			\
-Wno-larger-than			\
-Wlogical-not-parentheses		\
-Wlogical-op				\
-Wlong-long				\
-Wlto-type-mismatch			\
-Wmain					\
-Wmaybe-uninitialized			\
-Wmemset-elt-size			\
-Wmemset-transposed-args		\
-Wmisleading-indentation		\
-Wmissing-attributes			\
-Wmissing-braces			\
-Wmissing-field-initializers		\
-Wmissing-format-attribute		\
-Wmissing-include-dirs			\
-Wmissing-noreturn			\
-Wmissing-profile			\
-Wmultichar				\
-Wmultistatement-macros			\
-Wnonnull				\
-Wnonnull-compare			\
-Wnull-dereference			\
-Wodr					\
-Wopenacc-parallelism			\
-Wopenmp-simd				\
-Woverflow				\
-Woverlength-strings			\
-Woverride-init-side-effects		\
-Wpacked				\
-Wpacked-bitfield-compat		\
-Wpacked-not-aligned			\
-Wno-padded				\
-Wparentheses				\
-Wpointer-arith				\
-Wpointer-compare			\
-Wpointer-to-int-cast			\
-Wpragmas				\
-Wprio-ctor-dtor			\
-Wno-redundant-decls			\
-Wrestrict				\
-Wreturn-local-addr			\
-Wreturn-type				\
-Wscalar-storage-order			\
-Wsequence-point			\
-Wshadow				\
-Wshadow=global				\
-Wshadow=local				\
-Wshadow=compatible-local		\
-Wshift-count-negative			\
-Wshift-count-overflow			\
-Wshift-negative-value			\
-Wshift-overflow			\
-Wshift-overflow=2			\
-Wsign-compare				\
-Wsign-conversion			\
-Wsizeof-array-argument			\
-Wsizeof-array-div			\
-Wsizeof-pointer-div			\
-Wsizeof-pointer-memaccess		\
-Wstack-protector			\
-Wno-stack-usage			\
-Wstrict-aliasing			\
-Wstrict-aliasing=3			\
-Wstrict-overflow			\
-Wstrict-overflow=2			\
-Wstring-compare			\
-Wstringop-overflow			\
-Wstringop-overread			\
-Wstringop-truncation			\
-Wswitch				\
-Wswitch-bool				\
-Wswitch-default			\
-Wswitch-enum				\
-Wswitch-outside-range			\
-Wswitch-unreachable			\
-Wsync-nand				\
-Wno-system-headers			\
-Wtautological-compare			\
-Wtrampolines				\
-Wtrigraphs				\
-Wtrivial-auto-var-init			\
-Wtsan					\
-Wtype-limits				\
-Wundef					\
-Wuninitialized				\
-Wunknown-pragmas			\
-Wno-unsuffixed-float-constants		\
-Wunused				\
-Wunused-but-set-parameter		\
-Wunused-but-set-variable		\
-Wunused-const-variable			\
-Wunused-const-variable=2		\
-Wunused-function			\
-Wunused-label				\
-Wunused-local-typedefs			\
-Wno-unused-macros			\
-Wno-unused-parameter			\
-Wunused-result				\
-Wunused-value				\
-Wunused-variable			\
-Wvarargs				\
-Wvariadic-macros			\
-Wvector-operation-performance		\
-Wvla					\
-Wvolatile-register-var			\
-Wwrite-strings				\
-Wzero-length-bounds			\
-Wconversion

# TODO -Wno-unused-parameter

SANITIZE=-fsanitize=undefined \
	-fsanitize=return \
	-fsanitize=alignment \
	-fsanitize=nonnull-attribute \
	-fsanitize=returns-nonnull-attribute \
	-fsanitize=bool \
	-fsanitize=null \
	-fsanitize=shift \
	-fsanitize=bounds \
	-fsanitize=signed-integer-overflow \
	-fsanitize=enum \
	-fsanitize=unreachable \
	-fsanitize=float-cast-overflow \
	-fsanitize=float-divide-by-zero \
	-fsanitize=object-size \
	-fsanitize=vla-bound \
	-fsanitize=integer-divide-by-zero \
	-fsanitize=pointer-overflow \
	-fsanitize=vptr \
	-fsanitize=address

YY_SOURCES=src/parse.c \
	   src/scan.c
#	   src/sh-parse.c \
	   src/sh-scan.c

SOURCES=src/build.c \
	src/diagnostic.c \
	src/hash.c \
	src/thread.c \
	src/uniqstr.c \
	src/job-server.c \
	src/graph.c \
	src/cli-option-parser.c \
	src/yy.c \
	src/utils.c \
	src/libquote/quotearg.c \
	src/libquote/localcharset.c \
	src/libquote/system-quote.c \
	src/libquote/sh-quote.c \
#        src/parse-command.c

#	src/sh.c \


#src/watchdog.c \

#LIBS=src/libquote/libquote.a #\
     src/blake3/libblake3.a

#	src/blake3/blake3.c \
	src/blake3/blake3_dispatch.c \
	src/blake3/blake3_portable.c \
	src/libquote/quotearg.c \
	src/libquote/localcharset.c \

#src/dep-scan.c


#BLAKE3_SOURCES=BLAKE3/c/blake3.c \
	       BLAKE3/c/blake3_dispatch.c \
	       BLAKE3/c/blake3_portable.c \
	       BLAKE3/c/blake3_sse2_x86-64_unix.S \
	       BLAKE3/c/blake3_sse41_x86-64_unix.S \
	       BLAKE3/c/blake3_avx2_x86-64_unix.S \
	       BLAKE3/c/blake3_avx512_x86-64_unix.S

#BLAKE3_OBJECTS=$(BLAKE3_SOURCES:.c=.o) $(BLAKE3_SOURCES:.S=.o)

#-include $(wildcard BLAKE3/c/*.d)

#blake3.a: $(BLAKE3_OBJECTS)
#	@printf "\tAR\t%s\n" $@
#	@ar rcs blake3.a $(BLAKE3_OBJECTS)

OBJECTS=$(SOURCES:.c=.o) $(YY_SOURCES:.c=.o) $(LIBS)

src/libquote/libquote.a:
	make -C src/libquote

#src/blake3/libblake3.a:
#	make -C src/blake3

src/parse.c: src/parse.y
	@printf "\tYACC\t%s\n" $@
	@bison $<

src/sh-parse.c: src/sh-parse.y
	@printf "\tYACC\t%s\n" $@
	@bison $<

src/scan.c: src/scan.l
	@printf "\tLEX\t%s\n" $@
	@flex $<

src/sh-scan.c: src/sh-scan.l
	@printf "\tLEX\t%s\n" $@
	@flex $<

src/dep-scan.c: src/dep-scan.l
	@printf "\tLEX\t%s\n" $@
	@flex $<

.c.o: $(@:.o=.c)
	@printf "\tCC\t%s\n" $@
	@$(CC) $(WARN_FLAGS) $(WARN) $(SANITIZE) -c $(CPPFLAGS) $(CFLAGS) $< -o $@ #-MMD #-fdiagnostics-color=always

-include $(wildcard *.d)

build: $(OBJECTS)
	@printf "\tCCLD\t%s\n" $@
	@$(CC) $(WARN_FLAGS) $(WARN) $(SANITIZE) -o $@ $^ $(LDFLAGS) $(CFLAGS)

XGETTEXT_OPTIONS=--flag=fatal:1:c-format \
		 --flag=error_at:2:c-format \
		 --flag=error:1:c-format \
		 --flag=warning_at:2:c-format \
		 --flag=warning:1:c-format \
		 --flag=info:1:c-format \
		 --from-code=UTF-8 \
		 --flag=_:1:pass-c-format \
		 --keyword=_:1 \
		 --keyword=N_:1 \
		 --keyword=S_:1,2 \
		 --keyword=YY_FATAL_ERROR:1 \
		 --keyword=YY_:1 \
		 -F --no-wrap

$(PODIR)/%.po: $(POTFILE)
	@printf "\tMSGMERGE\t%s\n" $@
	@msgmerge -U $@ $<

$(POTFILE): $(SOURCES) src/parse.y src/scan.l
	@printf "\tXGETTEXT\t%s\n" $@
	@xgettext $(XGETTEXT_OPTIONS) --language=C --package-name $(PACKAGE) --package-version $(VERSION) --default-domain $(PACKAGE) --output $@ $^

$(PODIR)/locale/%/LC_MESSAGES/build.mo: $(PODIR)/%.po
	@printf "\tMSGFMT\t%s\n" $@
	@mkdir -p $(dir $@) && msgfmt --check --statistics --verbose --output-file $@ $<
