
#pragma once

#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "diagnostic.h"
#include "libquote/quoting-slots.h"
#include "system.h"
#include "uniqstr.h"

#ifdef _WIN32
extern NODISCARD uniqstr GetLastErrorString (void) RETURNS_NONNULL;
#endif

extern size_t get_count_of_processor_cores (void) NODISCARD;
extern struct timespec get_modification_time (const char *file, struct quoting_slots *qs) NODISCARD NONNULL (1, 2);

#ifdef _WIN32
typedef intptr_t process_id_t;
#else
typedef pid_t process_id_t;
#endif

extern bool process_spawn (process_id_t *restrict pid, uniqstr command, char **envp)
  NONNULL (1, 2, 3) ACCESS (write_only, 1);
extern bool process_wait (process_id_t pid);

static inline NODISCARD bool ACCESS (write_only, 3)
mul_overflow (size_t a, size_t b, size_t *c)
{
#if __GNUC__ >= 5 || (__clang_major__ > 3 || (__clang_major__ == 3 && __clang_minor__ >= 8))
  return __builtin_mul_overflow (a, b, c);
#else
# define SIZE_BIT (sizeof (size_t) * CHAR_BIT / 2)
# define HI(x)    (x >> SIZE_BIT)
# define LO(x)    ((((size_t) 1 << SIZE_BIT) - 1) & x)

  size_t s0, s1, s2, s3, x, result, carry;

  x = LO (a) * LO (b);
  s0 = LO (x);

  x = HI (a) * LO (b) + HI (x);
  s1 = LO (x);
  s2 = HI (x);

  x = s1 + LO (a) * HI (b);
  s1 = LO (x);

  x = s2 + HI (a) * HI (b) + HI (x);
  s2 = LO (x);
  s3 = HI (x);

  result = s1 << SIZE_BIT | s0;
  carry = s3 << SIZE_BIT | s2;

  *c = result;
  return carry != 0;

    /*
# define HALF_SIZE (sizeof (size_t) * 8 / 2)
# define HALF_MASK ((~(size_t) 0) >> HALF_SIZE)

  const size_t x = (a >> HALF_SIZE) * (b & HALF_MASK);
  const size_t y = (a & HALF_MASK) * (b >> HALF_SIZE);
  const bool overflow = (a >> HALF_SIZE) * (b >> HALF_SIZE) + (x >> HALF_SIZE) + (y >> HALF_SIZE);

  *c = x * y;
  return overflow;
# undef HALF_MASK
# undef HALF_SIZE
    */
#endif
}

static inline void * RETURNS_NONNULL NODISCARD ALLOC_SIZE (1)
xmalloc (size_t n)
{
  if (n == 0)
    fatal (_("attempting to allocate zero-size buffer"));

  void *p = malloc (n);

  if (p == NULL)
    fatal (_("memory exhausted"));

  return p;
}

static inline void * RETURNS_NONNULL NODISCARD ALLOC_SIZE (1, 2)
xcalloc (size_t n, size_t m)
{
  if (n == 0 || m == 0)
    fatal (_("attempting to allocate zero-size buffer"));

  void *p = calloc (n, m);

  if (p == NULL)
    fatal (_("memory exhausted"));

  return p;
}

static inline void * RETURNS_NONNULL NODISCARD ALLOC_SIZE (2)
xrealloc (void *p, size_t n)
{
  void *q = realloc (p, n);

  if (q == NULL)
    fatal (_("memory exhausted"));

  return q;
}

static inline void * RETURNS_NONNULL NODISCARD ALLOC_SIZE (1, 2)
xmallocarray (size_t n, size_t m)
{
  if (n == 0 || m == 0)
    fatal (_("attempting to allocate zero-size buffer"));

  size_t s;

  if (mul_overflow (n, m, &s))
    fatal (_("multiplication overflow: "PRIuSIZE" * "PRIuSIZE""), n, m);

  return xmalloc (s);
}

static inline void * RETURNS_NONNULL NODISCARD ALLOC_SIZE (2, 3)
xreallocarray (void *p, size_t n, size_t m)
{
  if (n == 0 || m == 0)
    fatal (_("attempting to allocate zero-size buffer"));

  size_t s;

  if (mul_overflow (n, m, &s))
    fatal (_("multiplication overflow: "PRIuSIZE" * "PRIuSIZE""), n, m);

  return xrealloc (p, s);
}

static inline void * RETURNS_NONNULL NODISCARD ALLOC_SIZE (2)
xmemdup (void *p, size_t n)
{
  return memcpy (xmalloc (n), p, n);
}

static inline char * RETURNS_NONNULL NODISCARD
xstrdup (const char *s)
{
  size_t n = strlen (s) + 1;
  char *r = memcpy (xmalloc (n), s, n);
  r[n - 1] = '\0';
  return r;
}

static inline char * RETURNS_NONNULL NODISCARD
xstrndup (const char *s, size_t n)
{
  size_t l = strnlen (s, n) + 1;
  char *r = memcpy (xmalloc (l), s, l);
  r[l - 1] = '\0';
  return r;
}
