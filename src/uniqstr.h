/* uniqstr.h -- uniqstr type and functions declarations.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#pragma once

#include <stdint.h>

#include "system.h"

/* Used as a marker for strings that are unique.  */
typedef const char *uniqstr;
typedef char uniqchr;

#define uniqstr_new_from_literal_string(str) \
  uniqstr_new_from_buffer (str, countof (str) - 1)

#define HashInsertUniqstr(table, string, data) \
  HashInsert (table, string, uniqstr_len (string), data)
#define HashFindUniqstr(table, string) \
  HashFind (table, string, uniqstr_len (string))
#define HashRemoveUniqstr(table, string) \
  HashRemove (table, string, uniqstr_len (string))

extern NODISCARD uniqchr *uniqstr_prepare (size_t size) RETURNS_NONNULL;
extern NODISCARD uniqstr uniqstr_finish_preparation (uniqchr *str) RETURNS_NONNULL NONNULL (1);
extern NODISCARD uniqstr uniqstr_new_from_buffer (char const *str, size_t size)
  NONNULL (1) ACCESS (read_only, 1, 2) RETURNS_NONNULL;
extern NODISCARD uniqstr uniqstr_new (const char *str)
  NONNULL (1) ACCESS (read_only, 1) RETURNS_NONNULL;
extern NODISCARD size_t uniqstr_hash (uniqstr str) NONNULL (1) PURE;
extern NODISCARD size_t uniqstr_len (uniqstr str) NONNULL (1) PURE;
extern NODISCARD bool uniqstr_cmp (uniqstr str1, uniqstr str2)
  NONNULL (1, 2) CONST;
extern NODISCARD const uniqchr *uniqstr_chr (uniqstr str, char c) NONNULL (1) PURE;
extern NODISCARD const uniqchr *uniqstr_end (uniqstr str) NONNULL (1) PURE;
extern void uniqstrs_table_create (void);
extern void uniqstrs_table_destroy (void);
