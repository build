
#pragma once

#define _GNU_SOURCE 1
#include <pthread.h>
#include <stddef.h>

#include "system.h"

/*
 * Lock-less NULL terminated single linked list
 *
 * Cases where locking is not needed:
 * If there are multiple producers and multiple consumers, llist_add() can be
 * used in producers and llist_del_all() can be used in consumers simultaneously
 * without locking. Also a single consumer can use llist_del_first() while
 * multiple producers simultaneously use llist_add(), without any locking.
 *
 * Cases where locking is needed:
 * If we have multiple consumers with llist_del_first() used in one consumer, and
 * llist_del_first() or llist_del_all() used in other consumers, then a lock is
 * needed.  This is because llist_del_first() depends on list->first->next not
 * changing, but without lock protection, there's no way to be sure about that
 * if a preemption happens in the middle of the delete operation and on being
 * preempted back, the list->first is the same as before causing the cmpxchg in
 * llist_del_first() to succeed. For example, while a llist_del_first() operation
 * is in progress in one consumer, then a llist_del_first(), llist_add(),
 * llist_add() (or llist_del_all(), llist_add(), llist_add()) sequence in another
 * consumer may cause violations.
 *
 * This can be summarized as follows:
 *
 *           |   add    | del_first |  del_all
 * add       |    -     |     -     |     -
 * del_first |          |     L     |     L
 * del_all   |          |           |     -
 *
 * Where, a particular row's operation can happen concurrently with a column's
 * operation, with "-" being no lock needed, while "L" being lock is needed.
 *
 * The list entries deleted via llist_del_all() can be traversed with
 * traversing function such as llist_for_each() etc.  But the list
 * entries can not be traversed safely before deleted from the list.
 * The order of deleted entries is from the newest to the oldest added
 * one.  If you want to traverse from the oldest to the newest, you
 * must reverse the order by yourself before traversing.
 *
 * The basic atomic operation of this list is cmpxchg on long.  On
 * architectures that don't have NMI-safe cmpxchg implementation, the
 * list can NOT be used in NMI handlers.  So code that uses the list in
 * an NMI handler should depend on CONFIG_ARCH_HAVE_NMI_SAFE_CMPXCHG.
 *
 * Copyright 2010, 2011 Intel Corp.
 *   Author: Huang Ying <ying.huang@intel.com>
 */

struct llist_node
{
  struct llist_node *_Atomic next;
};

struct llist_head
{
  struct llist_node *_Atomic first;
  atomic_bool lock; // TODO: heh, lock in lock-free list :), but come on, there is no other way for a while
};

#define LLIST_HEAD_INIT(name) { NULL, false }
#define LLIST_HEAD(name)      struct llist_head name = LLIST_HEAD_INIT (name)

/**
 * init_llist_head - initialize lock-less list head
 * @head:	the head for your lock-less list
 */
static inline void
llist_initialize_head (struct llist_head *list)
{
  list->first = NULL;
  list->lock = false;
}

/**
 * llist_entry - get the struct of this entry
 * @ptr:	the &struct llist_node pointer.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the llist_node within the struct.
 */
#define llist_entry(ptr, type, member) \
  container_of (ptr, type, member)

/**
 * llist_empty - tests whether a lock-less list is empty
 * @head:	the list to test
 *
 * Not guaranteed to be accurate or up to date.  Just a quick way to
 * test whether the list is empty without deleting something from the
 * list.
 */
static inline bool
llist_empty (const struct llist_head *head)
{
  return atomic_load (&head->first) == NULL;
}

static inline struct llist_node *
llist_next (struct llist_node *node)
{
  return node->next;
}

/**
 * llist_add_batch - add several linked entries in batch
 * @new_first:	first entry in batch to be added
 * @new_last:	last entry in batch to be added
 * @head:	the head for your lock-less list
 *
 * Return whether list is empty before adding.
 */
static inline bool
llist_add_batch (struct llist_node *new_first,
		 struct llist_node *new_last,
		 struct llist_head *head)
{
  struct llist_node *first = atomic_load (&head->first);

  do
    new_last->next = first;
  while (!atomic_compare_exchange_weak (&head->first, &first, new_first));

  return first == NULL;
}

static inline bool
__llist_add_batch (struct llist_node *new_first,
		   struct llist_node *new_last,
		   struct llist_head *head)
{
  new_last->next = head->first;
  head->first = new_first;
  return new_last->next == NULL;
}

/**
 * llist_add - add a new entry
 * @new:	new entry to be added
 * @head:	the head for your lock-less list
 *
 * Returns true if the list was empty prior to adding this entry.
 */
static inline bool
llist_add (struct llist_node *new, struct llist_head *head)
{
  return llist_add_batch (new, new, head);
}

static inline bool
__llist_add (struct llist_node *new, struct llist_head *head)
{
  return __llist_add_batch (new, new, head);
}

/**
 * llist_del_first - delete the first entry of lock-less list
 * @head:	the head for your lock-less list
 *
 * If list is empty, return NULL, otherwise, return the first entry
 * deleted, this is the newest added one.
 *
 * Only one llist_del_first user can be used simultaneously with
 * multiple llist_add users without lock.  Because otherwise
 * llist_del_first, llist_add, llist_add (or llist_del_all, llist_add,
 * llist_add) sequence in another user may change @head->first->next,
 * but keep @head->first.  If multiple consumers are needed, please
 * use llist_del_all or use lock between consumers.
 */
static inline struct llist_node *
llist_del_first (struct llist_head *head)
{
  struct llist_node *entry, *next;

  /* TODO: for a while here we use a spinlock, but I saw that exists lock-free lists, which do not
     use *any* locks even in llist_del_first() function.  */
  while (true)
    {
      if (!atomic_exchange_acquire (&head->lock, true))
        break;

      while (atomic_load_relaxed (&head->lock))
        sched_yield ();
    }

  entry = atomic_load (&head->first);

  do
    {
      if (entry == NULL)
        goto lreturn;

      next = atomic_load (&entry->next);
    }
  while (!atomic_compare_exchange_weak (&head->first, &entry, next));

lreturn:
  atomic_store_release (&head->lock, false);
  return entry;
}

static inline struct llist_node *
__llist_del_first (struct llist_head *head)
{
  struct llist_node *entry = head->first;

  if (entry == NULL)
    return NULL;

  head->first = entry->next;
  return entry;
}
