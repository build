
#include "yy.h"
#include "diagnostic.h"

void
yylocation_initialize (struct yyltype *yylocation, uniqstr file)
{
  yylocation->file                = file;
  yylocation->newline_offset      = -1;
  yylocation->next_newline_offset = -1;
  yylocation->first_line          = 1;
  yylocation->first_column        = 1;
  yylocation->last_line           = 1;
  yylocation->last_column         = 1;
}

void
yylocation_print (FILE *out, const struct yyltype *yylocation)
{
  int end_col = yylocation->last_column;

  end_col -= (end_col != 0);

  if (yylocation->first_line < yylocation->last_line)
    fprintf (out, "%d.%d-%d.%d",
             yylocation->first_line,
             yylocation->first_column,
             yylocation->last_line,
             end_col);
  else if (yylocation->first_column < end_col)
    fprintf (out, "%d.%d-%d",
             yylocation->first_line,
             yylocation->first_column,
             end_col);
  else
    fprintf (out, "%d.%d",
             yylocation->first_line,
             yylocation->first_column);
}

static inline bool
utf8_validate (const struct yyltype *loc,
               const char *string,
               int len,
               int *count)
{
#define VALIDATE_BYTE(mask, expect)                                   \
  do                                                                  \
    {                                                                 \
      if (UNLIKELY ((*p & mask) != expect))                           \
        {                                                             \
          error_at (loc,                                              \
                    _("expected UTF-8 continuation byte (got 0x%X)"), \
                    *p);                                              \
          goto error;                                                 \
        }                                                             \
    }                                                                 \
  while (0)

#define EXPECT_LENGTH(n)                                \
  do                                                    \
    {                                                   \
      if (UNLIKELY (len - (p - str) < n))               \
        {                                               \
          error_at (loc, _("string is too short"));     \
          goto error;                                   \
        }                                               \
    }                                                   \
  while (0)

  int character_count = 0;
  const unsigned char *const str = (const unsigned char *) string;
  const unsigned char *p;

  for (p = str; (p - str) < len && *p != '\0'; p++)
    {
      character_count++;

      if (*p < 0x80) /* 0xxxxxxx */
        continue;

      if (*p < 0xE0) /* 110xxxxx */
        {
          EXPECT_LENGTH (2);

          if (UNLIKELY (*p < 0xC2))
            goto invalid_start_byte;
        }
      else
        {
          if (*p < 0xF0) /* 1110xxxx */
            {
              EXPECT_LENGTH (3);

              switch (*p++ & 0x0F)
                {
                case 0x00:
                  VALIDATE_BYTE (0xE0, 0xA0); /* 0xA0 ... 0xBF */
                  break;
                case 0x0D:
                  VALIDATE_BYTE (0xE0, 0x80); /* 0x80 ... 0x9F */
                  break;
                default:
                  VALIDATE_BYTE (0xC0, 0x80); /* 10xxxxxx */
                }
            }
          else if (*p < 0xF5) /* 11110xxx excluding out-of-range */
            {
              EXPECT_LENGTH (4);

              switch (*p++ & 0x07)
                {
                case 0:
                  VALIDATE_BYTE (0xC0, 0x80); /* 10xxxxxx */

                  if (UNLIKELY ((*p & 0x30) == 0))
                    goto invalid_start_byte;

                  break;
                case 4:
                  VALIDATE_BYTE (0xF0, 0x80); /* 0x80 ... 0x8F */
                  break;
                default:
                  VALIDATE_BYTE (0xC0, 0x80); /* 10xxxxxx */
                  break;
                }

              p++;
              VALIDATE_BYTE (0xC0, 0x80); /* 10xxxxxx */
            }
          else
            goto invalid_start_byte;
        }

      p++;
      VALIDATE_BYTE (0xC0, 0x80); /* 10xxxxxx */
    }

  *count = character_count;
  return true;

invalid_start_byte:
  error_at (loc, _("invalid UTF-8 start byte (0x%X)"), *p);

error:
  *count = character_count;
  return false;
}

bool
yylocation_compute (struct yyltype *yylocation,
                    const char *text, int leng)
{
  /* Unicode version.  */
  int r;

  if (!utf8_validate (yylocation, text, leng, &r))
    return false;

  if (text[leng - 1] == '\n')
    {
      yylocation->first_line++;
      yylocation->last_line++;
      yylocation->first_column = 1;
      yylocation->last_column = 1;
    }
  else
    {
      yylocation->first_column = yylocation->last_column;
      yylocation->last_column += r;
    }

  return true;
}
