/* job-server.c -- declarations of list-based job-server routines.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#pragma once

#include "array.h"
#include "thread.h"

#include "lock-free-list.h"

/*
signal -- sem_post
wait -- sem_wait

n = the number of threads
count = 0
mutex = Semaphore(1)
barrier = Semaphore(0)


mutex.wait()
count = count + 1
mutex.signal()

if count == n: barrier.signal() # unblock ONE thread

barrier.wait()
barrier.signal() # once we are unblocked, it's our duty to unblock the next thread

//TODO May be use this strategy for job's parent control?
 */

typedef bool (*build_target_t) (const struct target *target, struct build_context *context, struct quoting_slots *qs);

struct job_list_node
{
  struct job *job;
  struct job_list_node *next;
};

struct job_list_head
{
  struct job_list_node *first;
};

struct job
{
  struct target *target;
  struct
  {
    struct mutex mutex;
    struct job_list_head head;
  } parents, children;
};

struct job_llist_node
{
  struct job *job;
  struct llist_node node;
};

struct job_server
{
  struct
  {
    struct llist_head head;
    struct semaphore semaphore; /* Kinda counter.  */
  } ready_jobs;
  struct
  {
    atomic_size_t count;
  } waiting_jobs;
  struct job job_tree;
  ARRAY_ENTRY (struct thread) threads;
  build_target_t routine;
  struct build_context build_context;
  atomic_bool failed;
  struct semaphore finished;
};

extern void job_tree_create_from_build_tree (struct job_server *job_server,
                                             struct job *root,
                                             struct build_tree *tree)
  NONNULL (1, 2);
extern NODISCARD bool job_server_initialize (struct job_server *job_server,
                                             build_target_t routine,
                                             size_t threads_count,
                                             struct build_context *context)
  NONNULL (1, 2, 4) ACCESS (write_only, 1);
extern NODISCARD bool job_server_start (struct job_server *job_server)
  NONNULL (1);
extern NODISCARD bool job_server_finalize (struct job_server *job_server)
  NONNULL (1);
