/* diagnostic.c -- diagnostic output routines.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#ifdef _WIN32
# include <windows.h>
#else
# include <unistd.h>
#endif

#include "diagnostic.h"

static bool verbose = false;
static const char *program_name;
static bool use_colors;
#ifndef _WIN32
static bool out_is_tty = true;
static bool err_is_tty = true;
#endif

void
diagnostic_initialize (const char *progname)
{
#ifndef _WIN32
  int saved_errno;
  const char *term = getenv ("TERM");

  use_colors = (term != NULL && strcmp (term, "dumb") != 0);
  saved_errno = errno;
  out_is_tty = isatty (1);
  err_is_tty = isatty (2);
  errno = saved_errno;
#endif
  program_name = progname;
}

void
diagnostic_enable_verbose (void)
{
  verbose = true;
}

enum diagnostic_type
  {
    diagnostic_type_bug,
    diagnostic_type_fatal,
    diagnostic_type_error,
    diagnostic_type_warning,
    diagnostic_type_info,
  };

#ifdef _WIN32
typedef WORD color_t;
# define COLOR_DEFAULT  0
# define COLOR_BOLD_RED (FOREGROUND_RED | FOREGROUND_INTENSITY)
# define COLOR_RED      (FOREGROUND_RED)
# define COLOR_YELLOW   (FOREGROUND_RED | FOREGROUND_GREEN)
# define COLOR_GREEN    (FOREGROUND_GREEN)
# define COLOR_CYAN     (FOREGROUND_GREEN | FOREGROUND_BLUE)
#else
typedef const char color_t[8];
# define COLOR_DEFAULT  "\x1B[00m\0\0"
# define COLOR_BOLD_RED "\x1B[1;31m"
# define COLOR_RED      "\x1B[31m"
# define COLOR_YELLOW   "\x1B[33m"
# define COLOR_GREEN    "\x1B[32m"
# define COLOR_CYAN     "\x1B[36m"
#endif

static const struct
{
  const char name[8];
  color_t color;
} diagnostic[5] =
  {
    [diagnostic_type_bug]     = { .name = N_("bug"),     .color = COLOR_BOLD_RED },
    [diagnostic_type_fatal]   = { .name = N_("fatal"),   .color = COLOR_BOLD_RED },
    [diagnostic_type_error]   = { .name = N_("error"),   .color = COLOR_RED      },
    [diagnostic_type_warning] = { .name = N_("warning"), .color = COLOR_YELLOW   },
    [diagnostic_type_info]    = { .name = N_("info"),    .color = COLOR_CYAN     },
  };

#undef COLOR_BOLD_RED
#undef COLOR_RED
#undef COLOR_YELLOW
#undef COLOR_GREEN
#undef COLOR_CYAN

static inline ALWAYS_INLINE void
set_color (FILE *fp, color_t color)
{
#ifdef _WIN32
  if (use_colors)
    {
      HANDLE hConsole;

      if (fp == stdout)
        hConsole = GetStdHandle (STD_OUTPUT_HANDLE);
      else if (fp == stderr)
        hConsole = GetStdHandle (STD_ERROR_HANDLE);
      else
        abort (); /* Unknown file stream.  */

      SetConsoleTextAttribute (hConsole, color);
    }
#else
  if (LIKELY (use_colors && (fileno (fp) == STDOUT_FILENO ? out_is_tty : (fileno (fp) == STDERR_FILENO ? err_is_tty : false))))
    fputs (color, fp);
#endif
}

static inline ALWAYS_INLINE void
file_stream_lock_acquire (FILE *fp)
{
#ifdef _WIN32
  _lock_file (fp);
#else
  flockfile (fp);
#endif
}

static inline ALWAYS_INLINE void
file_stream_lock_release (FILE *fp)
{
#ifdef _WIN32
  _unlock_file (fp);
#else
  funlockfile (fp);
#endif
}

/* The standard yyerror routine.  */
void
yyerror_base (const struct yyltype *yylocation, const char *s)
{
  file_stream_lock_acquire (stderr);
  fprintf (stderr, "%s: ", yylocation->file);
  yylocation_print (stderr, yylocation);
  fprintf (stderr, ": %s.\n", s);
  file_stream_lock_release (stderr);
}

static inline ALWAYS_INLINE void NONNULL (1, 4) FORMAT (printf, 4, 0)
diagnose (FILE *fp, enum diagnostic_type type,
          const struct yyltype *location,
          const char *f, va_list ap)
{
  file_stream_lock_acquire (fp);
  set_color (fp, diagnostic[type].color);
  fputs (program_name, fp);
  fputs (": ", fp);
  fputs (_(diagnostic[type].name), fp);
  fputs (": ", fp);

  if (location != NULL)
    {
      yylocation_print (fp, location);
      fputs (": ", fp);
    }

  vfprintf (fp, f, ap);
  fputc ('.', fp);
  set_color (fp, COLOR_DEFAULT);
  fputc ('\n', fp);
  file_stream_lock_release (fp);
}

void
fatal (const char *f, ...)
{
  va_list ap;

  va_start (ap, f);
  diagnose (stderr, diagnostic_type_fatal, NULL, f, ap);
  va_end (ap);
#ifdef _WIN32
  /* On Windows, some tools may inject extra threads.
     exit() may block on locks held by those threads, so forcibly exit.  */
  fflush (stderr);
  fflush (stdout);
  ExitProcess (EXIT_FAILURE);
#else
  exit (EXIT_FAILURE);
#endif
}

void
error (const char *f, ...)
{
  va_list ap;

  va_start (ap, f);
  diagnose (stderr, diagnostic_type_error, NULL, f, ap);
  va_end (ap);
}

void
error_at (const struct yyltype *location, const char *f, ...)
{
  va_list ap;

  va_start (ap, f);
  diagnose (stderr, diagnostic_type_error, location, f, ap);
  va_end (ap);
}

void
warning (const char *f, ...)
{
  va_list ap;

  va_start (ap, f);
  diagnose (stderr, diagnostic_type_warning, NULL, f, ap);
  va_end (ap);
}

void
warning_at (const struct yyltype *location, const char *f, ...)
{
  va_list ap;

  va_start (ap, f);
  diagnose (stderr, diagnostic_type_warning, location, f, ap);
  va_end (ap);
}

void
info (const char *f, ...)
{
  if (!verbose)
    return;

  va_list ap;

  va_start (ap, f);
  diagnose (stdout, diagnostic_type_info, NULL, f, ap);
  va_end (ap);
}
