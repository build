
#include "build.h"
#include "diagnostic.h"

void
graph (const struct build_tree *tree)
{
  error ("not implemented yet");
}

// TODO remake
#if 0

#include <stdio.h>

#include "graph.h"
#include "uniqstr.h"
#include "hash.h"
#include "utils.h"
#include "build.h"

struct edge
{
  uniqstr from;
  uniqstr to;
};

static inline void
visit_edge (struct hash_table *visited_edges, uniqstr from, uniqstr to)
{
  struct edge *edge = xcalloc (1, sizeof (*edge));

  edge->from = from;
  edge->to = to;
  HashInsert (visited_edges, edge, sizeof (*edge), edge);
}

static inline bool
edge_visited (struct hash_table *visited_edges, uniqstr from, uniqstr to)
{
  struct edge edge =
    {
      .from = from,
      .to = to
    };
  bool visited = HashFind (visited_edges, &edge, sizeof (edge)) != NULL;

  visit_edge (visited_edges, from, to);
  return visited;
}

static inline void NONNULL (1, 2, 3, 4)
add_edge (struct hash_table *visited_edges,
	  uniqstr from,
	  uniqstr to,
          struct apply_rule_array *rules)
{
  if (edge_visited (visited_edges, from, to))
    return;

  printf ("\"%s\" -> \"%s\"", from, to);
  fputs (" [label=\"", stdout);

  for (size_t i = 0; i < VECTOR_SIZE (*rules); i++)
    printf ("%s%s", VECTOR_AT (*rules, i).name, i + 1 == VECTOR_SIZE (*rules) ? "" : ", ");

  fputs ("\"];\n", stdout);
}

static void
same_rank (struct uniqstr_array nodes)
{
  if (VECTOR_SIZE (nodes) != 0)
    {
      fputs ("{ rank=same; ", stdout);

      for (size_t i = 0; i < VECTOR_SIZE (nodes); i++)
        printf ("\"%s\" ", VECTOR_AT (nodes, i));

      fputs ("}\n", stdout);
    }
}

static void NONNULL (1, 2, 3)
graph_add_node (struct hash_table *visited_nodes, struct hash_table *visited_edges,
                const struct build_tree_branch *tree)
{

  if (HashFind (visited_nodes, &tree, sizeof (tree)) != NULL)
    return;

  HashInsert (visited_nodes, &tree, sizeof (tree), &tree);

  if (VECTOR_SIZE (tree->target->skeleton->files) == 1)
    {
      add_edge (visited_edges,
                VECTOR_AT (tree->target->skeleton->files, 0),
                tree->target->name, &tree->target->skeleton->apply_rules);
    }
  else if (VECTOR_SIZE (tree->target->skeleton->files) > 1)
    {
      printf ("\"%s\" [shape=ellipse];\n",
              VECTOR_AT (tree->target->skeleton->apply_rules, 0).name);
      printf ("{ ");

      for (size_t i = 0; i < VECTOR_SIZE (tree->target->skeleton->files); i++)
        if (!edge_visited (visited_edges, VECTOR_AT (tree->target->skeleton->files, i),
                           VECTOR_AT (tree->target->skeleton->apply_rules, 0).name))
          printf ("\"%s\" ", VECTOR_AT (tree->target->skeleton->files, i));

      printf ("} -> \"%s\" [arrowhead=none];\n",
              VECTOR_AT (tree->target->skeleton->apply_rules, 0).name);
      printf ("\"%s\" -> \"%s\";\n",
              VECTOR_AT (tree->target->skeleton->apply_rules, 0).name,
              tree->target->name);
    }

  same_rank (tree->target->skeleton->files);

  if (tree->branches.count == 1)
    {
      graph_add_node (visited_nodes, visited_edges, tree->branches.array[0]);
      add_edge (visited_edges,
                tree->branches.array[0]->target->name,
                tree->target->name, &tree->target->skeleton->apply_rules);
    }
  else if (tree->branches.count > 1)
    {
      if (VECTOR_SIZE (tree->target->skeleton->apply_rules) != 0)
        {
          for (size_t i = 0; i < tree->branches.count; i++)
            {
              graph_add_node (visited_nodes, visited_edges,
                              tree->branches.array[i]);

              printf ("\"%s\" [shape=ellipse];\n",
                      VECTOR_AT (tree->target->skeleton->apply_rules, 0).name);

              if (!edge_visited (visited_edges,
                                 tree->branches.array[i]->target->name,
                                 VECTOR_AT (tree->target->skeleton->apply_rules, 0).name))
                printf ("\"%s\" -> \"%s\" [arrowhead=none];\n",
                        tree->branches.array[i]->target->name,
                        VECTOR_AT (tree->target->skeleton->apply_rules, 0).name);
            }

          if (!edge_visited (visited_edges,
                             VECTOR_AT (tree->target->skeleton->apply_rules, 0).name,
                             tree->target->name))
            printf ("\"%s\" -> \"%s\";\n",
                    VECTOR_AT (tree->target->skeleton->apply_rules, 0).name,
                    tree->target->name);
        }
      else
        {
          printf ("\"%s\" [shape=circle];\n", tree->target->name);

          for (size_t i = 0; i < tree->branches.count; i++)
            {
              graph_add_node (visited_nodes, visited_edges,
                              tree->branches.array[i]);

              if (!edge_visited (visited_edges,
                                 tree->branches.array[i]->target->name,
                                 tree->target->name))
                printf ("\"%s\" -> \"%s\";\n",
                        tree->branches.array[i]->target->name,
                        tree->target->name);
            }
        }
    }

  same_rank (tree->target->skeleton->dependencies);
  HashRemove (visited_nodes, &tree, sizeof (tree));
}

void
graph (const struct build_tree *tree)
{
  struct hash_table visited_nodes;
  struct hash_table visited_edges;
  fputs ("digraph {\n"
         "rankdir=\"LR\"\n"
         "node [fontsize=10, shape=box, height=0.25]\n"
         "edge [fontsize=10]\n", stdout);
  HashInit (&visited_nodes);
  HashInit (&visited_edges);
  graph_add_node (&visited_nodes, &visited_edges, tree->root);
  fputs ("}\n", stdout);
  HashClear (&visited_nodes);
  HashClearWithDestructor (&visited_edges, free);
}
#endif
