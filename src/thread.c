/* thread.c -- system-independent thread types and functions.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#ifndef _WIN32
# include <errno.h>
# include <string.h>
#endif

#include "diagnostic.h"
#include "thread.h"
#include "uniqstr.h"
#include "utils.h"

#ifdef _WIN32
static DWORD __stdcall
win32_worker (LPVOID p)
{
  struct thread *thread = p;
  thread->result = thread->routine (thread->argument);
  return 0;
}
#endif

bool
thread_create (struct thread *thread, thread_routine_t routine, void *argument)
{
#ifdef _WIN32
  thread->routine = routine;
  thread->argument = argument;
  thread->result = NULL;
  thread->thread = CreateThread (NULL,         /* Default security attributes.  */
                                 0,            /* Default stack size.  */
                                 win32_worker, /* Thread function.  */
                                 thread,       /* Thread function arguments.  */
                                 0,            /* Default creation flags.  */
                                 NULL);        /* Receive thread identifier.  */

  if (UNLIKELY (thread->thread == NULL))
    {
      error (_("failed to create thread: %s"), GetLastErrorString ());
      return false;
    }

  return true;
#else
  if (UNLIKELY (pthread_create (&thread->thread, NULL, routine, argument) != 0))
    {
      error (_("failed to create thread: %s"), strerror (errno));
      return false;
    }

  return true;
#endif
}

bool
thread_join (struct thread *thread, void **result)
{
#ifdef _WIN32
  if (UNLIKELY (WaitForSingleObjectEx (thread->thread, INFINITE, FALSE) != WAIT_OBJECT_0))
    {
      error (_("failed to join thread: %s"), GetLastErrorString ());
      return false;
    }

  CloseHandle (thread->thread);

  if (result != NULL)
    *result = thread->result;

  return true;
#else
  if (UNLIKELY (pthread_join (thread->thread, result) != 0))
    {
      error (_("failed to join thread: %s"), strerror (errno));
      return false;
    }

  return true;
#endif
}

void
mutex_create (struct mutex *mutex)
{
#ifdef _WIN32
  InitializeSRWLock (&mutex->mutex);
#else
  if (UNLIKELY (pthread_mutex_init (&mutex->mutex, NULL) != 0))
    fatal (_("failed to create mutex: %s"), strerror (errno));
#endif
}

void
mutex_acquire (struct mutex *mutex)
{
#ifdef _WIN32
  AcquireSRWLockExclusive (&mutex->mutex);
#else
  if (UNLIKELY (pthread_mutex_lock (&mutex->mutex) != 0))
    fatal (_("failed to acquire mutex: %s"), strerror (errno));
#endif
}

void
mutex_release (struct mutex *mutex)
{
#ifdef _WIN32
  ReleaseSRWLockExclusive (&mutex->mutex);
#else
  if (UNLIKELY (pthread_mutex_unlock (&mutex->mutex) != 0))
    fatal (_("failed to release mutex: %s"), strerror (errno));
#endif
}

void
mutex_destroy (UNUSED struct mutex *mutex)
{
#ifdef _WIN32
  /* Unlocked SWR locks use no resources.  */
#else
  pthread_mutex_destroy (&mutex->mutex);
#endif
}

#if USE_OWN_SEMAPHORE
static inline void
condition_variable_create (struct condition_variable *condition_variable)
{
#ifdef _WIN32
  InitializeConditionVariable (&condition_variable->condition_variable);
#else
  if (UNLIKELY (pthread_cond_init (&condition_variable->condition_variable, NULL) != 0))
    fatal (_("failed to create condition variable: %s"), strerror (errno));
#endif
}

static inline void
condition_variable_wait (struct condition_variable *condition_variable, struct mutex *mutex)
{
#ifdef _WIN32
  if (UNLIKELY (!SleepConditionVariableSRW (&condition_variable->condition_variable, &mutex->mutex,
                                            INFINITE, 0)))
    fatal (_("failed to wait condition variable: %s"), GetLastErrorString ());
#else
  if (UNLIKELY (pthread_cond_wait (&condition_variable->condition_variable, &mutex->mutex) != 0))
    fatal (_("failed to wait condition variable: %s"), strerror (errno));
#endif
}

static inline void
condition_variable_wake (struct condition_variable *condition_variable)
{
#ifdef _WIN32
  WakeConditionVariable (&condition_variable->condition_variable);
#else
  if (UNLIKELY (pthread_cond_signal (&condition_variable->condition_variable) != 0))
    fatal (_("failed to signal condition variable: %s"), strerror (errno));
#endif
}

static inline void
condition_variable_wake_all (struct condition_variable *condition_variable)
{
#ifdef _WIN32
  WakeAllConditionVariable (&condition_variable->condition_variable);
#else
  if (UNLIKELY (pthread_cond_broadcast (&condition_variable->condition_variable) != 0))
    fatal (_("failed to signal condition variable: %s"), strerror (errno));
#endif
}

static inline void
condition_variable_destroy (struct condition_variable *condition_variable)
{
#ifdef _WIN32
  /* No resources to be released.  */
#else
  if (UNLIKELY (pthread_cond_destroy (&condition_variable->condition_variable) != 0))
    fatal (_("failed to destroy condition variable: %s"), strerror (errno));
#endif
}


// Implementation of the semaphore taken from here:
// https://stackoverflow.com/a/6002408
/*
struct semaphore
{
  uint64_t count;
  uint64_t wait_count;
  struct mutex mutex;
  struct condition_variable condition_variable;
};
*/
void
semaphore_create (struct semaphore *semaphore, uint64_t initial_value)
{
  semaphore->count = initial_value;
  semaphore->wait_count = 0;
  mutex_create (&semaphore->mutex);
  condition_variable_create (&semaphore->condition_variable);
}

void
semaphore_wait (struct semaphore *semaphore)
{
  mutex_acquire (&semaphore->mutex);

  while (semaphore->count == 0)
    {
      semaphore->wait_count++;
      condition_variable_wait (&semaphore->condition_variable, &semaphore->mutex);

      // A call to wait() unlocks the mutex and suspends the thread.
      // It does not busy wait the thread is suspended.
      //
      // When a condition variable receives a condition_variable_wake() a random thread
      // is un-suspended. But it is not released from the call to wait
      // until the mutex can be reacquired by the thread.
      //
      // Thus we get here only after the mutex has been locked.
      //
      // You need to use a while loop above because of this potential situation.
      //      Thread A:  Suspended waiting on condition variable.
      //      Thread B:  Working somewhere else.
      //      Thread C:  calls signal() below (incrementing count to 1)
      //                 This results in A being awakened but it can not exit wait()
      //                 until it requires the mutex with a lock.  While it tries to
      //                 do that thread B finishes what it was doing and calls wait()
      //                 Thread C has incremented the count to 1 so thread B does not
      //                 suspend but decrements the count to zero and exits.
      //                 Thread B now aquires the mutex but the count has been decremented to
      //                 zero so it must immediately re-suspend on the condition variable.

      // Note a thread will not be released from wait until
      // it receives a signal and the mustex lock can be re-established.

      semaphore->wait_count--;
    }

  semaphore->count--;
  mutex_release (&semaphore->mutex);
}

void
semaphore_post (struct semaphore *semaphore)
{
  // You could optimize this part with interlocked increment.
  mutex_acquire (&semaphore->mutex);
  semaphore->count++;

  // This Comment based on using `interlocked increment` rather than mutex.
  //
  // As this part does not modify anything you do not actually need the lock.
  // Potentially this will release more threads than you need (as you do not
  // have exclusivity on reading 'wait_count' but that will not matter as the
  // wait() method does and any extra woken threads will be put back to sleep.

  // If there are any waiting threads let them out.
  if (semaphore->wait_count != 0)
    condition_variable_wake (&semaphore->condition_variable);

  mutex_release (&semaphore->mutex);
}

/* Unconditionally wake up all waiter to do something.  */
void
semaphore_post_all (struct semaphore *semaphore)
{
  mutex_acquire (&semaphore->mutex);
  semaphore->count += semaphore->wait_count;
  condition_variable_wake_all (&semaphore->condition_variable);
  mutex_release (&semaphore->mutex);
}

void
semaphore_destroy (struct semaphore *semaphore)
{
  mutex_destroy (&semaphore->mutex);
  condition_variable_destroy (&semaphore->condition_variable);
}
#else
void
semaphore_create (struct semaphore *semaphore, size_t initial_value)
{
#ifdef _WIN32
  semaphore->semaphore = CreateSemaphoreA (NULL, (LONG) initial_value, real_maximum, NULL);

  if (UNLIKELY (semaphore->semaphore == NULL))
    fatal (_("failed to create semaphore: %s"), GetLastErrorString ());
#else
  if (UNLIKELY (sem_init (&semaphore->semaphore, 0 /* Not shared.  */, (unsigned int) initial_value) != 0))
    fatal (_("failed to create semaphore: %s"), strerror (errno));
#endif
}

void
semaphore_wait (struct semaphore *semaphore)
{
#ifdef _WIN32
  if (UNLIKELY (WaitForSingleObject (semaphore->semaphore, INFINITE) != WAIT_OBJECT_0))
    fatal (_("failed to wait semaphore: %s"), GetLastErrorString ());
#else
  while (UNLIKELY (sem_wait (&semaphore->semaphore) < 0))
    if (UNLIKELY (errno != EINTR))
      fatal (_("failed to wait semaphore: %s"), strerror (errno));
#endif
}

void
semaphore_post (struct semaphore *semaphore)
{
#ifdef _WIN32
  if (UNLIKELY (!ReleaseSemaphore (semaphore->semaphore, 1, NULL)))
    fatal (_("failed to post semaphore: %s"), GetLastErrorString ());
#else
  if (UNLIKELY (sem_post (&semaphore->semaphore) < 0))
    fatal (_("failed to post semaphore: %s"), strerror (errno));
#endif
}

void
semaphore_destroy (struct semaphore *semaphore)
{
#ifdef _WIN32
  CloseHandle (semaphore->semaphore);
#else
  if (UNLIKELY (sem_destroy (&semaphore->semaphore) < 0))
    fatal (_("failed to destroy semaphore: %s"), strerror (errno));
#endif
}
#endif

void
rwlock_create (struct rwlock *rwlock)
{
#ifdef _WIN32
  InitializeSRWLock (&rwlock->rwlock);
#else
  if (UNLIKELY (pthread_rwlock_init (&rwlock->rwlock, NULL) != 0))
    fatal (_("failed to create read-write lock: %s"), strerror (errno));
#endif
}

void
rwlock_read_acquire (struct rwlock *rwlock)
{
#ifdef _WIN32
  AcquireSRWLockShared (&rwlock->rwlock);
#else
  if (UNLIKELY (pthread_rwlock_rdlock (&rwlock->rwlock) != 0))
    fatal (_("failed to acquire for reading read-write lock: %s"), strerror (errno));
#endif
}

void
rwlock_read_release (struct rwlock *rwlock)
{
#ifdef _WIN32
  ReleaseSRWLockShared (&rwlock->rwlock);
#else
  if (UNLIKELY (pthread_rwlock_unlock (&rwlock->rwlock) != 0))
    fatal (_("failed to release read-write lock: %s"), strerror (errno));
#endif
}

void
rwlock_write_acquire (struct rwlock *rwlock)
{
#ifdef _WIN32
  AcquireSRWLockExclusive (&rwlock->rwlock);
#else
  if (UNLIKELY (pthread_rwlock_wrlock (&rwlock->rwlock) != 0))
    fatal (_("failed to acquire for writing read-write lock: %s"), strerror (errno));
#endif
}

void
rwlock_write_release (struct rwlock *rwlock)
{
#ifdef _WIN32
  ReleaseSRWLockExclusive (&rwlock->rwlock);
#else
  if (UNLIKELY (pthread_rwlock_unlock (&rwlock->rwlock) != 0))
    fatal (_("failed to release read-write lock: %s"), strerror (errno));
#endif
}

void
rwlock_destroy (struct rwlock *rwlock)
{
#ifdef _WIN32
  return; /* Nothing required.  */
#else
  if (UNLIKELY (pthread_rwlock_destroy (&rwlock->rwlock) != 0))
    fatal (_("failed to destroy read-write lock: %s"), strerror (errno));
#endif
}
