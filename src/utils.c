
#if defined (__APPLE__) || defined (__FreeBSD__)
# include <sys/sysctl.h>
#elif defined (__SVR4) && defined (__sun)
# include <sys/loadavg.h>
# include <unistd.h>
#elif defined (linux) || defined (__GLIBC__)
# include <sys/sysinfo.h>
#endif

#if defined (__FreeBSD__) || defined (__OpenBSD__) || defined (__NetBSD__)
# define __BSD_VISIBLE 1
# include <sys/sysctl.h>
# include <sys/types.h>
#endif

#ifndef _WIN32
# include <errno.h>
# include <spawn.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <unistd.h>
#endif

#include "utils.h"

size_t
get_count_of_processor_cores (void)
{
#if defined (__linux__)
  long n_cores = sysconf (_SC_NPROCESSORS_ONLN);

  return (n_cores < 1) ? 1 : (size_t) n_cores;
#elif defined (__FreeBSD__) || defined (__OpenBSD__) || defined (__NetBSD__)
  int mib[4];
  int n_cores;
  size_t size = sizeof (n_cores);

  /* Set the mib for hw.ncpu.  */
  mib[0] = CTL_HW;
  mib[1] = HW_NCPU;

  /* Get the number of CPUs from the system.  */
  sysctl (mib, 2, &n_cores, &size, NULL, 0);

  return (n_cores < 1) ? 1 : (size_t) n_cores;
#elif defined (_WIN32)
  SYSTEM_INFO sysinfo;
  DWORD n_cores;

  GetSystemInfo (&sysinfo);
  n_cores = sysinfo.dwNumberOfProcessors;

  return (size_t) n_cores;
#else
# warning "can not determine count of processor's cores."
  return 1;
#endif
}

#ifdef _WIN32
uniqstr
GetLastErrorString (void)
{
  DWORD err = GetLastError ();
  char *msg_buf;
  uniqstr msg;

  FormatMessageA (FORMAT_MESSAGE_ALLOCATE_BUFFER
                | FORMAT_MESSAGE_FROM_SYSTEM
                | FORMAT_MESSAGE_IGNORE_INSERTS,
                  NULL,
                  err,
                  MAKELANGID (LANG_NEUTRAL, SUBLANG_DEFAULT),
                  (char *) &msg_buf,
                  0,
                  NULL);
  msg = uniqstr_new (msg_buf);
  LocalFree (msg_buf);
  return msg;
}
#endif

struct timespec
get_modification_time (uniqstr file, struct quoting_slots *qs)
{
#ifdef _WIN32
  struct timespec ts = { .tv_sec = 0, .tv_nsec = 0 };
  WIN32_FILE_ATTRIBUTE_DATA attributes;

  if (GetFileAttributesExA (file, GetFileExInfoStandard, &attributes))
    {
      FILETIME mt = attributes.ftLastWriteTime;

      ts.tv_sec  = (((__int64) mt.dwHighDateTime << (sizeof (DWORD) * 8))
                   + (__int64) mt.dwLowDateTime) / (10000000);
      ts.tv_nsec = (((__int64) mt.dwHighDateTime << (sizeof (DWORD) * 8))
                   + (__int64) mt.dwLowDateTime) % (10000000) * 100;
    }
  else
    {
      DWORD err = GetLastError ();

      if (err != ERROR_FILE_NOT_FOUND && err != ERROR_PATH_NOT_FOUND)
        error (_("%s: can not get modification time: %s"), quote (qs, file), GetLastErrorString ());
    }

  return ts;
#else
  struct stat sb;
  struct timespec ts = { .tv_sec = 0, .tv_nsec = 0 };

  if (stat (file, &sb) == 0)
    ts = sb.st_mtim;
  else if (errno != ENOENT)
    error (_("%s: can not get modification time: %s"), quote (qs, file), strerror (errno));

  return ts;
#endif
}

bool
process_spawn (process_id_t *restrict pid, uniqstr command, char **envp)
{
#ifdef _WIN32
  const char *argv[] = { "cmd", "/c", command, NULL };
  *pid = _spawnvpe (_P_NOWAIT, argv[0], (const char *const *) argv, (const char *const *) envp);
  return *pid >= 0;
#else
  const char *argv[] = { "/bin/sh", "-c", command, NULL };
  return posix_spawnp (pid, argv[0], NULL, NULL, worst_cast (argv), envp) == 0;
#endif
}

bool
process_wait (process_id_t pid)
{
#ifdef _WIN32
  int status;

  return _cwait (&status, pid, WAIT_CHILD) >= 0;
#else
  int status;

  return waitpid (pid, &status, 0) && WIFEXITED (status) && WEXITSTATUS (status) == EXIT_SUCCESS;
#endif
}
