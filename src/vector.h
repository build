
#pragma once

#include <stddef.h>

#include "utils.h"

/* Vector always has capacity at least equal to __VECTOR_INITIAL_CAPACITY.  */
#define __VECTOR_INITIAL_CAPACITY 8

#define VECTOR_DECLARE_TYPE(name, type) struct name { size_t capacity; size_t used; type *buffer; }
#define VECTOR_ENTRY(name) struct name
#define VECTOR_ANONYMOUS_ENTRY(type) struct { size_t capacity; size_t used; type *buffer; }

#define VECTOR_CAPACITY(a0)  (a0).capacity
#define VECTOR_SIZE(a0)      (a0).used
#define VECTOR_AT(a0, index) (a0).buffer[index]

#define VECTOR_INITIALIZE_EMPTY(a0)             \
  do                                            \
    {                                           \
      (a0).capacity = 0;                        \
      (a0).used = 0;                            \
      (a0).buffer = NULL;                       \
      /*VECTOR_INITIALIZE (a0);*/               \
    }                                           \
  while (0)

#define VECTOR_INITIALIZE(a0)                                   \
  do                                                            \
    {                                                           \
      (a0).capacity = __VECTOR_INITIAL_CAPACITY;                \
      (a0).used = 0;                                            \
      (a0).buffer = xmallocarray ((a0).capacity,                \
                                  sizeof (*(a0).buffer));       \
    }                                                           \
  while (0)

#define VECTOR_INITIALIZE_WITH_CAPACITY(a0, c)                          \
  do                                                                    \
    {                                                                   \
      (a0).capacity = ((c) < __VECTOR_INITIAL_CAPACITY                  \
                       ? __VECTOR_INITIAL_CAPACITY : (c));              \
      (a0).used = 0;                                                    \
      (a0).buffer = xmallocarray ((a0).capacity,                        \
                                  sizeof (*(a0).buffer));               \
    }                                                                   \
  while (0)

#define VECTOR_FINALIZE(a0)                     \
  do                                            \
    {                                           \
      free ((a0).buffer);                       \
    }                                           \
  while (0)

#define VECTOR_DUPLICATE(a0, a1)                                        \
  do                                                                    \
    {                                                                   \
      if ((a1).used == 0)                                               \
        VECTOR_INITIALIZE_EMPTY (a0);                                   \
      else                                                              \
        {                                                               \
          (a0).used = (a1).used;                                        \
          (a0).capacity = (a1).used;                                    \
          (a0).buffer = xmemdup ((a1).buffer,                           \
                                 (a1).used * sizeof (*(a0).buffer));    \
        }                                                               \
    }                                                                   \
  while (0)

#define __max(a, b) ((a) >= (b) ? (a) : (b))

#define VECTOR_RESERVE(a0, size)                                        \
  do                                                                    \
    {                                                                   \
      if ((a0).used + (size) >= (a0).capacity)                          \
        {                                                               \
          if ((a0).capacity == 0)                                       \
            VECTOR_INITIALIZE (a0);                                     \
          else                                                          \
            {                                                           \
              (a0).capacity = __max ((a0).capacity * 2,                 \
                                     (a0).used + size);                 \
              (a0).buffer = xreallocarray ((a0).buffer,                 \
                                           (a0).capacity,               \
                                           sizeof (*(a0).buffer));      \
            }                                                           \
        }                                                               \
    }                                                                   \
  while (0)

#define VECTOR_PUSH(a0, element)                                        \
  do                                                                    \
    {                                                                   \
      if ((a0).used + 1 >= (a0).capacity)                               \
        {                                                               \
          if ((a0).capacity == 0)                                       \
            VECTOR_INITIALIZE (a0);                                     \
          else                                                          \
            {                                                           \
              (a0).capacity *= 2;                                       \
              (a0).buffer = xreallocarray ((a0).buffer,                 \
                                           (a0).capacity,               \
                                           sizeof (*(a0).buffer));      \
            }                                                           \
        }                                                               \
                                                                        \
      (a0).buffer[(a0).used++] = (element);                             \
    }                                                                   \
  while (0)

#define VECTOR_CATENATE(a0, a1)                                 \
  do                                                            \
    {                                                           \
      if ((a1).used != 0)                                       \
        {                                                       \
          (a0).buffer = xreallocarray ((a0).buffer,             \
                                       (a0).used + (a1).used,   \
                                       sizeof (*(a0).buffer));  \
          memcpy ((a0).buffer + (a0).used, (a1).buffer,         \
                  (a1).used * sizeof (*(a0).buffer));           \
          (a0).used += (a1).used;                               \
        }                                                       \
    }                                                           \
  while (0)

#define VECTOR_SHRINK(a0)                                               \
  do                                                                    \
    {                                                                   \
      (a0).capacity = ((a0).used < __VECTOR_INITIAL_CAPACITY            \
                       ? __VECTOR_INITIAL_CAPACITY : (a0).used);        \
      (a0).buffer = xreallocarray ((a0).buffer, (a0).capacity,          \
                                   sizeof ((a0).buffer));               \
    }                                                                   \
  while (0)
