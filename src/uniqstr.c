/* uniqstr.c -- implementation of unique strings used for shortage
   memory footprint by lot of strings.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "diagnostic.h"
#include "hash.h"
#include "system.h"
#include "thread.h"
#include "uniqstr.h"
#include "utils.h"

/* Use global variable (yeah, that is shit) to simplify API.  */
static struct hash_table uniqstrs_table;
static struct rwlock uniqstrs_table_lock; /* Read-write lock for uniqstrs_table (that should be obvious).  */

struct ustring
{
  size_t size;
  uniqchr buffer[__flexible_array__] counted_by (size);
};

static inline void
free_uniqstr (uniqstr s)
{
  free ((char *) worst_cast (s) - offsetof (struct ustring, buffer));
}

uniqchr *
uniqstr_prepare (size_t size)
{
  struct ustring *u;

  /* First insertion in the hash.  */
  u = xmalloc (sizeof (struct ustring) + __flexible_array_extra_size__ + size + 1);
  u->size = size;
  return u->buffer;
}

uniqstr
uniqstr_finish_preparation (uniqchr *string)
{
  uniqstr result;

  rwlock_write_acquire (&uniqstrs_table_lock);
  result = HashInsertNoReplace (&uniqstrs_table, string, uniqstr_len (string), string);
  rwlock_write_release (&uniqstrs_table_lock);

  if (result != string)
    free_uniqstr (string);

  return result;
}

uniqstr
uniqstr_new_from_buffer (const char *buffer, size_t size)
{
  uniqstr result;
  struct ustring *u;

  rwlock_read_acquire (&uniqstrs_table_lock);
  result = HashFind (&uniqstrs_table, buffer, size);
  rwlock_read_release (&uniqstrs_table_lock);

  if (UNLIKELY (result == NULL))
    {
      /* First insertion in the hash.  */
      u = xmalloc (sizeof (struct ustring) + __flexible_array_extra_size__ + size + 1);
      u->size = size;
      memcpy (u->buffer, buffer, size);
      u->buffer[size] = '\0';
      rwlock_write_acquire (&uniqstrs_table_lock);
      result = HashInsertNoReplace (&uniqstrs_table, u->buffer, size, u->buffer);
      rwlock_write_release (&uniqstrs_table_lock);

      if (UNLIKELY (result != u->buffer))
        free (u);
      else
        result = u->buffer;
    }

  return result;
}

uniqstr
uniqstr_new (const char *buffer)
{
  return uniqstr_new_from_buffer (buffer, strlen (buffer));
}

size_t
uniqstr_len (uniqstr string)
{
  return ((struct ustring *) (string - offsetof (struct ustring, buffer)))->size;
}

bool
uniqstr_cmp (uniqstr string1, uniqstr string2)
{
  return string1 == string2;
}

const uniqchr *
uniqstr_chr (uniqstr string, char c)
{
  return memchr (string, c, uniqstr_len (string));
}

const uniqchr *
uniqstr_end (uniqstr string)
{
  return string + uniqstr_len (string);
}

void
uniqstrs_table_create (void)
{
  rwlock_create (&uniqstrs_table_lock);

  /* Can not provide uniqstr_hash() since HashFind can be called
     with non-uniqstr pointer.  */
  HashInit (&uniqstrs_table);
}

static void
uniqstr_destroy (void *p)
{
  free_uniqstr (p);
}

void
uniqstrs_table_destroy (void)
{
  HashClearWithDestructor (&uniqstrs_table, uniqstr_destroy);
  rwlock_destroy (&uniqstrs_table_lock);
}
