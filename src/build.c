/* build.c -- main file of the Build.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>

#include "build.h"
#include "cli-option-parser.h"
#include "diagnostic.h"
#include "graph.h"
#include "job-server.h"
#include "libquote/system-quote.h"
#include "parse.h"
#include "scan.h"
#include "thread.h"
#include "utils.h"

static inline bool
variable_initialize (struct hash_table *variables, struct variable *variable,
                     uniqstr name, struct variable_value *value)
{
  variable->name = name;

  switch (value->type)
    {
    case VARIABLE_STRING:
    case VARIABLE_STRING_ARRAY:
      variable->value = *value;
      break;
    default:
      ERROR_UNKNOWN_VALUE_TYPE (*value);
      return false;
    }

  HashInsertUniqstr (variables, name, variable);
  return true;
}

bool
variable_create (struct hash_table *variables, uniqstr name, struct variable_value *value)
{
  struct variable *variable;

  if (UNLIKELY (STRLITEQ (name, "in")
             || STRLITEQ (name, "out")
             || STRLITEQ (name, "target")
             || STRLITEQ (name, "dependencies")
             || STRLITEQ (name, "files")))
    {
      /* No quote needed since we are sure that variable name
         contains only letters, digits and underscore.  */
      error (_("%s: this variable name is reserved"), name);
      return false;
    }

  variable = xmalloc (sizeof (*variable));
  return variable_initialize (variables, variable, name, value);
}

static inline bool
variable_lookup (const struct hash_table *variables, uniqstr name,
                 const struct variable **v)
{
  const struct variable *variable = HashFindUniqstr (variables, name);

  if (LIKELY (variable != NULL))
    {
      *v = variable;
      return true;
    }

  error (_("variable %s is not defined"), name);
  return false;
}

static void
variable_destroy (void *p)
{
  struct variable *variable = p;

  switch (variable->value.type)
    {
    case VARIABLE_STRING_ARRAY:
      VECTOR_FINALIZE (variable->value.array);
      break;
    case VARIABLE_STRING:
      break;
    default:
      ERROR_UNKNOWN_VALUE_TYPE (variable->value);
      break;
    }

  free (variable);
}

static inline void NONNULL (1, 2)
variables_duplicate (struct hash_table *dst, const struct hash_table *src)
{
  HashInit (dst);

  for (struct hash_table_entry *p = HashFirst (src); p != NULL; p = HashNext (p))
    {
      const struct variable *v = p->data;
      struct variable *variable = xmalloc (sizeof (*variable));

      variable->name = v->name;
      variable->value.type = v->value.type;

      switch (v->value.type)
        {
        case VARIABLE_STRING_ARRAY:
          VECTOR_DUPLICATE (variable->value.array, v->value.array);
          break;
        case VARIABLE_STRING:
          variable->value.string = v->value.string;
          break;
        default:
          ERROR_UNKNOWN_VALUE_TYPE (v->value);
          break;
        }

      HashInsert (dst, HashKeyPtr (p), HashKeySize (p), variable);
    }
}

bool
target_create (struct target **t,
               struct hash_table *targets,
               uniqstr name,
               struct target_skeleton *skeleton,
               struct quoting_slots *qs)
{
  if (UNLIKELY (HashFindUniqstr (targets, name) != NULL))
    {
      error (_("redefinition of target %s"), quote (qs, name));
      return false;
    }

  if (UNLIKELY (VECTOR_SIZE (skeleton->files) != 0 && VECTOR_SIZE (skeleton->apply_rules) == 0))
    {
      error (_("target %s depends on files, but no rules provided"), quote (qs, name));
      return false;
    }

  struct target *target = xmalloc (sizeof (*target));

  target->name = name;
  target->skeleton = skeleton;
  HashInsertUniqstr (targets, name, target);

  if (t != NULL)
    *t = target;

  return true;
}

static void
target_skeleton_destroy (void *p)
{
  struct target_skeleton *skeleton = p;

  VECTOR_FINALIZE (skeleton->dependencies);
  VECTOR_FINALIZE (skeleton->files);
  VECTOR_FINALIZE (skeleton->outfiles);
  VECTOR_FINALIZE (skeleton->apply_rules);
  free (skeleton);
}

static void rule_destroy (void *p);

bool
rule_create (struct hash_table *rules, uniqstr name, uniqstr description,
             struct uniqstr_array *commands, const struct hash_table *variables)
{
  if (UNLIKELY (HashFindUniqstr (rules, name) != NULL))
    {
      error (_("redefinition of rule %s"), name);
      return false;
    }

  struct rule *rule = xmalloc (sizeof (*rule));

  rule->name = name;
  rule->description = description;
  rule->commands = *commands;
  variables_duplicate (&rule->variables, variables);
  HashInsertUniqstr (rules, name, rule);
  return true;
}

static void
rule_destroy (void *p)
{
  struct rule *rule = p;

  VECTOR_FINALIZE (rule->commands);
  HashClearWithDestructor (&rule->variables, variable_destroy);
  free (rule);
}

static inline bool
is_name_character (char c)
{
  return (c >= '0' && c <= '9')
      || (c >= 'a' && c <= 'z')
      || (c >= 'A' && c <= 'Z')
      || c == '_';
}

static inline bool
ascii_isspace (char c)
{
  return c == ' '
    || c == '\t'
    || c == '\f'
    || c == '\n'
    || c == '\r'
    || c == '\v';
}

static inline NODISCARD bool
parse_description (const uniqstr description,
                   const struct hash_table *variables,
                   char **result)
{
  size_t slen = uniqstr_len (description);
  uniqstr d = description;
  size_t written = 0;
  char *s = xmalloc (slen + 1);

  while (d[0] != '\0')
    {
      if (d[0] == '@' && is_name_character (d[1]))
        {
          size_t len = 2;
          uniqstr name;
          const struct variable *variable;

          while (is_name_character (d[len]))
            len++;

          name = uniqstr_new_from_buffer (d + 1, len - 1);

          if (UNLIKELY (!variable_lookup (variables, name, &variable)))
            goto lfail;

          switch (variable->value.type)
            {
            case VARIABLE_STRING:
              slen += uniqstr_len (variable->value.string);
              break;
            case VARIABLE_STRING_ARRAY:
              for (size_t i = 0; i < VECTOR_SIZE (variable->value.array); i++)
                slen += uniqstr_len (VECTOR_AT (variable->value.array, i)) + 1;

              break;
            default:
              ERROR_UNKNOWN_VALUE_TYPE (variable->value);
              break;
            }

          s = xrealloc (s, slen + 1);

          switch (variable->value.type)
            {
            case VARIABLE_STRING:
              memcpy (s + written, variable->value.string, uniqstr_len (variable->value.string));
              written += uniqstr_len (variable->value.string);
              s[written++] = ' ';
              break;
            case VARIABLE_STRING_ARRAY:
              for (size_t i = 0; i < VECTOR_SIZE (variable->value.array); i++)
                {
                  memcpy (s + written, VECTOR_AT (variable->value.array, i),
                          uniqstr_len (VECTOR_AT (variable->value.array, i)));
                  written += uniqstr_len (VECTOR_AT (variable->value.array, i));
                  s[written++] = ' ';
                }
              break;
            default:
              ERROR_UNKNOWN_VALUE_TYPE (variable->value);
              break;
            }

          written--;
          d += len;
          continue;
        }

      s[written++] = *d++;
    }

  s[written] = '\0';
  *result = s;
  return true;

lfail:
  free (s);
  return false;
}

static inline size_t
variable_name_length (const char *string)
{
  size_t n = 0;

  while (is_name_character (string[n]))
    n++;

  return n;
}

struct string_builder
{
  size_t size;
  size_t length;
  char *buffer;
};

static inline void
string_builder_initialize (struct string_builder *sb, size_t size)
{
  sb->size = size;
  sb->length = 0;
  sb->buffer = xmalloc (size);
}

#define MAX(a, b) ((a) >= (b) ? (a) : (b))

static inline void
string_builder_resevre (struct string_builder *sb, size_t size)
{
  if (UNLIKELY (sb->length + size + 1 >= sb->size))
    {
      sb->size = MAX (size * 2, sb->length + size + 1);
      sb->buffer = xrealloc (sb->buffer, sb->size);
    }
}

static inline void
string_builder_append_character (struct string_builder *sb, char character)
{
  string_builder_resevre (sb, 1);
  sb->buffer[sb->length++] = character;
}

static inline void
string_builder_append_string(struct string_builder *sb, const char *s, size_t n)
{
  string_builder_resevre (sb, n);
  memcpy (sb->buffer + sb->length, s, n);
  sb->length += n;
}

static inline bool
string_builder_append_variable_value (struct string_builder *sb, const char **input,
                                      const struct hash_table *variables, struct quoting_slots *qs)
{
  size_t len = variable_name_length (*input);
  const struct variable *variable;
  uniqstr name;

  name = uniqstr_new_from_buffer (*input, len);

  if (UNLIKELY (!variable_lookup (variables, name, &variable)))
    {
      error (_("variable %s is not defined"), quote (qs, name));
      return false;
    }

  size_t quoted_length = 0;

  switch (variable->value.type)
    {
    case VARIABLE_STRING:
      quoted_length += 1 + system_quote_length (SCI_SYSTEM,
                                                variable->value.string);
      break;
    case VARIABLE_STRING_ARRAY:
      for (size_t j = 0; j < VECTOR_SIZE (variable->value.array); j++)
        quoted_length += 1 + system_quote_length (SCI_SYSTEM,
                                                  VECTOR_AT (variable->value.array, j));
      break;
    default:
      ERROR_UNKNOWN_VALUE_TYPE (variable->value);
      break;
    }

  string_builder_resevre (sb, quoted_length + 1);

  switch (variable->value.type)
    {
    case VARIABLE_STRING:
      {
        char *p;

        p = system_quote_copy (sb->buffer + sb->length,
                               SCI_SYSTEM,
                               variable->value.string);
        sb->length += ((size_t) (p - (sb->buffer + sb->length)));
      }
      break;
    case VARIABLE_STRING_ARRAY:
      for (size_t j = 0; j < VECTOR_SIZE (variable->value.array); j++)
        {
          char *p;

          p = system_quote_copy (sb->buffer + sb->length,
                                 SCI_SYSTEM,
                                 VECTOR_AT (variable->value.array, j));
          sb->length += ((size_t) (p - (sb->buffer + sb->length)));

          if (LIKELY (j != VECTOR_SIZE (variable->value.array) - 1))
            string_builder_append_character (sb, ' ');
        }

      break;
    default:
      ERROR_UNKNOWN_VALUE_TYPE (variable->value);
      break;
    }

  *input += len - 1;
  return true;
}

/* Given a pointer to a string, parse the string extracting fields
   separated by whitespace and optionally enclosed within either single
   or double quotes (which are stripped off), and build a final
   string to pass to spawn.  The input string remains unchanged.

   All of the memory for the pointer array and copies of the string
   is obtained from malloc().

   Returns a true if successful.  Returns
   false if undefined variable found.

   The memory for the string is dynamically expanded as necessary.
   In order to provide a working buffer for extracting arguments into,
   with appropriate stripping of quotes and translation of backslash
   sequences, we allocate a working buffer at least as long as the input
   string.  This ensures that we always have enough space in which to
   work, since the extracted arg is never larger than the input string.  */
static inline bool NONNULL (1, 2, 3, 4) ACCESS (write_only, 4) ACCESS (read_only, 2) ACCESS (read_only, 1)
parse_command (uniqstr input,
               const struct hash_table *variables,
               struct quoting_slots *qs,
               char **result)
{
  bool squote = false;
  bool dquote = false;
  bool bsquote = false;
  struct string_builder sb;

  string_builder_initialize (&sb, strlen (input) + 1);

  while (true)
    {
      while (ascii_isspace (*input))
        input++;

      while (*input != '\0')
        {
          if (ascii_isspace (*input) && !squote && !dquote && !bsquote)
            break;
          else
            {
              if (bsquote)
                {
                  bsquote = false;
                  string_builder_append_character (&sb, *input);
                }
              else if (*input == '\\')
                bsquote = true;
              else if (squote)
                {
                  if (*input == '\'')
                    squote = false;
                  else
                    string_builder_append_character (&sb, *input);
                }
              else if (dquote)
                {
                  if (*input == '"')
                    dquote = false;
                  else if (*input == '@')
                    {
                      input++;

                      if (!string_builder_append_variable_value (&sb, &input, variables, qs))
                        goto lfail;
                    }
                  else
                    string_builder_append_character (&sb, *input);
                }
              else
                {
                  if (*input == '\'')
                    squote = true;
                  else if (*input == '"')
                    dquote = true;
                  else if (*input == '@')
                    {
                      input++;

                      if (!string_builder_append_variable_value (&sb, &input, variables, qs))
                        goto lfail;
                    }
                  else
                    string_builder_append_character (&sb, *input);
                }

              input++;
            }
        }

      while (ascii_isspace (*input))
        input++;

      if (*input == '\0')
        break;

      string_builder_append_character (&sb, ' ');
    }

  sb.buffer[sb.length] = '\0';
  *result = sb.buffer;
  return true;

lfail:
  *result = NULL;
  free (sb.buffer);
  return false;
}

static inline NODISCARD bool NONNULL (1, 2)
command_execute (uniqstr command,
                 const struct hash_table *variables,
                 char **envp,
                 struct quoting_slots *qs,
                 bool dryrun,
                 bool print_command)
{
  bool status = true;
  char *result;

  if (UNLIKELY (!parse_command (command, variables, qs, &result)))
    return false;

  if (print_command)
    puts (result);

  if (!dryrun)
    {
      pid_t pid;

      if (!process_spawn (&pid, result, envp))
        {
          status = false;
          error (_("failed to execute %s"), quote (qs, result));
        }

      if (!process_wait (pid))
        {
          status = false;
          error (_("execution failed of %s"), quote (qs, result));
        }
    }

  free (result);
  return status;
}

static inline NODISCARD bool NONNULL (1, 2)
rule_apply (const struct apply_rule *apply_rule, struct build_context *context, struct quoting_slots *qs)
{
  struct hash_table variables;
  struct variable in = { 0 };
  struct variable out = { 0 };
  struct rule *r = HashFindUniqstr (context->rules, apply_rule->name);

  if (UNLIKELY (r == NULL))
    {
      error (_("can not find rule %s"), quote (qs, apply_rule->name));
      return false;
    }

  variables_duplicate (&variables, &r->variables);

  if (LIKELY (VECTOR_SIZE (apply_rule->in) != 0))
    {
      struct variable_value in_value = { .type = VARIABLE_STRING_ARRAY, .array = apply_rule->in };

      if (UNLIKELY (!variable_initialize (&variables, &in,
                                          uniqstr_new_from_literal_string ("in"), &in_value)))
        return false;;
    }

  if (LIKELY (apply_rule->out != NULL))
    {
      struct variable_value out_value = { .type = VARIABLE_STRING, .string = apply_rule->out };

      if (UNLIKELY (!variable_initialize (&variables, &out,
                                          uniqstr_new_from_literal_string ("out"), &out_value)))
        return false;
    }

  bool status = true;

  if (r->description != NULL)
    {
      char *desc;

      if (LIKELY (parse_description (r->description, &variables, &desc)))
        {
          puts (desc);
          free (desc);
        }
      else
        warning (_("%s: failed to parse rule description"), quote (qs, r->description));
    }

  for (size_t i = 0; i < VECTOR_SIZE (r->commands); i++)
    if (UNLIKELY (!command_execute (VECTOR_AT (r->commands, i), &variables, context->envp, qs,
                                    context->dryrun, r->description == NULL)))
      {
        status = false;
        break;
      }

  if (LIKELY (VECTOR_SIZE (apply_rule->in) != 0))
    HashRemoveUniqstr (&variables, in.name);

  if (LIKELY (apply_rule->out != NULL))
    HashRemoveUniqstr (&variables, out.name);

  HashClearWithDestructor (&variables, variable_destroy);
  return status;
}

struct target_skeleton *
target_skeleton_create (struct hash_table *skeletons,
                        struct uniqstr_array *dependencies,
                        struct uniqstr_array *files,
                        struct uniqstr_array *outfiles,
                        struct apply_rule_array *apply_rules)
{
  struct target_skeleton *skeleton = xcalloc (1, sizeof (*skeleton));

  skeleton->dependencies = *dependencies;
  skeleton->files = *files;
  skeleton->outfiles = *outfiles;
  skeleton->apply_rules = *apply_rules;
  atomic_init (&skeleton->need_rebuild, false);
  HashInsert (skeletons, skeleton, sizeof (*skeleton), skeleton);
  return skeleton;
}

static NODISCARD struct target * NONNULL (1, 2)
target_find (const struct hash_table *targets, uniqstr name)
{
  return HashFindUniqstr (targets, name);
}

static inline bool
inputs_newer_than_output (const struct uniqstr_array *in,
                          const struct uniqstr_array *out,
                          struct quoting_slots *qs)
{
#define timespeccmp(tsp, usp, cmp)     \
  (((tsp).tv_sec == (usp).tv_sec)      \
   ? ((tsp).tv_nsec cmp (usp).tv_nsec) \
   : ((tsp).tv_sec cmp (usp).tv_sec))

  struct timespec outts = { .tv_sec = 0, .tv_nsec = 0 };

  for (size_t i = 0; i < VECTOR_SIZE (*out); i++)
    {
      struct timespec ts = get_modification_time (VECTOR_AT (*out, i), qs);

      if (timespeccmp (outts, ts, <))
        outts = ts;
    }

  for (size_t i = 0; i < VECTOR_SIZE (*in); i++)
    {
      struct timespec ints = get_modification_time (VECTOR_AT (*in, i), qs);

      if (timespeccmp (outts, ints, <=))
        return true;
    }

  return false;

#undef timespeccmp
}

static NODISCARD bool NONNULL (1, 2, 3, 4, 6, 7, 8)
build_tree_create (struct build_tree *root,
                   struct build_tree_branch **pbranch,
                   const struct hash_table *restrict targets,
                   struct hash_table *restrict branches,
                   bool rebuild,
                   uniqstr target_name,
                   struct quoting_slots *qs,
                   struct hash_table *restrict table_for_loop_detection)
{
  struct target *target = target_find (targets, target_name);

  if (UNLIKELY (target == NULL))
    {
      error (_("there is no %s target"), quote (qs, target_name));
      return false;
    }

  if (UNLIKELY (HashFindUniqstr (table_for_loop_detection, target->name) != NULL))
    {
      error (_("dependence loop detected: %s depends on itself"),
             quote (qs, target_name));
      return false;
    }

  HashInsertUniqstr (table_for_loop_detection, target->name, target);

  struct build_tree_branch *branch = HashFindUniqstr (branches, target->name);

  if (LIKELY (branch == NULL))
    {
      branch = xmalloc (sizeof (*branch));
      branch->target = target;

      if (LIKELY (VECTOR_SIZE (target->skeleton->dependencies) != 0))
        {
          branch->branches.count = VECTOR_SIZE (target->skeleton->dependencies);
          branch->branches.array = xmallocarray (branch->branches.count, sizeof (*branch->branches.array));

          for (size_t i = 0; i < VECTOR_SIZE (target->skeleton->dependencies); i++)
            {
              if (UNLIKELY (!build_tree_create (root,
                                                &branch->branches.array[i],
                                                targets,
                                                branches,
                                                rebuild,
                                                VECTOR_AT (target->skeleton->dependencies, i),
                                                qs, table_for_loop_detection)))
                return false;

              target->skeleton->need_rebuild |= branch->branches.array[i]->target->skeleton->need_rebuild;
            }
        }
      else
        {
          branch->branches.count = 0;
          branch->branches.array = NULL;
        }

      for (size_t i = 0; i < VECTOR_SIZE (target->skeleton->files); i++)
        {
          if (target_find (targets, VECTOR_AT (target->skeleton->files, i)) != NULL)
            {
              branch->branches.array = xreallocarray (branch->branches.array,
                                                      (branch->branches.count + 1),
                                                      sizeof (*branch->branches.array));

              if (UNLIKELY (!build_tree_create (root,
                                                &branch->branches.array[branch->branches.count],
                                                targets,
                                                branches,
                                                rebuild,
                                                VECTOR_AT (target->skeleton->files, i),
                                                qs, table_for_loop_detection)))
                return false;

              target->skeleton->need_rebuild |= branch->branches.array[branch->branches.count]->target->skeleton->need_rebuild;
              branch->branches.count++;
            }
        }

      if (rebuild /* First check auto variable.  */
       || (VECTOR_SIZE (target->skeleton->files) == 0 && VECTOR_SIZE (target->skeleton->dependencies) == 0) /* After check members of structures.  */
       || inputs_newer_than_output (&target->skeleton->files, &target->skeleton->outfiles, qs) /* And at the end do the slowest.  */)
        target->skeleton->need_rebuild = true;

      for (size_t i = 0; !target->skeleton->need_rebuild && i < VECTOR_SIZE (target->skeleton->outfiles); i++)
        target->skeleton->need_rebuild |= (access (VECTOR_AT (target->skeleton->outfiles, i), F_OK) != 0);

      root->count_of_targets++;
      HashInsertUniqstr (branches, target->name, branch);
    }

  HashRemoveUniqstr (table_for_loop_detection, target->name);
  *pbranch = branch;
  return true;
}

static NODISCARD bool NONNULL (1, 2)
build (const struct target *target, struct build_context *context, struct quoting_slots *qs)
{
  for (size_t i = 0; i < VECTOR_SIZE (target->skeleton->apply_rules); i++)
    if (UNLIKELY (!rule_apply (&VECTOR_AT (target->skeleton->apply_rules, i), context, qs)))
      return false;

  if (!context->dryrun)
    for (size_t i = 0; i < VECTOR_SIZE (target->skeleton->outfiles); i++)
      if (access (VECTOR_AT (target->skeleton->outfiles, i), F_OK) != 0)
        {
          error (_("target %s must have created file %s"),
                 quote_n (qs, 0, target->name),
                 quote_n (qs, 1, VECTOR_AT (target->skeleton->outfiles, i)));
          return false;
        }

  target->skeleton->need_rebuild = false;
  return true;
}

static bool
build_target (const struct target *target, struct build_context *context, struct quoting_slots *qs)
{
  bool need_rebuild = target->skeleton->need_rebuild;

  need_rebuild |= inputs_newer_than_output (&target->skeleton->files, &target->skeleton->outfiles, qs);

  if (!need_rebuild)
    {
      info (_("nothing to be done for %s"), quote (qs, target->name));
      return true;
    }

  if (LIKELY (build (target, context, qs)))
    {
      /* Do not inform about targets with no rules.  */
      if (VECTOR_SIZE (target->skeleton->apply_rules) != 0)
        info (_("%s: was successfully built"), quote (qs, target->name));

      return true;
    }

  error (_("%s: building failed"), quote (qs, target->name));
  return false;
}

static NODISCARD bool NONNULL (1, 2)
build_tree_traverse (struct build_tree *tree,
                     struct build_context *context,
                     size_t cores_count)
{
  struct job_server job_server;
  bool status = true;

  if (tree->count_of_targets == 0)
    return true;

  if (cores_count == 0)
    cores_count = get_count_of_processor_cores () * 2;

  if (UNLIKELY (!job_server_initialize (&job_server, build_target, cores_count, context)))
    return false;

  job_tree_create_from_build_tree (&job_server, &job_server.job_tree, tree);
  status &= job_server_start (&job_server);
  status &= job_server_finalize (&job_server);
  return status;
}

static void
build_tree_branch_destroy (struct build_tree_branch *branch)
{
  if (branch->branches.count != 0)
    {
      for (size_t i = 0; i < branch->branches.count; i++)
        build_tree_branch_destroy (branch->branches.array[i]);

      free (branch->branches.array);
      branch->branches.count = 0;
    }

  //TODOfree (branch);
}

static void
build_tree_destroy (struct build_tree *tree)
{
  build_tree_branch_destroy (tree->root);
  free (tree->root);
}

struct build_options
{
  uniqstr file;
  uniqstr target;
  uniqstr directory;
  size_t n_jobs;
  bool rebuild:1;
  bool dryrun:1;
  bool graph:1;
  bool verbose:1;
  bool list_targets:1;
};

struct cli_option cli_options[] =
  {
    { "file",      required_argument, NULL, 'f' },
    { "target",    required_argument, NULL, 't' },
    { "directory", required_argument, NULL, 'd' },
    { "jobs",      required_argument, NULL, 'j' },
    { "rebuild",   no_argument,       NULL, 'r' },
    { "dryrun",    no_argument,       NULL, 'D' },
    { "graph",     no_argument,       NULL, 'g' },
    { "verbose",   no_argument,       NULL, 'v' },
    { "list",      no_argument,       NULL, 'l' },
  };

static const char cli_options_string[] = "f:j:t:d:DgrvlqQ";

static inline void
build_options_initialize (struct build_options *o)
{
  o->file = uniqstr_new_from_literal_string ("default.build");
  o->target = uniqstr_new_from_literal_string ("default");
  o->directory = NULL;
  o->n_jobs = 0;
  o->rebuild = false;
  o->dryrun = false;
  o->graph = false;
  o->verbose = false;
  o->list_targets = false;
}

static inline bool ACCESS (write_only, 1) ACCESS (read_only, 2)
string_to_number (size_t *number, const char *string, struct quoting_slots *qs)
{
  size_t x = 0;
  const char *s = string;

  while (((unsigned int) *s - '0') <= 9)
    {
      if (x > SIZE_MAX / 10 || (x * 10 > SIZE_MAX - (size_t) (*s - '0')))
        {
          error (_("number is too big: %s"), string);
          return false;
        }

      x *= 10;
      x += (size_t) (*s - '0');
      s++;
    }

  if (UNLIKELY (*s != '\0'))
    {
      error (_("failed to parse number, invalid string: %s"), quote (qs, string));
      return false;
    }

  *number = x;
  return true;
}

static inline NODISCARD bool
parse_options (struct build_options *o, int argc, char **argv, struct quoting_slots *qs)
{
  int optc = -1;
  struct cli_option_parser_state state;

  cli_option_parser_state_initialize (&state);

  while ((optc = cli_option_parse (&state, argc, argv, cli_options_string,
                                   countof (cli_options), cli_options, NULL)) >= 0)
    {
      switch (optc)
        {
        case 'f':
          o->file = uniqstr_new (state.argument);
          break;
        case 't':
          o->target = uniqstr_new (state.argument);
          break;
        case 'd':
          o->directory = uniqstr_new (state.argument);
          break;
        case 'j':
          if (UNLIKELY (!string_to_number (&o->n_jobs, state.argument, qs)))
            return false;

          break;
        case 'r':
          o->rebuild = true;
          break;
        case 'D':
          o->dryrun = true;
          break;
        case 'g':
          o->graph = true;
          break;
        case 'v':
          o->verbose = true;
          break;
        case 'l':
          o->list_targets = true;
          break;
        default:
          return false;
        }
    }

  return true;
}

static bool
parse_build_file (uniqstr file,
                  struct parse_context *context,
                  struct quoting_slots *qs)
{
  yyscan_t yyscanner;
  YYLTYPE yylocation;
  int status;
  FILE *fp;
  YYETYPE extra;

  if (UNLIKELY (yylex_init (&yyscanner) != 0))
    {
      error (_("failed to initialize flex scanner"));
      return false;
    }

  fp = fopen (file, "r");

  if (UNLIKELY (fp == NULL))
    {
      error (_("%s: can not open file: %s"),
             quotef (qs, file), strerror (errno));
      return false;
    }

  extra.yylocation = &yylocation;
  extra.qs = qs;
  yylocation_initialize (&yylocation, uniqstr_new (quotef (qs, file)));
  extra.buffer.base = xcalloc (16, sizeof (char));
  extra.buffer.size = 16;
  extra.buffer.length = 0;
  yyset_extra (&extra, yyscanner);
  yyset_in (fp, yyscanner);
  yyset_out (stdout, yyscanner);
  status = yyparse (yyscanner, context);
  yylex_destroy (yyscanner);
  free (extra.buffer.base);
  fclose (fp);

  if (UNLIKELY (status != 0))
    {
      error (_("%s: parsing failed"), quotef (qs, file));
      return false;
    }

  return true;
}

static int
target_comparator (const void *p1, const void *p2)
{
  const struct target *t1 = (const struct target *) p1;
  const struct target *t2 = (const struct target *) p2;

  int d = (VECTOR_SIZE (t1->skeleton->outfiles) > VECTOR_SIZE (t2->skeleton->outfiles))
        - (VECTOR_SIZE (t1->skeleton->outfiles) < VECTOR_SIZE (t2->skeleton->outfiles));

  return d != 0 ? d : strcmp (t1->name, t2->name);
}

static inline void
target_list (const struct hash_table *targets)
{
  ARRAY_ENTRY (struct target) array;
  ARRAY_INITIALIZE (array, targets->count);

  struct hash_table_entry *p = HashFirst (targets);

  for (size_t i = 0; i < array.count; i++)
    {
      memcpy (&ARRAY_AT (array, i), HashData (p), sizeof (struct target));
      p = HashNext (p);
    }

  ARRAY_SORT (array, target_comparator);

  for (size_t i = 0; i < array.count; i++)
    printf ("%s target: %s\n",
            VECTOR_SIZE (ARRAY_AT (array, i).skeleton->outfiles) == 0 ? "virtual" : "   file",
            ARRAY_AT (array, i).name);
}

int
main (int argc, char **argv, char **envp)
{
  struct parse_context parse_context;
  struct build_tree tree;
  struct quoting_slots quoting_slots;
  struct build_options options;
  struct hash_table table_for_loop_detection;
  struct hash_table branches;

  setlocale (LC_ALL, "");
  diagnostic_initialize (argv[0]);

  if (UNLIKELY (bindtextdomain (PACKAGE, LOCALEDIR) == NULL))
    warning (_("failed to set base directory for text domain"));

  if (UNLIKELY (textdomain (PACKAGE) == NULL))
    warning (_("failed to set current message domain"));

  quoting_slots_initialize (&quoting_slots);
  uniqstrs_table_create ();
  build_options_initialize (&options);
  parse_context.default_target = NULL;
  HashInit (&parse_context.skeletons);
  HashInit (&parse_context.variables);
  HashInit (&parse_context.rules);
  HashInit (&parse_context.targets);

  if (UNLIKELY (!parse_options (&options, argc, argv, &quoting_slots)))
    return EXIT_FAILURE;

  if (options.directory != NULL)
    if (UNLIKELY (chdir (options.directory) != 0))
      {
        error (_("failed to change directory to %s: %s"),
               quotef (&quoting_slots, options.directory), strerror (errno));
        return EXIT_FAILURE;
      }

  if (options.verbose)
    diagnostic_enable_verbose ();

  if (options.graph)
    {
      if (options.rebuild)
        warning (_("-r option is useless with -g option"));

      if (options.dryrun)
        warning (_("-d option is useless with -g option"));
    }

  if (UNLIKELY (!parse_build_file (options.file, &parse_context, &quoting_slots)))
    return EXIT_FAILURE;

  if (options.list_targets)
    {
      target_list (&parse_context.targets);
      return EXIT_SUCCESS;
    }

  HashInit (&branches);
  HashInit (&table_for_loop_detection);
  tree.count_of_targets = 0;

  uniqstr target = (options.target == NULL
                    ? (parse_context.default_target == NULL
                       ? uniqstr_new_from_literal_string ("default")
                       : parse_context.default_target)
                    : options.target);

  if (UNLIKELY (!build_tree_create (&tree,
                                    &tree.root,
                                    &parse_context.targets,
                                    &branches,
                                    options.rebuild,
                                    target,
                                    &quoting_slots,
                                    &table_for_loop_detection)))
    {
      error (_("failed to create build tree"));
      return EXIT_FAILURE;
    }

  HashClear (&table_for_loop_detection);

  if (options.graph)
    graph (&tree);
  else
    {
      struct build_context build_context =
        {
          &parse_context.rules,
          envp,
          options.dryrun
        };

      if (UNLIKELY (!build_tree_traverse (&tree,
                                          &build_context,
                                          options.n_jobs)))
        {
          error (_("failed to build"));
          return EXIT_FAILURE;
        }
    }

  build_tree_destroy (&tree);
  //TODO musl-gcc's binary segfaults here:
  //HashClearWithDestructor (&branches, free);//<--
  HashClearWithDestructor (&parse_context.targets, free);
  HashClearWithDestructor (&parse_context.rules, rule_destroy);
  HashClearWithDestructor (&parse_context.variables, variable_destroy);
  HashClearWithDestructor (&parse_context.skeletons, target_skeleton_destroy);
  uniqstrs_table_destroy ();
  quoting_slots_finalize (&quoting_slots);
  return EXIT_SUCCESS;
}
