
#pragma once

#include <stddef.h>

#include "vector.h"
#include "utils.h"

#define ARRAY_DECLARE_TYPE(name, type) struct name { size_t count; type *elements; }
#define ARRAY_ENTRY(type) struct { size_t count; type *elements; }

#define ARRAY_COUNT(a0) (a0).count
#define ARRAY_AT(a0, index) (a0).elements[index]

#define ARRAY_INITIALIZE(a0, c)                                  \
  do                                                             \
    {                                                            \
      (a0).count = c;                                            \
      (a0).elements = xmallocarray (c, sizeof (*(a0).elements)); \
    }                                                            \
  while (0)

#define ARRAY_SORT(a0, comparator)                      \
  do                                                    \
    {                                                   \
      qsort ((a0).elements, (a0).count,                 \
             sizeof (*(a0).elements), comparator);      \
    }                                                   \
  while (0)

#define ARRAY_FINALIZE(a0)                      \
  do                                            \
    {                                           \
      free ((a0).elements);                     \
    }                                           \
  while (0)

#define ARRAY_INITIALIZE_FROM_VECTOR(a, v)      \
  do                                            \
    {                                           \
      VECTOR_SHRINK (v);                        \
      (a).count = (v).used;                     \
      (a).elements = (v).buffer;                \
    }                                           \
  while (0)
