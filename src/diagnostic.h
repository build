/* diagnostic.h -- diagnostic output routines declarations.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#pragma once

#include <stdio.h>

#include "yy.h"
#include "system.h"

extern void diagnostic_initialize (const char *progname)
  NONNULL (1);

extern void diagnostic_enable_verbose (void);

extern void yyerror_base (const struct yyltype *yylocation, const char *s)
  NONNULL (1, 2);

extern NORETURN void fatal (const char *f, ...)
  NONNULL (1) FORMAT (printf, 1, 2);

extern void error (const char *f, ...)
  NONNULL (1) FORMAT (printf, 1, 2);

extern void error_at (const struct yyltype *location, const char *f, ...)
  NONNULL (1, 2) FORMAT (printf, 2, 3);

extern void warning (const char *f, ...)
  NONNULL (1) FORMAT (printf, 1, 2);

extern void warning_at (const struct yyltype *location, const char *f, ...)
  NONNULL (1, 2) FORMAT (printf, 2, 3);

extern void info (const char *fmt, ...)
  NONNULL (1) FORMAT (printf, 1, 2);
