/* scan.l -- the build scanner routines.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of thee License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

%{

#include "diagnostic.h"
#include "libquote/quote.h"
#include "parse.h"
#include "system.h"
#include "uniqstr.h"
#include "utils.h"
#include "yy.h"

#define BUFFER_INITIALIZE()                     \
  do                                            \
    {                                           \
      yyextra->buffer.length = 0;               \
      yyextra->buffer.base[0] = '\0';           \
    }                                           \
  while (0)

#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define BUFFER_APPEND_STRING(text, leng)                                \
  do                                                                    \
    {                                                                   \
      if (yyextra->buffer.length + leng >= yyextra->buffer.size)        \
        {                                                               \
          yyextra->buffer.size = MAX (yyextra->buffer.size * 2,         \
                                      yyextra->buffer.size + leng);     \
          yyextra->buffer.base = xrealloc (yyextra->buffer.base,        \
                                           yyextra->buffer.size);       \
        }                                                               \
                                                                        \
      memcpy (yyextra->buffer.base + yyextra->buffer.length, text, leng); \
      yyextra->buffer.length += leng;                                   \
    }                                                                   \
  while (0)

#define BUFFER_FINALIZE()                                               \
  do                                                                    \
    {                                                                   \
      yylval->string = uniqstr_new_from_buffer (yyextra->buffer.base,   \
                                                yyextra->buffer.length); \
    }                                                                   \
  while (0)

#define YY_USER_ACTION                                          \
  do                                                            \
    {                                                           \
      if (!yylocation_compute (yylloc, yytext, yyleng))         \
        return YYTOKEN_YYerror;                                 \
    }                                                           \
  while (0);

#define YY_INPUT(buffer, result, size)                               \
  do                                                                 \
    {                                                                \
      result = do_input (yyscanner, (unsigned char *) buffer, size); \
    }                                                                \
  while (0)

#define YY_NO_UNISTD_H 1
#define YY_STDINIT     0

/* Report a fatal error. */
#if !defined YY_FATAL_ERROR
# define YY_FATAL_ERROR(msg) yy_fatal_error (_(msg), yyscanner)
#endif

static inline NODISCARD int do_input (yyscan_t yyscanner,
                                      unsigned char *buffer, int max_size)
  NONNULL (1, 2) ACCESS (read_only, 2, 3);

%}

%x COMMAND

%option outfile="src/scan.c" header="src/scan.h"
%option bison-bridge bison-locations reentrant
%option warn align ecs 8bit full batch never-interactive pointer line
%option noyywrap nostack nodefault noreject nostdinit noread
%option noyyget_debug noyyset_debug
%option noyyget_lineno noyyset_lineno
%option noyyget_text noyyget_leng
%option noyyget_lval noyyset_lval
%option nounput noyylineno
    //noinput
%option noyy_push_state noyy_pop_state noyy_top_state
%option noyy_scan_buffer noyy_scan_bytes noyy_scan_string

NAME    [a-zA-Z0-9_\-]+
SPACE   [ \t\r\f\v]+
NEWLINE "#"[^\n]*|\n
STRING  \'[^\n\']*\'

/* '\n' may be only at the end of token.  */

%%

{SPACE}|{NEWLINE} { continue; }

"{"|"}"|"["|"]"|"("|")"|"="|":"|"@"|"="|"~"|"."|"," { return yytext[0]; }

"+="          { return YYTOKEN_CATENATE; }
"->"          { return YYTOKEN_ARROW; }
"rule"        { return YYTOKEN_RULE; }
"target"      { return YYTOKEN_TARGET; }
"description" { return YYTOKEN_DESCRIPTION; }
"file"        { return YYTOKEN_FILE; }
"let"         { return YYTOKEN_LET; }
"default"     { return YYTOKEN_DEFAULT; }
"directory"   { return YYTOKEN_DIRECTORY; }
"map"         { return YYTOKEN_MAP; }
"filter"      { return YYTOKEN_FILTER; }

"/*" {
  int c;

  while (true)
    {
      while ((c = input (yyscanner)) != '*' && c != EOF)
        continue;

      if (c == '*')
        {
          while ((c = input (yyscanner)) == '*')
            continue;

          /* At end of comment.  */
          if (c == '/')
            break;
        }

      if (c == EOF)
        {
          error (_("end of file encountered in the comment"));
          return YYTOKEN_YYerror;
        }
    }
}

> {
  BUFFER_INITIALIZE ();
  BEGIN (COMMAND);
}

<COMMAND>{
  [^\\\n]+|\\+[^\n] {
    BUFFER_APPEND_STRING (yytext, (size_t) yyleng);
  }

  [\\][[:space:]]*[\r]?[\n][[:space:]]*[>] {
    continue;
  }

  \n {
    BUFFER_FINALIZE ();
    BEGIN (INITIAL);
    return YYTOKEN_COMMAND;
  }
}

@{NAME} {
  yylval->string = uniqstr_new_from_buffer (yytext + 1, (size_t) (yyleng - 1));
  return YYTOKEN_VALUE;
}

{NAME} {
  yylval->string = uniqstr_new_from_buffer (yytext, (size_t) yyleng);
  return YYTOKEN_NAME;
}

{STRING} {
  yylval->string = uniqstr_new_from_buffer (yytext + 1, (size_t) (yyleng - 2));
  return YYTOKEN_STRING;
}

<INITIAL,COMMAND>{
  [\x00-\x7F]|[\xC2-\xDF][\x80-\xBF]|\xE0[\xA0-\xBF][\x80-\xBF]|[\xE1-\xEC][\x80-\xBF][\x80-\xBF]|\xED[\x80-\x9F][\x80-\xBF]|[\xEE\xEF][\x80-\xBF][\x80-\xBF]|\xF0[\x90-\xBF][\x80-\xBF][\x80-\xBF]|[\xF1-\xF3][\x80-\xBF][\x80-\xBF][\x80-\xBF]|\xF4[\x80-\x8F][\x80-\xBF][\x80-\xBF] {
    /* https://stackoverflow.com/questions/921648/how-to-make-a-flex-lexical-scanner-to-read-utf-8-characters-input#comment73101581_921648 */
    /* Do not parse anything else, just say that we do not know
       what to do with this input.  */
    error_at (yylloc_param, _("unexpected token: %s"),
              quote_memory (yyextra->qs, yytext, (size_t) yyleng));
    return YYTOKEN_YYerror;
  }

  . {
    /* Something may not be cought by rule above,
       so fallback here and notice that.  */
    error_at (yylloc_param, _("unexpected invalid UTF-8 token: %s"),
              quote_memory (yyextra->qs, yytext, (size_t) yyleng));
    return YYTOKEN_YYerror;
  }
}

%%

static inline int
do_input (yyscan_t yyscanner, unsigned char *buffer, int max_size)
{
  int n = 0;
  FILE *in = yyget_in (yyscanner);
  YYLTYPE *yylocation = yyget_extra (yyscanner)->yylocation;

  while (n < max_size)
    {
      int c = fgetc (in);

      if (UNLIKELY (c == EOF))
        {
          if (UNLIKELY (ferror (in) != 0))
            YY_FATAL_ERROR ("input in flex scanner failed");

          break;
        }

      buffer[n++] = (unsigned char) c;

      if (UNLIKELY (c == '\n'))
        {
          const file_offset_t next_newline_offset = file_get_offset (yyget_in (yyscanner));

          yylocation->newline_offset = yylocation->next_newline_offset;
          yylocation->next_newline_offset = next_newline_offset;

          if (UNLIKELY (next_newline_offset < 0))
            warning (_("failed to get offset of the newline: %s"),
                     strerror (errno));

          break;
        }
    }

  return n;
}
