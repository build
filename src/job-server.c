/* job-server.c -- implementation of list-based job-server routines.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#include <stdlib.h>

#include "build.h"
#include "diagnostic.h"
#include "job-server.h"
#include "system.h"
#include "thread.h"
#include "utils.h"

bool
job_server_initialize (struct job_server *job_server,
                       build_target_t routine,
                       size_t threads_count,
                       struct build_context *build_context)
{
  semaphore_create (&job_server->ready_jobs.semaphore, 0);
  llist_initialize_head (&job_server->ready_jobs.head);
  atomic_init (&job_server->waiting_jobs.count, 0);
  ARRAY_INITIALIZE (job_server->threads, threads_count);
  job_server->routine = routine;
  job_server->build_context = *build_context;
  atomic_init (&job_server->failed, false);
  semaphore_create (&job_server->finished, 0);
  return true;
}

static inline void
__job_put (struct job_server *job_server, struct job *job, bool thread_safe)
{
  struct job_llist_node *job_llist_node = xmalloc (sizeof (*job_llist_node));

  job_llist_node->job = job;
  job_llist_node->node.next = NULL;
  (thread_safe ? llist_add : __llist_add) (&job_llist_node->node, &job_server->ready_jobs.head);
  semaphore_post (&job_server->ready_jobs.semaphore);
}

static inline void
job_put_unsafe (struct job_server *job_server, struct job *job)
{
  __job_put (job_server, job, false);
}

static inline void
job_put (struct job_server *job_server, struct job *job)
{
  __job_put (job_server, job, true);
}

static inline struct job *
job_get (struct job_server *job_server)
{
  struct job_llist_node *job_node;
  struct job *job;
  bool finished = false;

  {
    size_t count;

    do
      {
        count = atomic_load (&job_server->waiting_jobs.count);

        if (count == 0)
          {
            finished = true;
            break;
          }
      }
    while (!atomic_compare_exchange_weak (&job_server->waiting_jobs.count, &count, count - 1));
  }

  if (UNLIKELY (finished || atomic_load (&job_server->failed)))
    return NULL;

  /* May suspend thread, so we shall handle 'failed' variable after that
     and we shall do semaphore_post() on same semaphore on failure to unlock
     semaphore here in different threads.  */
  semaphore_wait (&job_server->ready_jobs.semaphore);

  /* We could be woken up on failure, so check again 'failed'.  */
  if (atomic_load (&job_server->failed))
    return NULL;

  /* NOTE: llist_del_first() will surely return non-null.  */
  job_node = llist_entry (llist_del_first (&job_server->ready_jobs.head), struct job_llist_node, node);
  job = job_node->job;
  free (job_node);
  return job;
}

static inline void
job_tree_add_relative (struct job *relative, struct job_list_head *head)
{
  struct job_list_node *node = xmalloc (sizeof (*node));

  node->job = relative;
  node->next = head->first;
  head->first = node;
}

static inline struct job *
job_tree_add_branch_recursive (struct job_server *job_server,
                               struct job *root,
                               struct job *parent,
                               struct hash_table *jobs,
                               struct hash_table *children,
                               struct build_tree_branch *branch)
{
  struct target *target = branch->target;
  struct job *job;

  job = HashFindUniqstr (children, target->name);

  if (UNLIKELY (job != NULL))
    return job;

  job = HashFindUniqstr (jobs, target->name);

  if (UNLIKELY (job != NULL))
    {
      /* Just add a new parent for a job.  */
      job_tree_add_relative (parent, &job->parents.head);
      return job;
    }

  job = xmalloc (sizeof (*job));
  HashInsertUniqstr (children, target->name, job);
  HashInsertUniqstr (jobs, target->name, job);
  job->target = target;
  job->parents.head.first = NULL;
  job->children.head.first = NULL;
  mutex_create (&job->parents.mutex);
  mutex_create (&job->children.mutex);

  if (LIKELY (parent != NULL))
    job_tree_add_relative (parent, &job->parents.head);

  if (LIKELY (branch->branches.count != 0))
    {
      for (size_t i = 0; i < branch->branches.count; i++)
        {
          struct job *child;
          struct hash_table childs;

          HashInit (&childs);
          child = job_tree_add_branch_recursive (job_server, root, job, jobs,
                                                 &childs, branch->branches.array[i]);
          HashClear (&childs);
          job_tree_add_relative (child, &job->children.head);
        }
    }
  else
    {
      /* Job is independent, so can be built immidiately.  */
      job_put_unsafe (job_server, job);
    }

  /* No lock required.  */
  job_server->waiting_jobs.count++;
  return job;
}

void
job_tree_create_from_build_tree (struct job_server *job_server,
                                 struct job *root,
                                 struct build_tree *tree)
{
  struct hash_table jobs;
  struct hash_table children;

  HashInit (&jobs);
  HashInit (&children);
  job_tree_add_branch_recursive (job_server, root, NULL, &jobs, &children, tree->root);
  HashClear (&children);
  HashClear (&jobs);
}

static inline void
job_tree_delete_node (struct job_server *job_server, struct job *job)
{
  mutex_acquire (&job->parents.mutex);

  {
    struct job_list_node **indirect = &job->parents.head.first;

    while (*indirect != NULL)
      {
        bool childless;

        mutex_acquire (&(*indirect)->job->children.mutex);

        {
          struct job_list_node **i = &(*indirect)->job->children.head.first;

          while (*i != NULL && (*i)->job != job)
            i = &(*i)->next;

          if (*i != NULL)
            {
              struct job_list_node *n = (*i)->next;

              free (*i);
              *i = n;
            }

          childless = ((*indirect)->job->children.head.first == NULL);
        }

        mutex_release (&(*indirect)->job->children.mutex);

        if (childless)
          {
            /* Remove node from tree and put it to ready jobs queue.  */
            struct job_list_node *n = (*indirect)->next;

            job_put (job_server, (*indirect)->job);
            free (*indirect);
            *indirect = n;
          }
        else
          indirect = &(*indirect)->next;
      }
  }

  mutex_release (&job->parents.mutex);
}

static void *
job_server_main (void *p)
{
  struct job_server *job_server = p;
  thread_status_t status = THREAD_SUCCESS;
  struct build_context context;
  struct quoting_slots qs;

  context = job_server->build_context;
  quoting_slots_initialize (&qs);

  while (true)
    {
      struct job *job;
      struct target *target;

      if (UNLIKELY (atomic_load (&job_server->failed)))
        break;

      job = job_get (job_server);

      /* Either building failed, or just no more targets for this thread.  */
      if (UNLIKELY (job == NULL))
        break;

      target = job->target;

      if (UNLIKELY (!job_server->routine (target, &context, &qs)))
        {
          status = THREAD_FAILURE;
          atomic_store (&job_server->failed, true);
          break;
        }

      job_tree_delete_node (job_server, job);
      free (job);
    }

  semaphore_post (&job_server->finished);
  quoting_slots_finalize (&qs);
  return status;
}

bool
job_server_start (struct job_server *job_server)
{
  if (ARRAY_COUNT (job_server->threads) == 1)
    return job_server_main (job_server);

  for (size_t i = 0; i < ARRAY_COUNT (job_server->threads); i++)
    if (!thread_create (&ARRAY_AT (job_server->threads, i), job_server_main, job_server))
      {
        atomic_store (&job_server->failed, true);
        return false;
      }

  return true;
}

bool
job_server_finalize (struct job_server *job_server)
{
  bool status = true;

  if (ARRAY_COUNT (job_server->threads) == 1)
    return true;

  for (size_t i = 0; i < ARRAY_COUNT (job_server->threads); i++)
    {
      semaphore_wait (&job_server->finished);

      if (UNLIKELY (atomic_load (&job_server->failed)))
        break;
    }

  if (UNLIKELY (atomic_load (&job_server->failed)))
    {
      /* Unlock all waiters to be sure that we can join all threads.  */
      semaphore_post_all (&job_server->ready_jobs.semaphore);

      for (size_t i = 0; i < ARRAY_COUNT (job_server->threads); i++)
        thread_join (&ARRAY_AT (job_server->threads, i), NULL);

      status = false;
    }
  else
    for (size_t i = 0; i < ARRAY_COUNT (job_server->threads); i++)
      {
        void *result;

        status &= thread_join (&ARRAY_AT (job_server->threads, i), &result);
        status &= (result == THREAD_SUCCESS);
      }

  semaphore_destroy (&job_server->ready_jobs.semaphore);
  semaphore_destroy (&job_server->finished);
  ARRAY_FINALIZE (job_server->threads);
  return status;
}
