/*
** 2001 September 22
**
** The author disclaims copyright to this source code.  In place of
** a legal notice, here is a blessing:
**
**    May you do good and not evil.
**    May you find forgiveness for yourself and forgive others.
**    May you share freely, never taking more than you give.
**
*************************************************************************
** This is the header file for the generic hash-table implementation
** used in SQLite.
*/
#ifndef SQLITE_HASH_H
#define SQLITE_HASH_H

#include <stddef.h>
#include <stdint.h>

#include "system.h"

/* Forward declarations of structures. */
struct hash_table_entry;

/*
 * A complete hash table is an instance of the following structure.
 * The internals of this structure are intended to be opaque -- client
 * code should not attempt to access or modify the fields of this structure
 * directly.  Change this structure only by using the routines below.
 * However, some of the "procedures" and "functions" for modifying and
 * accessing this structure are really macros, so we can not really make
 * this structure opaque.
 *
 * All elements of the hash table are on a single doubly-linked list.
 * Hash.first points to the head of this list.
 *
 * There are Hash.size buckets.  Each bucket points to a spot in
 * the global doubly-linked list.  The contents of the bucket are the
 * element pointed to plus the next table.count-1 elements in the list.
 *
 * Hash.size and Hash.table may be zero.  In that case lookup is done
 * by a linear search of the global list.  For small tables, the
 * Hash.table table is never allocated because if there are few elements
 * in the table, it is faster to do a linear search than to manage
 * the hash table.
 */
struct hash_table
{
  size_t size;                      /* Number of buckets in the hash table.  */
  size_t count;                     /* Number of entries in this table.  */
  struct hash_table_entry *first;   /* The first element of the array.  */
  struct hash_table_bucket          /* The hash table.  */
  {
    struct hash_table_entry *chain; /* Pointer to first entry with this hash.  */
    size_t count;                   /* Number of entries with this hash.  */
  } *table;
};

/*
 * Each element in the hash table is an instance of the following
 * structure.  All elements are stored on a single doubly-linked list.
 *
 * Again, this structure is intended to be opaque, but it can not really
 * be opaque because it is used by macros.
 */
struct hash_table_entry
{
  struct hash_table_entry *next, *prev; /* Next and previous elements in the table.  */
  void *data;         /* Data associated with this element.  */
  const char *keyPtr; /* Key associated with this element.  */
  size_t keySize;     /* Size of the Key associated with this element.  */
  size_t keyHash;     /* Hash of the Key associated with this element.  */
};

/*
 * Access routines.
 */
void HashInit (struct hash_table *)
  NONNULL (1) ACCESS (write_only, 1);
void *HashInsert (struct hash_table *, const void *keyPtr, size_t keySize, void *pData)
  NONNULL (1, 2, 4) ACCESS (read_only, 2, 3) ACCESS (none, 4) RETURNS_NONNULL;
void *HashInsertNoReplace (struct hash_table *pH, const void *keyPtr, size_t keySize, void *data)
  NONNULL (1, 2, 4) ACCESS (read_only, 2, 3) ACCESS (none, 4) RETURNS_NONNULL;
void *HashRemove (struct hash_table *, const void *keyPtr, size_t keySize)
  NONNULL (1, 2) ACCESS (read_only, 1) ACCESS (read_only, 2, 3);
void *HashFind (const struct hash_table *, const void *keyPtr, size_t keySize)
  NONNULL (1, 2) PURE ACCESS (read_only, 2, 3);
void HashClear (struct hash_table *) NONNULL (1);
void HashClearWithDestructor (struct hash_table *, void (*) (void *)) NONNULL (1, 2);

/*
 * Macros for looping over all elements of a hash table.  The idiom is
 * like this:
 *
 *   struct hash_table h;
 *   struct hash_table_entry *p;
 *   ...
 *   for (p = HashFirst (&h); p != NULL; p = HashNext (p))
 *     {
 *       SomeStructure *pData = HashData (p);
 *       // do something with pData
 *     }
 */
#define HashFirst(H)   ((H)->first)
#define HashNext(E)    ((E)->next)
#define HashData(E)    ((E)->data)
#define HashKeyPtr(E)  ((E)->keyPtr)
#define HashKeySize(E) ((E)->keySize)

/*
 * Number of entries in a hash table
 */
#define HashCount(H) ((H)->count)

#endif /* SQLITE_HASH_H */
