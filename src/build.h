/* build.h -- type definitions and declarations of some common
   functions.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#pragma once

#include "array.h"
#include "vector.h"
#include "hash.h"
#include "system.h"
#include "thread.h"
#include "uniqstr.h"

VECTOR_DECLARE_TYPE (uniqstr_array, uniqstr);
VECTOR_DECLARE_TYPE (apply_rule_array, struct apply_rule);

struct variable_value
{
  enum { VARIABLE_STRING, VARIABLE_STRING_ARRAY } type;
  union
  {
    uniqstr string;
    VECTOR_ENTRY (uniqstr_array) array;
  };
};

#define ERROR_UNKNOWN_VALUE_TYPE(value) \
  error (_("unknown value type with number %u"), (value).type)

struct variable
{
  uniqstr name;
  struct variable_value value;
};

struct rule
{
  uniqstr name;
  uniqstr description;
  VECTOR_ENTRY (uniqstr_array) commands;
  struct hash_table variables;
};

struct apply_rule
{
  uniqstr name;
  VECTOR_ENTRY (uniqstr_array) in;
  uniqstr out;
};

struct target_skeleton
{
  VECTOR_ENTRY (uniqstr_array) dependencies;
  VECTOR_ENTRY (uniqstr_array) files;
  VECTOR_ENTRY (uniqstr_array) outfiles;
  VECTOR_ENTRY (apply_rule_array) apply_rules;
  atomic_bool need_rebuild;
};

struct target
{
  uniqstr name;
  struct target_skeleton *skeleton;
};

struct build_tree_branch
{
  struct target *target;
  struct
  {
    size_t count;
    struct build_tree_branch **array;
  } branches;
};

struct build_tree
{
  size_t count_of_targets;
  struct build_tree_branch *root;
};

struct build_context
{
  const struct hash_table *rules;
  char **envp;
  bool dryrun;
};

struct parse_context
{
  struct hash_table variables;
  struct hash_table rules;
  struct hash_table skeletons;
  struct hash_table targets;

  uniqstr default_target;

  /* Temporary variables.  */
  struct variable target;
  struct variable dependencies;
  struct variable files;
};

extern NODISCARD bool variable_create (struct hash_table *variables, uniqstr name,
                                       struct variable_value *value)
  NONNULL (1, 2, 3);


extern NODISCARD struct target_skeleton *target_skeleton_create (struct hash_table *skeletons,
                                                                 struct uniqstr_array *dependencies,
                                                                 struct uniqstr_array *files,
                                                                 struct uniqstr_array *outfiles,
                                                                 struct apply_rule_array *apply_rules)
  NONNULL (1, 2, 3, 4, 5);

extern NODISCARD bool target_create (struct target **t,
                                     struct hash_table *targets,
                                     uniqstr name,
                                     struct target_skeleton *skeleton,
                                     struct quoting_slots *qs)
  NONNULL (2, 3, 4, 5);

extern NODISCARD bool rule_create (struct hash_table *restrict rules,
                                   uniqstr name,
                                   uniqstr description,
                                   struct uniqstr_array *commands,
                                   const struct hash_table *restrict variables)
  NONNULL (1, 2, 4, 5);
