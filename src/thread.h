/* thread.h -- system-independent thread types and functions declarations.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#pragma once

#ifdef _WIN32
# include <windows.h>
#else
# include <limits.h>
# include <semaphore.h>
# include <pthread.h>
#endif

#include "system.h"

#define THREAD_SUCCESS ((thread_status_t) EXIT_SUCCESS)
#define THREAD_FAILURE ((thread_status_t) EXIT_FAILURE)

typedef void *thread_status_t;
typedef thread_status_t (*thread_routine_t) (void *);

struct thread
{
#ifdef _WIN32
  void *argument;
  void *result;
  thread_routine_t routine;
  HANDLE thread;
#else
  pthread_t thread;
#endif
};

struct mutex
{
#ifdef _WIN32
  SRWLOCK mutex;
#else
  pthread_mutex_t mutex;
#endif
};

// For now i use the hand-written semaphores to use it as a counter of ready jobs in list,
// perhaps there is a better way to do kinda same, but for now we do how we do

#define USE_OWN_SEMAPHORE 1

#if USE_OWN_SEMAPHORE
struct condition_variable
{
#ifdef _WIN32
  CONDITION_VARIABLE condition_variable;
#else
  pthread_cond_t condition_variable;
#endif
};
#endif

/* Create our own semaphore implementation motivated using it as a counter with locking on zero.
   Perhaps... It is not a good way, but worth is better, an anarchy is the only way, someone will come
   and do it much better, but not me.  */
struct semaphore
{
#if USE_OWN_SEMAPHORE
  // Finally it is done, using 64 bit counter to be sure that no overflow will come.
  uint64_t count;
  uint64_t wait_count;
  struct mutex mutex;
  struct condition_variable condition_variable;
#else
# ifdef _WIN32
  HANDLE semaphore;
# else
  sem_t semaphore;
# endif
#endif
};

struct rwlock
{
#ifdef _WIN32
  SRWLOCK rwlock;
#else
  pthread_rwlock_t rwlock;
#endif
};

extern NODISCARD bool thread_create (struct thread *tid, thread_routine_t routine, void *arg) NONNULL (1, 2, 3);
extern bool thread_join (struct thread *tid, void **result) NONNULL (1);
extern NORETURN void thread_exit (void *result) ACCESS (none, 1);

extern void mutex_create (struct mutex *mutex) NONNULL (1);
extern void mutex_acquire (struct mutex *mutex) NONNULL (1);
extern void mutex_release (struct mutex *mutex) NONNULL (1);
extern void mutex_destroy (struct mutex *mutex) NONNULL (1);

extern void semaphore_create (struct semaphore *semaphore, size_t initial_value) NONNULL (1);
extern void semaphore_wait (struct semaphore *semaphore) NONNULL (1);
extern void semaphore_post (struct semaphore *semaphore) NONNULL (1);
extern void semaphore_post_all (struct semaphore *semaphore) NONNULL (1);
extern void semaphore_destroy (struct semaphore *semaphore) NONNULL (1);

extern void rwlock_create (struct rwlock *rwlock) NONNULL (1);
extern void rwlock_read_acquire (struct rwlock *rwlock) NONNULL (1);
extern void rwlock_read_release (struct rwlock *rwlock) NONNULL (1);
extern void rwlock_write_acquire (struct rwlock *rwlock) NONNULL (1);
extern void rwlock_write_release (struct rwlock *rwlock) NONNULL (1);
extern void rwlock_destroy (struct rwlock *rwlock) NONNULL (1);
