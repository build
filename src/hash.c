/*
 * 2001 September 22
 *
 * The author disclaims copyright to this source code.  In place of
 * a legal notice, here is a blessing:
 *
 *    May you do good and not evil.
 *    May you find forgiveness for yourself and forgive others.
 *    May you share freely, never taking more than you give.
 *
 ********************************************************************************
 * This is the implementation of generic hash-tables
 * used in SQLite.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"
#include "utils.h"

/*
 * Turn bulk memory into a hash table object by initializing the
 * fields of the struct hash_table structure.
 *
 * "pNew" is a pointer to the hash table that is to be initialized.
 */
void
HashInit (struct hash_table *pNew)
{
#if 0
  assert (pNew != NULL);
#endif
  pNew->first = 0;
  pNew->count = 0;
  pNew->size = 0;
  pNew->table = 0;
}

/*
 * Remove all entries from a hash table and may be call destructor for all data
 * in entries.  Reclaim all memory.
 * Call this routine to delete a hash table or to reset a hash table
 * to the empty state.
 */
static inline ALWAYS_INLINE void
HashClearMayBeWithDestructor (struct hash_table *pH, void (*pD) (void *))
{
  struct hash_table_entry *entry; /* For looping over all entries of the table.  */

#if 0
  assert (pH != NULL);
#endif
  entry = pH->first;
  pH->first = NULL;
  free (pH->table);
  pH->table = NULL;
  pH->size = 0;

  while (entry != NULL)
    {
      struct hash_table_entry *next_entry = entry->next;

      if (pD != NULL)
        pD (entry->data);

      free (entry);
      entry = next_entry;
    }

  pH->count = 0;
}

void
HashClear (struct hash_table *pH)
{
  HashClearMayBeWithDestructor (pH, NULL);
}

void
HashClearWithDestructor (struct hash_table *pH, void (*pD) (void *))
{
  HashClearMayBeWithDestructor (pH, pD);
}

/*
 * The hashing function.
 */
static inline ALWAYS_INLINE size_t
strHash (const char *z, size_t n)
{
  size_t h = 0;
  unsigned char c;
  const char *p = z + n;

  while (z < p)
    { /*OPTIMIZATION-IF-TRUE*/
      /*
       * Knuth multiplicative hashing.  (Sorting & Searching, p. 510).
       * 0x9E3779B1 is 2654435761 which is the closest prime number to
       * (2**32)*golden_ratio, where golden_ratio = (sqrt(5) - 1)/2.
       */
      c = *(const unsigned char *) z;
      h += c;
      h *= 0x9E3779B1;
      z++;
    }

  return h;
}

/*
 * Link pNew entry into the hash table pH.  If pBucket!=0 then also
 * insert pNew into the pBucket hash bucket.
 */
static inline ALWAYS_INLINE void
insertEntry (struct hash_table *pH,             /* The complete hash table.  */
             struct hash_table_bucket *pBucket, /* The bucket into which pNew is inserted.  */
             struct hash_table_entry *pNew)     /* The entry to be inserted.  */
{
  struct hash_table_entry *pHead; /* First entry already in pBucket.  */

  if (pBucket != NULL)
    {
      pHead = pBucket->count != 0 ? pBucket->chain : NULL;
      pBucket->count++;
      pBucket->chain = pNew;
    }
  else
    {
      pHead = NULL;
    }

  if (pHead != NULL)
    {
      pNew->next = pHead;
      pNew->prev = pHead->prev;

      if (pHead->prev != NULL)
        {
          pHead->prev->next = pNew;
        }
      else
        {
          pH->first = pNew;
        }

      pHead->prev = pNew;
    }
  else
    {
      pNew->next = pH->first;

      if (pH->first != NULL)
        {
          pH->first->prev = pNew;
        }

      pNew->prev = 0;
      pH->first = pNew;
    }
}

/*
 * Resize the hash table so that it cantains "new_size" buckets.
 */
static inline ALWAYS_INLINE void
rehash (struct hash_table *pH, size_t new_size)
{
  struct hash_table_bucket *new_table;
  struct hash_table_entry *entry, *next_entry; /* For looping over existing entries.  */

  new_table = xreallocarray (pH->table, new_size, sizeof (*new_table));
  memset (new_table, '\0', new_size * sizeof (*new_table));
  pH->table = new_table;
  pH->size = new_size;

  for (entry = pH->first, pH->first = NULL; entry != NULL; entry = next_entry)
    {
      next_entry = entry->next;
      insertEntry (pH, &new_table[entry->keyHash % new_size], entry);
    }
}

/*
 * This function (for internal use only) locates an entry in an
 * hash table that matches the given key.  If no entry is found,
 * a pointer to a static null entry with entry.data==0 is returned.
 * If pHash is not NULL, then the hash for this key is written to *pHash.
 */
static inline ALWAYS_INLINE struct hash_table_entry *
findEntryWithHash (const struct hash_table *pH, /* The pH to be searched.  */
                   const void *keyPtr,          /* The key we are searching for.  */
                   size_t keySize,              /* The size of key.  */
                   size_t *pHash)               /* Write the hash value here.  */
{
  struct hash_table_entry *entry; /* Used to loop thru the entry list.  */
  size_t count; /* Number of entry left to test.  */
  static struct hash_table_entry nullEntry = { 0, 0, 0, 0, 0, 0 };

  if (LIKELY (pH->table != NULL))
    { /*OPTIMIZATION-IF-TRUE*/
      struct hash_table_bucket *pBucket;
      size_t h;

      h = strHash (keyPtr, keySize);
      pBucket = &pH->table[h % pH->size];
      entry = pBucket->chain;
      count = pBucket->count;

      if (pHash != NULL)
        *pHash = h;
    }
  else
    {
      entry = pH->first;
      count = pH->count;

      if (pHash != NULL)
        *pHash = strHash (keyPtr, keySize);
    }

  while (count-- != 0)
    {
      assert (entry != NULL);

      if (entry->keySize == keySize && memcmp (entry->keyPtr, keyPtr, keySize) == 0)
        return entry;

      entry = entry->next;
    }

  return &nullEntry;
}

/*
 * Remove a single entry from the hash table given a pointer to that
 * entry and a hash on the entry's key.
 */
static inline ALWAYS_INLINE void
removeEntryGivenHash (struct hash_table *pH,          /* The pH containing "entry".  */
                      struct hash_table_entry *entry, /* The entry to be removed from the pH.  */
                      size_t h)                     /* Hash value for the entry.  */
{
  struct hash_table_bucket *pBucket;

  if (entry->prev != NULL)
    {
      assert (entry->prev->next == entry);
      entry->prev->next = entry->next;
    }
  else
    {
      assert (pH->first == entry);
      pH->first = entry->next;
    }

  if (entry->next != NULL)
    {
      assert (entry->next->prev == entry);
      entry->next->prev = entry->prev;
    }

  if (pH->table != NULL)
    {
      pBucket = &pH->table[h % pH->size];

      if (pBucket->chain == entry)
        pBucket->chain = entry->next;

      assert (pBucket->count != 0);
      pBucket->count--;
    }

  free (entry);
  pH->count--;

  if (pH->count == 0)
    {
      assert (pH->first == 0);
      assert (pH->count == 0);
      HashClear (pH);
    }
}

/*
 * Attempt to locate an entry of the hash table pH with a key
 * that matches keyPtr.  Return the data for this entry if it is
 * found, or NULL if there is no match.
 */
void *
HashFind (const struct hash_table *pH, const void *keyPtr, size_t keySize)
{
#if 0
  assert (pH != NULL);
  assert (keyPtr != NULL);
#endif
  return findEntryWithHash (pH, keyPtr, keySize, 0)->data;
}

static inline ALWAYS_INLINE struct hash_table_entry *
createEntry (const void *keyPtr, size_t keySize, void *data)
{
  struct hash_table_entry *new_entry = xmalloc (sizeof (struct hash_table_entry));

  new_entry->keyPtr = keyPtr;
  new_entry->keySize = keySize;
  new_entry->data = data;
  return new_entry;
}

/*
 * Insert an entry into the hash table "pH".  The key is "keyPtr"
 * and the data is "data".
 *
 * If no entry exists with a matching key, then a new
 * entry is created and data is returned.
 *
 * If another entry already exists with the same key, then
 * (depending on "replace") the new data replaces the old data
 * and the old data is returned.  The key is not copied in this
 * instance.  If a malloc fails, then the NULL is returned and
 * the hash table is unchanged.
 */
static inline ALWAYS_INLINE void *
HashInsertMayBeReplace (struct hash_table *pH, const void *keyPtr, size_t keySize, void *data, bool replace)
{
  size_t h;                         /* The hash of the key modulo hash table size.  */
  struct hash_table_entry *entry;     /* Used to loop thru the entry list.  */
  struct hash_table_entry *new_entry; /* New entry added to the pH.  */

#if 0
  assert (pH != NULL);
  assert (keyPtr != NULL);
  assert (data != NULL);
#endif
  entry = findEntryWithHash (pH, keyPtr, keySize, &h);

  if (entry->data != NULL)
    {
      void *old_data = entry->data;

      if (replace)
        {
          entry->data = data;
          entry->keyPtr = keyPtr;
          entry->keySize = keySize;
          entry->keyHash = h;
        }

      return old_data;
    }

  new_entry = createEntry (keyPtr, keySize, data);
  pH->count++;

  if (pH->count >= 16 && pH->count > 2 * pH->size)
    {
      rehash (pH, pH->count * 2);
      assert (pH->size > 0);
      h = strHash (keyPtr, keySize);
    }

  new_entry->keyHash = h;
  insertEntry (pH, pH->table != NULL ? &pH->table[h % pH->size] : NULL, new_entry);
  return data;
}

void *
HashInsert (struct hash_table *pH, const void *keyPtr, size_t keySize, void *data)
{
  return HashInsertMayBeReplace (pH, keyPtr, keySize, data, true);
}

void *
HashInsertNoReplace (struct hash_table *pH, const void *keyPtr, size_t keySize,
                     void *data)
{
  return HashInsertMayBeReplace (pH, keyPtr, keySize, data, false);
}

/*
 * The entry corresponding to "key" is removed from the hash table.
 * The key is keyPtr and the data is "data".
 *
 * If no entry exists with a matching key, then a nothing
 * is removed and NULL is returned.
 */
void *
HashRemove (struct hash_table *pH, const void *keyPtr, size_t keySize)
{
  size_t h;                       /* The hash of the key modulo hash table size.  */
  struct hash_table_entry *entry; /* Used to loop thru the entry list.  */

#if 0
  assert (pH!=0);
  assert (keyPtr != 0);
#endif
  entry = findEntryWithHash (pH, keyPtr, keySize, &h);

  if (entry->data != NULL)
    {
      void *old_data = entry->data;

      removeEntryGivenHash (pH, entry, h);
      return old_data;
    }

  return NULL;
}
