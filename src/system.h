/* system.h -- system-independent miscellaneous defines.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of the License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

#pragma once

#if !defined (_WIN32) && !defined (_POSIX_C_SOURCE)
# error unsupported host.
#endif

#define bool  _Bool
#define true  (bool) +1u
#define false (bool) +0u

#define alignas(alignment)  _Alignas (alignment)
#define alignof(expression) _Alignof (expression)

#if !defined (static_assert)
# define static_assert _Static_assert
#endif

#ifdef _WIN32
# define PRIuSIZE "%Iu"
#else
# define PRIuSIZE "%zu"
#endif

#ifdef _WIN32
# define file_offset_t   __int64
# define file_set_offset _fseeki64
# define file_get_offset _ftelli64
#else
# include <sys/types.h>
# define file_offset_t   off_t
# define file_set_offset fseeko
# define file_get_offset ftello
#endif

#ifdef _WIN32
# include <windows.h>
#endif

#if (defined (__i386) || defined (__amd64) || defined (__powerpc__)) && defined (__GNUC__)
# if defined (__clang__)
#  define HAVE_ATOMIC 1
# endif
# if defined (__GLIBC__) && defined (__GLIBC_PREREQ)
#  if (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC__ >= 1)) && __GLIBC_PREREQ (2, 6)
#   define HAVE_ATOMIC 1
#  endif
# endif
#endif

#if HAVE_ATOMIC || defined (__TINYC__)
# include <stdatomic.h>

# define atomic_exchange_acquire(object, value) \
  atomic_exchange_explicit ((object), (value), memory_order_acquire)

# define atomic_load_relaxed(object) \
  atomic_load_explicit ((object), memory_order_relaxed)

# define atomic_store_release(object, value) \
  atomic_store_explicit ((object), (value), memory_order_release)

//# define atomic _Atomic
#elif defined (_WIN32)
# define atomic_init(object, value)             \
  do                                            \
    {                                           \
      *(object) = (value);                      \
    }                                           \
  while (0)

#define atomic_store(object, value) \
  InterlockedExchange (object, value)

#define atomic_load(object) \
  InterlockedOr (object, 0)

# define atomic_increment(object) \
  InterlockedIncrement (object)

# define atomic_decrement(object) \
  InterlockedDecrement (object)

# define atomic_exchange(object, value) \
  InterlockedExchange ((object), (value))

# define atomic_compare_exchange(object, value, desired) \
  InterlockedCompareExchange ((object), (value), (desired))

# define atomic_compare_exchange_weak(object, value, desired) \
  atomic_compare_exchange (object, value, desired)

# define atomic_exchange_acquire(object, value) \
  atomic_exchange ((object), (value))

# define atomic_load_relaxed(object) \
  atomic_load (object)

# define atomic_store_release(object, value) \
  atomic_store ((object), (value))

typedef LONG atomic_size_t;
typedef LONG atomic_bool;
#else
# error "Unable to determine atomic operations for your platform"
#endif

/* Does the __typeof__ keyword work?  This could be done by
   'configure', but for now it's easier to do it by hand.  */
#if __GNUC__ >= 2 \
 || (__clang_major__ >= 4) \
 || (__IBMC__ >= 1210 && defined (__IBM__TYPEOF__)) \
 || (__SUNPRO_C >= 0x5110 && !__STDC__) \
 || defined (__TINYC__)
# define HAVE___TYPEOF__ 1
#else
# define HAVE___TYPEOF__ 0
#endif

#include <stdint.h>

/* To check alignment gcc has an appropriate operator.
   Other compilers do not.  */
#if __GNUC__ >= 2 && !defined (__PCC__)
# define pointer_aligned(pointer, type) \
  ((uintptr_t) (pointer) % __alignof__ (type) == 0)
#else
# define pointer_aligned(pointer, type) \
  ((uintptr_t) (pointer) % sizeof (type) == 0)
#endif

/* Used for suppressing warning when we know what we are doing.  */
#define pointer_unqualify(pointer) ((void *) (uintptr_t) (pointer))

#define BARF_IF_FAIL(e) (sizeof (char [1 - 2 * (!(e) ? 1 : 0)]) - 1)

#if (__GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ > 1) \
 || defined(__clang__) || defined(__PCC__) || defined(__TINYC__)) \
 && HAVE___TYPEOF__
# define countof(array) \
  (sizeof (array) / sizeof ((array)[0]) \
   + BARF_IF_FAIL (!__builtin_types_compatible_p (__typeof__ (array), \
                                                  __typeof__ (&array[0]))))

/* Are two types/variables the same type (ignoring qualifiers)? */
# define __same_type(a, b) __builtin_types_compatible_p (typeof (a), typeof (b))

/**
 * container_of - cast a member of a structure out to the containing structure
 * @ptr:	the pointer to the member.
 * @type:	the type of the container struct this is embedded in.
 * @member:	the name of the member within the struct.
 *
 * WARNING: any const qualifier of @ptr is lost.
 */
# define container_of(ptr, type, member)                                \
  __extension__                                                         \
  ({                                                                    \
    void *__member_pointer = (void *) (ptr);                            \
    static_assert (__same_type (*(ptr), ((type *) 0)->member)           \
                || __same_type (*(ptr), void),                          \
		   "pointer type mismatch in container_of()");          \
    ((type *) ((char *) __member_pointer - offsetof (type, member)));	\
  })
#else
# define countof(array) (sizeof (array) / sizeof ((array)[0]))
# define container_of(ptr, type, member) \
  ((type *) ((char *) (ptr) - offsetof (type, member)))
#endif

/*
 * Optional: only supported since gcc >= 14
 * Optional: only supported since clang >= 17
 *
 *   gcc: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=108896
 * clang: https://reviews.llvm.org/D148381
 */
#if (defined (__GNUC__) && __GNUC__ >= 14) || (defined (__clang__) && __clang_major__ >= 17)
# define counted_by(member) __attribute__ ((__element_count__ (#member)))
#else
# define counted_by(member)
#endif

#define STRLITEQ(s1, s2) (strncmp (s1, s2, countof (s2)) == 0)

/* __builtin_expect(CONDITION, EXPECTED_VALUE) evaluates to CONDITION,
   but notifies the compiler that the most likely value of CONDITION
   is EXPECTED_VALUE.  */
#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 96)
# define LIKELY(condition)   __builtin_expect (!!(condition), true)
# define UNLIKELY(condition) __builtin_expect (!!(condition), false)
#else
# define LIKELY(condition)   (!!(condition))
# define UNLIKELY(condition) (!!(condition))
#endif

//TODO
#ifdef _WIN32
# undef PURE
# undef CONST
#endif

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 95)
# define CONST __attribute__ ((__const__))
#else
# define CONST
#endif

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 96)
# define PURE __attribute__ ((__pure__))
#else
# define PURE
#endif

#if __GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ >= 3) || defined (__clang__)
# define NONNULL(...) __attribute__ ((__nonnull__ (__VA_ARGS__)))
#else
# define NONNULL(...)
#endif

/* Warn about unused results of certain
   function calls which can lead to problems.  */
#if __GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ >= 4)
# define NODISCARD __attribute__ ((__warn_unused_result__))
#else
# define NODISCARD
#endif

#if __GNUC__ >= 7
# define FALLTHROUGH __attribute__ ((__fallthrough__))
#else
# define FALLTHROUGH do { } while (0)
#endif

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)
# define UNUSED __attribute__ ((__unused__))
#else
# define UNUSED
#endif

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 5)
# define FORMAT(type, p1, p2) __attribute__ ((__format__ (type, p1, p2)))
#else
# define FORMAT(type, p1, p2)
#endif

#if (defined (_MSC_VER) && _MSC_VER >= 1200) || defined (__CYGWIN__)
# define NORETURN __declspec (noreturn)
#elif (__GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 8)) || defined (__clang__)
# define NORETURN __attribute__ ((__noreturn__))
#else
# define NORETURN _Noreturn
#endif

/* Designates a 1-based positional argument ref-index of pointer type
   that can be used to access size-index elements of the pointed-to
   array according to access mode, or at least one element when
   size-index is not provided:
     access (access-mode, <ref-index> [, <size-index>])  */
#if __GNUC__ >= 10
# define ACCESS_read_only(...) \
  __attribute__ ((__access__ (__read_only__, __VA_ARGS__)))
# define ACCESS_write_only(...) \
  __attribute__ ((__access__ (__write_only__, __VA_ARGS__)))
# define ACCESS_read_write(...) \
  __attribute__ ((__access__ (__read_write__, __VA_ARGS__)))
#else
# define ACCESS_read_only(...)
# define ACCESS_write_only(...)
# define ACCESS_read_write(...)
#endif

#if __GNUC__ >= 11
# define ACCESS_none(...) __attribute__ ((__access__ (__none__, __VA_ARGS__)))
#else
# define ACCESS_none(...)
#endif

#define ACCESS(mode, ...) ACCESS_##mode (__VA_ARGS__)

#if (__GNUC__ >= 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 9)) \
  && !defined(__PCC__)
# define RETURNS_NONNULL __attribute__ ((__returns_nonnull__))
#else
# define RETURNS_NONNULL
#endif

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)
# define PACKED __attribute__ ((__packed__))
#else
# define PACKED
#endif

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 96)
# define MALLOC __attribute__ ((__malloc__))
#else
# define MALLOC
#endif

#if __GNUC__ >= 11
/* Designates dealloc as a function to call to deallocate objects
   allocated by the declared function.  */
# define DEALLOC(dealloc, argno) __attribute__ ((__malloc__ (dealloc, argno)))
#else
# define DEALLOC(dealloc, argno)
#endif

#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 3)
# define ALLOC_SIZE(...) __attribute__ ((__alloc_size__ (__VA_ARGS__)))
#else
# define ALLOC_SIZE(...)
#endif

#if __GNUC__ > 3 || (__GNUC__ == 3 && __GNUC_MINOR__ >= 1)
# define ALWAYS_INLINE __attribute__ ((__always_inline__))
#else
# define ALWAYS_INLINE
#endif

/* Warningless cast from const pointer to usual pointer.  */
static inline ALWAYS_INLINE void *
worst_cast (const void *p)
{
  union
  {
    const void *p;
    void *q;
  } u = { .p = p };

  return u.q;
}

#define pragma(x) _Pragma (#x)
#define TODO(x) pragma (message ("TODO: " #x))

/* Support for flexible arrays.
   Usage:

   struct foo
   {
     size_t size;
     char bar[__flexible_array__];
   };

   stuct foo *
   baz (size_t size)
   {
     return malloc (sizeof (struct foo) - __flexible_array_extra_size__ + size);
   } */
#if defined (__STDC_VERSION__) && __STDC_VERSION__ > 199901L && !defined (__HP_cc)
# define __flexible_array__
# define __flexible_array_extra_size__ 0
#elif __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 96) || defined (__clang__)
/* GCC 2.97 and clang support C99 flexible array members as an extension,
   even when in C89 mode or compiling C++ (any version).  */
# define __flexible_array__
# define __flexible_array_extra_size__ 0
#elif defined (__GNUC__)
/* Pre-2.97 GCC did not support C99 flexible arrays but did have
   an equivalent extension with slightly different notation.  */
# define __flexible_array__ 0
# define __flexible_array_extra_size__ 1
#else
/* Some other non-C99 compiler.  Approximate with [1].  */
# define __flexible_array__ 1
# define __flexible_array_extra_size__ 1
#endif

/* How Build quotes filenames, to minimize use of outer quotes,
   but also provide better support for copy and paste when used.  */
#include "libquote/quote.h"
#include "libquote/quotearg.h"

/* Use these to shell quote only when necessary,
   when the quoted item is already delimited with colons.  */
#define quotef(qs, arg) \
  quotearg_n_style_colon (qs, 0, quoting_style_shell_escape, arg)
#define quotef_n(qs, n, arg) \
  quotearg_n_style_colon (qs, n, quoting_style_shell_escape, arg)

/* Use these when there are spaces around the file name,
   in the error message.  */
#define quoteaf(qs, arg) \
  quotearg_style (qs, quoting_style_shell_escape_always, arg)
#define quoteaf_n(qs, n, arg) \
  quotearg_n_style (qs, n, quoting_style_shell_escape_always, arg)

#if ENABLE_NLS
/* Get declarations of GNU message catalog functions.  */
# include <libintl.h>
#else
/* Disabled NLS.
   The casts to 'const char *' serve the purpose of producing warnings
   for invalid uses of the value returned from these functions.  */
# define gettext(msgid) ((const char *) (msgid))
# define ngettext(msgid1, msgid2, n) \
  ((n) == 1 ? (const char *) (msgid1) : (const char *) (msgid2))
# define textdomain(domainname) \
  ((const char *) (domainname))
# define bindtextdomain(domainname, dirname) \
  ((void) (domainname), (const char *) (dirname))
# ifndef PACKAGE
#  define PACKAGE   ""
# endif
# ifndef LOCALEDIR
#  define LOCALEDIR ""
# endif
#endif

/* A pseudo function call that serves as a marker for the automated
   extraction of messages, but does not call gettext().  The run-time
   translation is done at a different place in the code.
   The argument, MSGID, should be a literal string.  Concatenated strings
   and other string expressions will not work.
   The macro's expansion is not parenthesized, so that it is suitable as
   initializer for static 'char[]' or 'const char[]' variables.  */
#define gettext_noop(msgid) msgid

#define  _(msgid)             gettext (msgid)
#define N_(msgid)             gettext_noop (msgid)
#define S_(msgid1, msgid2, n) ngettext (msgid1, msgid2, n)
