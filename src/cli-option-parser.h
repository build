
#pragma once

#include "diagnostic.h"
#include "system.h"

/* Constants defined by this include file.  */
enum argument
{
  no_argument = 0,
  required_argument = 1,
  optional_argument = 2
};

/* Types defined by this include file.  */

/* `struct cli_option' is the type of long option.  */
struct cli_option
{
  /* The name of the long option.  */
  const char *name;
  /* One of the above constants.  */
  enum argument has_argument;
  /* Determines if `cli_option_parse()' returns a value
     for a long option; if it is non-NULL,
     0 is returned as a function value and the value
     of `value' is stored in the area pointed to by `flag'.
     Otherwise, `value' is returned.  */
  int *flag;
  /* Determines the value to return if `flag' is NULL.  */
  int value;
};

struct cli_option_parser_state
{
  char *argument;
  int index;
  int where;
  char option; //TODO rename mb?
  bool error;
};

enum
  {
    CLI_OPTION_PARSE_NONE = -1,
    CLI_OPTION_PARSE_ERROR = -2,
  };

/* Function prototypes.  */
extern void cli_option_parser_state_initialize (struct cli_option_parser_state *state)
  NONNULL (1) ACCESS (write_only, 1);
extern int cli_option_parse (struct cli_option_parser_state *state,
                             int argc, char **argv,
                             const char *shortopts,
                             int optcount,
                             struct cli_option *options,
                             int *longind)
  NONNULL (1, 3, 4, 6);
