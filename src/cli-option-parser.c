/****************************************************************************

getopt.c - Read command line options

AUTHOR: Gregory Pietsch
CREATED Fri Jan 10 21:13:05 1997

DESCRIPTION:

The getopt() function parses the command line arguments.  Its arguments argc
and argv are the argument count and array as passed to the main() function
on program invocation.  The argument optstring is a list of available option
characters.  If such a character is followed by a colon (`:'), the option
takes an argument, which is placed in optarg.  If such a character is
followed by two colons, the option takes an optional argument, which is
placed in optarg.  If the option does not take an argument, optarg is NULL.

The external variable optind is the index of the next array element of argv
to be processed; it communicates from one call to the next which element to
process.

The getopt_long() function works like getopt() except that it also accepts
long options started by two dashes `--'.  If these take values, it is either
in the form

--arg=value

 or

--arg value

It takes the additional arguments longopts which is a pointer to the first
element of an array of type GETOPT_LONG_OPTION_T.  The last element of the
array has to be filled with NULL for the name field.

The longind pointer points to the index of the current long option relative
to longopts if it is non-NULL.

The getopt() function returns the option character if the option was found
successfully, `:' if there was a missing parameter for one of the options,
`?' for an unknown option character, and EOF for the end of the option list.

The getopt_long() function's return value is described in the header file.

The function getopt_long_only() is identical to getopt_long(), except that a
plus sign `+' can introduce long options as well as `--'.

The following describes how to deal with options that follow non-option
argv-elements.

If the caller did not specify anything, the default is REQUIRE_ORDER if the
environment variable POSIXLY_CORRECT is defined, PERMUTE otherwise.

REQUIRE_ORDER means don't recognize them as options; stop option processing
when the first non-option is seen.  This is what Unix does.  This mode of
operation is selected by either setting the environment variable
POSIXLY_CORRECT, or using `+' as the first character of the optstring
parameter.

PERMUTE is the default.  We permute the contents of ARGV as we scan, so that
eventually all the non-options are at the end.  This allows options to be
given in any order, even with programs that were not written to expect this.

RETURN_IN_ORDER is an option available to programs that were written to
expect options and other argv-elements in any order and that care about the
ordering of the two.  We describe each non-option argv-element as if it were
the argument of an option with character code 1.  Using `-' as the first
character of the optstring parameter selects this mode of operation.

The special argument `--' forces an end of option-scanning regardless of the
value of ordering.  In the case of RETURN_IN_ORDER, only `--' can cause
getopt() and friends to return EOF with optind != argc.

COPYRIGHT NOTICE AND DISCLAIMER:

Copyright (C) 1997 Gregory Pietsch

This file and the accompanying getopt.h header file are hereby placed in the
public domain without restrictions.  Just give the author credit, don't
claim you wrote it or prevent anyone else from using it.

Gregory Pietsch's current e-mail address:
gpietsch@comcast.net
****************************************************************************/

/* Include files.  */
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cli-option-parser.h"

/* Types.  */
enum ordering
{
  PERMUTE,
  RETURN_IN_ORDER,
  REQUIRE_ORDER
};

/* Functions.  */

/* Initialize CLI option parser state by default values.  */
void
cli_option_parser_state_initialize (struct cli_option_parser_state *state)
{
  state->argument = NULL;
  state->index = 0;
  state->where = 0;
  state->error = true;
  state->option = '?';
}

/* Reverses num elements starting at argv.  */
static void
reverse_argv_elements (char **argv, int num)
{
  int i;
  char *tmp;

  for (i = 0; i < (num >> 1); i++)
    {
      tmp = argv[i];
      argv[i] = argv[num - i - 1];
      argv[num - i - 1] = tmp;
    }
}

/* Swap two blocks of argv-elements given their lengths.  */
static void
permute (char **argv, int len1, int len2)
{
  reverse_argv_elements (argv, len1);
  reverse_argv_elements (argv, len1 + len2);
  reverse_argv_elements (argv, len2);
}

/* Is this argv-element an option or the end of the option list? */
static int
is_option (char *argv_element)
{
  return ((argv_element == NULL) || (argv_element[0] == '-'));
}

int
cli_option_parse (struct cli_option_parser_state *state,
                  int argc, char **argv,
                  const char *shortopts,
                  int optcount,
                  struct cli_option *longopts,
                  int *longind)
{
  enum ordering ordering = PERMUTE;
  int permute_from = 0;
  int num_nonopts = 0;
  int match_chars = 0;
  char *possible_arg = NULL;
  int longopt_match = -1;
  int has_argument = -1;
  char *cp = NULL;
  int arg_next = 0;

  if (state->index >= argc || argv[state->index] == NULL)
    return EOF;

  if (strcmp (argv[state->index], "--") == 0)
    {
      state->index++;
      return EOF;
    }

  /* If this is our first time through.  */
  if (state->index == 0)
    state->index = state->where = 1;

  /* Define ordering.  */
  switch (*shortopts)
    {
    case '-':
      ordering = RETURN_IN_ORDER;
      shortopts++;
      break;
    case '+':
      ordering = REQUIRE_ORDER;
      shortopts++;
      break;
    default:
      ordering = (getenv ("POSIXLY_CORRECT") != NULL) ? REQUIRE_ORDER : PERMUTE;
      break;
    }

  /* Based on ordering, find our next option, if we're at the beginning of one.  */
  if (state->where == 1)
    {
      switch (ordering)
        {
        case PERMUTE:
          permute_from = state->index;
          num_nonopts = 0;

          while (!is_option (argv[state->index]))
            {
              state->index++;
              num_nonopts++;
            }

          if (argv[state->index] == NULL)
            {
              /* No more options.  */
              state->index = permute_from;
              return EOF;
            }
          else if (strcmp (argv[state->index], "--") == 0)
            {
              /* No more options, but have to get `--' out of the way.  */
              permute (argv + permute_from, num_nonopts, 1);
              state->index = permute_from + 1;
              return EOF;
            }

          break;
        case RETURN_IN_ORDER:
          if (!is_option (argv[state->index]))
            {
              state->argument = argv[state->index++];
              state->option = 1;
              return state->option;
            }

          break;
        case REQUIRE_ORDER:
          if (!is_option (argv[state->index]))
            return EOF;

          break;
        }
    }

  /* We have got an option, so parse it.  */

  /* First, is it a long option? */
  if (memcmp (argv[state->index], "--", 2) == 0 && state->where == 1)
    {
      /* Handle long options.  */
      state->where = 2;
      possible_arg = strchr (argv[state->index] + state->where, '=');

      if (possible_arg == NULL)
        {
          /* No =, so next argv might be argument.  */
          size_t len = strlen (argv[state->index]);

          if (len > INT_MAX)
            {
              /* We have option which name is too long.  */
              if (state->error)
                error (_("option `%s' is too long"), argv[state->index]);

              return (state->option = '?');
            }

          match_chars = (int) len;
          possible_arg = argv[state->index] + match_chars;
          match_chars -= state->where;
        }
      else
        match_chars = (int) (possible_arg - argv[state->index]) - state->where;

      for (int optindex = 0; optindex < optcount; optindex++)
        {
          if (memcmp (argv[state->index] + state->where,
                      longopts[optindex].name,
                      (size_t) match_chars) == 0)
            {
              size_t len = strlen (longopts[optindex].name);

              if (len > INT_MAX)
                {
                  /* We have option which name is too long.  */
                  if (state->error)
                    error (_("option `%s' is too long"), longopts[optindex].name);

                  return (state->option = '?');
                }

              /* Do we have an exact match? */
              if (match_chars == (int) len)
                {
                  longopt_match = optindex;
                  break;
                }
              /* Do any characters match? */
              else
                {
                  if (longopt_match < 0)
                    longopt_match = optindex;
                  else
                    {
                      /* We have ambiguous options.  */
                      if (state->error)
                        error (_("option `%s' is ambiguous (could be `--%s' or `--%s')"),
                               argv[state->index],
                               longopts[longopt_match].name,
                               longopts[optindex].name);

                      return (state->option = '?');
                    }
                }
            }
        }

      if (longopt_match >= 0)
        has_argument = longopts[longopt_match].has_argument;
    }

  /* If we did not find a long option, is it a short option? */
  if (longopt_match < 0 && shortopts != NULL)
    {
      cp = strchr (shortopts, argv[state->index][state->where]);

      if (cp == NULL)
        {
          /* Could not find option in shortopts.  */
          if (state->error)
            error (_("invalid option -- `-%c'"),
                   argv[state->index][state->where]);

          state->where++;

          if (argv[state->index][state->where] == '\0')
            {
              state->index++;
              state->where = 1;
            }

          return (state->option = '?');
        }

      has_argument = ((cp[1] == ':') ? ((cp[2] == ':') ? optional_argument : required_argument) : no_argument);
      possible_arg = argv[state->index] + state->where + 1;
      state->option = *cp;
    }

  /* Get argument and reset state->where.  */
  arg_next = 0;

  switch (has_argument)
    {
    case optional_argument:
      if (*possible_arg == '=')
        possible_arg++;

      if (*possible_arg != '\0')
        {
          state->argument = possible_arg;
          state->where = 1;
        }
      else
        state->argument = NULL;

      break;
    case required_argument:
      if (*possible_arg == '=')
        possible_arg++;

      if (*possible_arg != '\0')
        {
          state->argument = possible_arg;
          state->where = 1;
        }
      else if (state->index + 1 >= argc)
        {
          if (state->error)
            {
              if (longopt_match >= 0)
                error (_("argument required for option `--%s'"), longopts[longopt_match].name);
              else
                error (_("argument required for option `-%c'"), *cp);
            }

          state->index++;
          return (state->option = ':');
        }
      else
        {
          state->argument = argv[state->index + 1];
          arg_next = 1;
          state->where = 1;
        }

      break;
    case no_argument:
      if (longopt_match < 0)
        {
          state->where++;

          if (argv[state->index][state->where] == '\0')
            state->where = 1;
        }
      else
        state->where = 1;

      state->argument = NULL;
      break;
    }

  /* Do we have to permute or otherwise modify state->index? */
  if (ordering == PERMUTE && state->where == 1 && num_nonopts != 0)
    {
      permute (argv + permute_from, num_nonopts, 1 + arg_next);
      state->index = permute_from + 1 + arg_next;
    }
  else if (state->where == 1)
    state->index = state->index + 1 + arg_next;

  /* Finally return.  */
  if (longopt_match >= 0)
    {
      if (longind != NULL)
        *longind = longopt_match;

      if (longopts[longopt_match].flag != NULL)
        {
          *(longopts[longopt_match].flag) = longopts[longopt_match].value;
          return 0;
        }
      else
        return longopts[longopt_match].value;
    }
  else
    return state->option;
}
