
#pragma once

#include <stdio.h>

#include "system.h"
#include "uniqstr.h"

struct yyltype
{
  uniqstr file;
  file_offset_t newline_offset;
  file_offset_t next_newline_offset;
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};

/* An opaque pointer. */
#ifndef YY_TYPEDEF_YY_SCANNER_T
# define YY_TYPEDEF_YY_SCANNER_T
typedef struct yyscanner *yyscan_t;
#endif

extern void yylocation_initialize (struct yyltype *yylocation, uniqstr file);
extern void yylocation_print (FILE *out, const struct yyltype *yylocation);
extern bool yylocation_compute (struct yyltype *yylocation, const char *text, int leng);
