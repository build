/* parse.y -- the build parser routines.

   Copyright (C) 2022 Sergey Sushilin <sergeysushilin@protonmail.com>

   This file is part of Build.

   Build is free software: you can redistribute it and/or
   modify it under the terms of either the GNU General Public License
   as published by the Free Software Foundation;
   either version 2 of the License, or version 3 of thee License,
   or both in parallel, as here.

   Build is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License
   version 2 and 3 along with this program.
   If not, see http://www.gnu.org/licenses/.  */

%{
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
%}

%header "src/parse.h"
%output "src/parse.c"
%param {yyscan_t yyscanner}
%parse-param {struct parse_context *context}
%define parse.lac     full
%define api.pure      full
%define api.push-pull pull

/* Include the header in the implementation rather than duplicating it.  */
%define api.header.include {"parse.h"}

/* Customized syntax error messages (see yyreport_syntax_error).  */
%define parse.error custom

/* To avoid name clashes (e.g., with C's EOF) prefix token definitions
   with 'YYTOKEN_' (e.g., YYTOKEN_EOF).  */
%define api.token.prefix {YYTOKEN_}

/* Same for symbols.  */
%define api.symbol.prefix {YYSYMBOL_}

%define api.value.type {YYSTYPE}
%language "C"
%locations

%code requires
{
#include "build.h"
#include "uniqstr.h"
#include "yy.h"

  /* Left-value type.  */
#if defined (YYSTYPE) || defined (YYSTYPE_IS_DECLARED)
# error Unexpected that YYSTYPE is already declared
#endif

  typedef union
  {
    uniqstr string;
    struct variable_value variable_value;
    struct apply_rule apply_rule;

    struct
    {
      uniqstr directory;
      uniqstr description;
    } options;
    VECTOR_ENTRY (uniqstr_array) strings;
    VECTOR_ENTRY (apply_rule_array) apply_rules;
  } YYSTYPE;

#define YYSTYPE_IS_DECLARED 1
#define YYSTYPE_IS_TRIVIAL  1

#if defined (YYLTYPE) || defined (YYLTYPE_IS_DECLARED)
# error Unexpected that YYLTYPE is already declared
#endif

#define YYLTYPE struct yyltype

#define YYLTYPE_IS_DECLARED 1
#define YYLTYPE_IS_TRIVIAL  0
}

%code provides
{
  /* YYINITDEPTH -- initial size of the parser's stacks.  */
#define YYINITDEPTH 256

  /* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
     if the built-in stack extension method is used).

     Do not make this value too large; the results are undefined if
     YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
     evaluated with infinite-precision integer arithmetic.  */
#define YYMAXDEPTH 8192

  typedef struct
  {
    struct
    {
      char *base;
      size_t size;
      size_t length;
    } buffer;
    YYLTYPE *yylocation;
    struct quoting_slots *qs;
    const struct hash_table *variables;
  } YYETYPE;

#define YY_EXTRA_TYPE YYETYPE *

#if !defined (FLEX_SCANNER) && 0
  extern NODISCARD int yylex_init (yyscan_t *yyscanner) NONNULL (1);
  extern int yylex_destroy (yyscan_t yyscanner) NONNULL (1);
  extern void yyset_extra (YY_EXTRA_TYPE user_defined, yyscan_t yyscanner) NONNULL (1, 2);
  extern NODISCARD YY_EXTRA_TYPE yyget_extra (yyscan_t yyscanner) RETURNS_NONNULL;
  extern NODISCARD FILE *yyget_in (yyscan_t yyscanner) NONNULL (1) RETURNS_NONNULL;
  extern void yyset_in (FILE *fp, yyscan_t yyscanner) NONNULL (1, 2);
  extern void yyset_out (FILE *fp, yyscan_t yyscanner) NONNULL (1, 2);
#endif
}

%code
{
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "build.h"
#include "diagnostic.h"
#include "libquote/quote.h"
#include "system.h"
#include "utils.h"
#include "scan.h"

  /*
#define ARRAY_DUPLICATE(a0, a1)                                         \
  do                                                                    \
    {                                                                   \
      (a0).count = (a1).count;                                          \
      (a0).array = xmallocarray ((a0).count, sizeof (*(a0).array));     \
      memcpy ((a0).array, (a1).array,                                   \
              (a0).count * sizeof (*(a0).array));                       \
    }                                                                   \
  while (0)

#define ARRAY_INITIALIZE_EMPTY(a0)   \
  do                                 \
    {                                \
      (a0).count = 0;                \
      (a0).array = NULL;             \
    }                                \
  while (0)

#define ARRAY_INITIALIZE(a0, a1)                   \
  do                                               \
    {                                              \
      (a0).count = 0;                              \
      (a0).array = xmalloc (sizeof (*(a0).array)); \
      (a0).array[(a0).count++] = (a1);             \
    }                                              \
  while (0)

#define ARRAY_EXPAND(a0, a1)                                            \
  do                                                                    \
    {                                                                   \
      (a0).array = xreallocarray ((a0).array, ((a0).count + 1),         \
                                  sizeof (*(a0).array));                \
      (a0).array[(a0).count++] = (a1);                                  \
    }                                                                   \
  while (0)

#define ARRAY_CATENATE(a0, a1)                                         \
  do                                                                   \
    {                                                                  \
      (a0).array = xreallocarray ((a0).array, (a0).count + (a1).count, \
                                  sizeof (*(a0).array));               \
      memcpy ((a0).array + (a0).count, (a1).array,                     \
              (a1).count * sizeof (*(a0).array));                      \
      (a0).count += (a1).count;                                        \
    }                                                                  \
  while (0)
  */

#define VARIABLE_FIND(variable, name, name_loc)                         \
  do                                                                    \
    {                                                                   \
      variable = HashFindUniqstr (&context->variables, name);           \
                                                                        \
      if (UNLIKELY (variable == NULL))                                  \
        {                                                               \
          error_at (&name_loc,                                          \
                    _("variable is not declared: %s"),                  \
                    name);                                              \
          YYABORT;                                                      \
        }                                                               \
    }                                                                   \
  while (0)

#define VARIABLE_INSERT(variable)                                        \
  do                                                                     \
    {                                                                    \
      HashInsertUniqstr (&context->variables, variable.name, &variable); \
    }                                                                    \
  while (0)

#define VARIABLE_REMOVE(variable)                                       \
  do                                                                    \
    {                                                                   \
      if (LIKELY (variable.name != NULL))                               \
        {                                                               \
          HashRemoveUniqstr (&context->variables, variable.name);       \
                                                                        \
          /* Do not 'free (variable.value.array);'                      \
             since this array used by target_create().  */              \
                                                                        \
          memset (&variable, '\0', sizeof (variable));                  \
        }                                                               \
    }                                                                   \
  while (0)

#define VARIABLE_TEMPORARY_DEFINE_STRING(variable, str, val)    \
  do                                                            \
    {                                                           \
      variable.value.type = VARIABLE_STRING;                    \
      variable.name = uniqstr_new_from_literal_string (str);    \
      variable.value.string = val;                              \
      VARIABLE_INSERT (variable);                               \
    }                                                           \
  while (0)

#define VARIABLE_TEMPORARY_DEFINE_ARRAY(variable, string, val)  \
  do                                                            \
    {                                                           \
      variable.value.type = VARIABLE_STRING_ARRAY;              \
      variable.name = uniqstr_new_from_literal_string (string); \
      variable.value.array = val;                               \
      VARIABLE_INSERT (variable);                               \
    }                                                           \
  while (0)

  static inline NODISCARD uniqstr NONNULL (1, 2)
  replace_percent (uniqstr string, uniqstr percent)
  {
    uniqchr *buffer = uniqstr_prepare (uniqstr_len (percent) + uniqstr_len (string) - 1);
    uniqstr start = string;
    uniqstr end = string + uniqstr_len (string);
    uniqchr *p = memchr (string, '%', uniqstr_len (string));
    size_t len = 0;

    if (p == NULL)
      {
        error (_("expected percent sign in string: %s"),
               string);
        return NULL;
      }

    if (p != start)
      {
        memcpy (buffer, start, (size_t) (p - start));
        len += (size_t) (p - start);
      }

    memcpy (buffer + len, percent, uniqstr_len (percent));
    len += uniqstr_len (percent);

    if (p + 1 != end)
      {
        if (memchr (p + 1, '%', (size_t) (end - p - 1)) != NULL)
          {
            error (_("too many percent signs in string: %s"),
                   string);
            return NULL;
          }

        memcpy (buffer + len, p + 1, (size_t) (end - (p + 1)));
        len += (size_t) (end - (p + 1));
      }

    buffer[len] = '\0';
    return uniqstr_finish_preparation (buffer);
  }

  struct pattern
  {
    uniqstr start;
    uniqstr finish;
  };

  static NODISCARD struct pattern NONNULL (1)
  pattern_from_string (uniqstr string)
  {
    const uniqchr *const percent = uniqstr_chr (string, '%');
    const uniqchr *const start = string;
    const uniqchr *const finish = string + (percent - string + 1);
    const size_t start_len = (size_t) (percent - string);
    const size_t finish_len = (size_t) (uniqstr_end (string) - percent - 1);
    struct pattern pattern =
      {
        .start = uniqstr_new_from_buffer (start, start_len),
        .finish = uniqstr_new_from_buffer (finish, finish_len),
      };

    return pattern;
  }

  static NODISCARD bool NONNULL (1, 2, 3)
  pattern_match (const struct pattern *pattern, uniqstr name, uniqstr *percent)
  {
    uniqstr start = pattern->start;
    uniqstr finish = pattern->finish;
    const size_t start_len = uniqstr_len (start);
    const size_t finish_len = uniqstr_len (finish);
    const size_t name_len = uniqstr_len (name);

    if (name_len < start_len + finish_len + 1)
      return false;

    if (start_len != 0 && memcmp (name, start, start_len) != 0)
      return false;

    if (finish_len != 0 && memcmp (uniqstr_end (name) - finish_len, finish, finish_len) != 0)
      return false;

    *percent = uniqstr_new_from_buffer (name + start_len, name_len - (start_len + finish_len));
    return true;
  }

  static void NONNULL (1, 2, 3, 4)
  yyerror (const YYLTYPE *yylocation,
           UNUSED yyscan_t yyscanner,
           UNUSED struct parse_context *parse_context,
           const char *s)
  {
    yyerror_base (yylocation, s);
  }
}

%token
  END 0 _("end of input")
  <string>
    STRING  _("string")
    NAME    _("name")
    VALUE   _("value")
    COMMAND _("command")
  <void>
    ':'
    '{'
    '}'
    '['
    ']'
    '('
    ')'
    '@'
    '~'
    ARROW       "->"
    CATENATE    "+="
    DESCRIPTION _("'description' keyword")
    LET         _("'let' keyword")
    RULE        _("'rule' keyword")
    TARGET      _("'target' keyword")
    FILE        _("'file' keyword")
    DEFAULT     _("'default' keyword")
    DIRECTORY   _("'directory' keyword")
    MAP         _("'map' keyword")
    FILTER      _("'filter' keyword")

%type
  <string>
    string name description directory target_name out
    command depfile map_in map_out
  <strings>
    _strings strings strings_duplicate strings_or_nothing dependencies files in
    _commands commands map_strings filter_strings file_target_name
  <apply_rule>
    apply_rule
  <apply_rules>
    apply_rules apply_rules_or_nothing
  <options>
    options
  <variable_value>
    string_or_strings value

%start program

%initial-action
{
  @$ = *yyget_extra (yyscanner)->yylocation;
}

%%

program:        %empty
        |       program default_target
        |       program rule
        |       program target
        |       program file_target
        |       program variable
        |       program map_rule
        |       program error { YYABORT; }
                ;

default_target: DEFAULT TARGET STRING
                  {
                    if (context->default_target != NULL)
                      {
                        error_at (&@STRING, _("default target name is already specified"));
                        YYABORT;
                      }

                    context->default_target = $STRING;
                  }
                ;

name:           NAME
                  {
                    $$ = $NAME;
                  }
                ;

string:         STRING
                  {
                    $$ = $STRING;
                  }
        |       value
                  {
                    if (UNLIKELY ($value.type != VARIABLE_STRING))
                      {
                        error_at (&@value, _("unexpected string array, expecting string"));
                        YYABORT;
                      }

                    $$ = $value.string;
                  }
                ;

strings_or_nothing:
                %empty
                  {
                    VECTOR_INITIALIZE_EMPTY ($$);
                  }
        |       strings
                  {
                    $$ = $strings;
                  }
                ;

string_or_strings:
                string
                  {
                    $$.type = VARIABLE_STRING;
                    $$.string = $string;
                  }
        |       '[' strings_or_nothing ']'
                  {
                    $$.type = VARIABLE_STRING_ARRAY;
                    $$.array = $strings_or_nothing;
                  }
                ;

variable:       LET name '=' string_or_strings
                  {
                    if (UNLIKELY (!variable_create (&context->variables, $name,
                                                    &$string_or_strings)))
                      YYABORT;
                  }
        |       name '=' string_or_strings
                  {
                    struct variable *variable;

                    VARIABLE_FIND (variable, $name, @name);

                    if (variable->value.type == VARIABLE_STRING_ARRAY)
                      VECTOR_FINALIZE (variable->value.array);

                    variable->value = $string_or_strings;
                  }
        |       name CATENATE string_or_strings
                  {
                    if (VECTOR_SIZE ($string_or_strings.array) != 0)
                      {
                        struct variable *variable;

                        VARIABLE_FIND (variable, $name, @name);
                        VECTOR_CATENATE (variable->value.array, $string_or_strings.array);

                        switch ($string_or_strings.type)
                          {
                        case VARIABLE_STRING_ARRAY:
                          VECTOR_FINALIZE ($string_or_strings.array);
                          break;
                        case VARIABLE_STRING:
                          break;
                        default:
                          ERROR_UNKNOWN_VALUE_TYPE ($string_or_strings);
                          }
                      }
                  }
                ;

command:        COMMAND
                  {
                    $$ = $COMMAND;
                  }
                ;

_commands:      command
                  {
                    VECTOR_INITIALIZE ($$);
                    VECTOR_PUSH ($$, $command);
                  }
        |       commands command
                  {
                    VECTOR_PUSH ($$, $command);
                  }
                ;

commands:       _commands
                  {
                    $$ = $_commands;
                    VECTOR_SHRINK ($$);
                  }
                ;

description:    DESCRIPTION ':' string
                  {
                    $$ = $string;
                  }
                ;

directory:      DIRECTORY ':' string
                  {
                    $$ = $string;
                  }
                ;

options:        %empty
                  {
                    $$.directory = NULL;
                    $$.description = NULL;
                  }
        |       directory
                  {
                    $$.description = NULL;
                    $$.directory = $directory;
                  }
        |       description
                  {
                    $$.description = $description;
                    $$.directory = NULL;
                  }
        |       directory description
                  {
                    $$.description = $description;
                    $$.directory = $directory;
                  }
        |       description directory
                  {
                    $$.description = $description;
                    $$.directory = $directory;
                  }
                ;

rule:           RULE name '{' options commands '}'
                  {
                    if (UNLIKELY (!rule_create (&context->rules,
                                                $name,
                                                $options.description,
                                                &$commands,
                                                &context->variables)))
                      YYABORT;
                  }
                ;

out:            %empty
                  {
                    $$ = NULL;
                  }
        |       ARROW string
                  {
                    $$ = $string;
                  }
                ;

in:             %empty
                  {
                    VECTOR_INITIALIZE_EMPTY ($$);
                  }
        |       strings
                  {
                    $$ = $strings;
                  }
                ;

apply_rule:     name in out
                  {
                    $$.name = $name;
                    $$.in = $in;
                    $$.out = $out;
                  }
                ;

value:          VALUE
                  {
                    struct variable *variable;

                    VARIABLE_FIND (variable, $VALUE, @VALUE);

                    switch (variable->value.type)
                      {
                      case VARIABLE_STRING:
                      case VARIABLE_STRING_ARRAY:
                        $$ = variable->value;
                        break;
                      default:
                        ERROR_UNKNOWN_VALUE_TYPE (variable->value);
                        YYABORT;
                      }
                  }
                ;

_strings:       STRING
                  {
                    VECTOR_INITIALIZE ($$);
                    VECTOR_PUSH ($$, $STRING);
                  }
        |       value
                  {
                    switch ($value.type)
                      {
                      case VARIABLE_STRING:
                        VECTOR_INITIALIZE ($$);
                        VECTOR_PUSH ($$, $value.string);
                        break;
                      case VARIABLE_STRING_ARRAY:
                        VECTOR_DUPLICATE ($$, $value.array);
                        break;
                      default:
                        ERROR_UNKNOWN_VALUE_TYPE ($value);
                        break;
                      }
                  }
        |       map_strings
                  {
                    if (VECTOR_SIZE ($map_strings) != 0)
                      $$ = $map_strings;
                  }
        |       filter_strings
                  {
                    if (VECTOR_SIZE ($filter_strings) != 0)
                      $$ = $filter_strings;
                  }
        |       strings STRING
                  {
                    VECTOR_PUSH ($$, $STRING);
                  }
        |       strings value
                  {
                    VECTOR_CATENATE ($$, $value.array);
                  }
        |       strings map_strings
                  {
                    if (VECTOR_SIZE ($map_strings) != 0)
                      VECTOR_CATENATE ($$, $map_strings);
                  }
        |       strings filter_strings
                  {
                    if (VECTOR_SIZE ($filter_strings) != 0)
                      VECTOR_CATENATE ($$, $filter_strings);
                  }
                ;

strings:        _strings
                  {
                    $$ = $_strings;
                    VECTOR_SHRINK ($$);
                  }
                ;

apply_rules:    apply_rule
                  {
                    VECTOR_INITIALIZE ($$);
                    VECTOR_PUSH ($$, $apply_rule);
                  }
        |       apply_rules apply_rule
                  {
                    VECTOR_PUSH ($$, $apply_rule);
                  }
                ;

apply_rules_or_nothing:
                %empty
                  {
                    VECTOR_INITIALIZE_EMPTY ($$);
                  }
        |       '{' apply_rules '}'
                  {
                    $$ = $apply_rules;
                  }
                ;

dependencies:   %empty
                  {
                    VECTOR_INITIALIZE_EMPTY ($$);
                    memset (&context->dependencies, '\0', sizeof (context->dependencies));
                  }
        |       ':' strings
                  {
                    VARIABLE_TEMPORARY_DEFINE_ARRAY (context->dependencies, "dependencies", $strings);
                    $$ = $strings;
                  }
                ;

files:          %empty
                  {
                    VECTOR_INITIALIZE_EMPTY ($$);
                    memset (&context->files, '\0', sizeof (context->files));
                  }
        |       '@' strings
                  {
                    VARIABLE_TEMPORARY_DEFINE_ARRAY (context->files, "files", $strings);
                    $$ = $strings;
                  }
                ;


depfile:        %empty
                  {
                    $$ = NULL;
                  }
/*        |       '~' string
                  {
                    HashInsert (&context->depfiles, $string, uniqstr_len ($string),
                                pointer_unqualify ($string));
                    $$ = $string;
                    }*/
                ;

target_name:    string
                  {
                    VARIABLE_TEMPORARY_DEFINE_STRING (context->target,
                                                      "target",
                                                      $string);
                    $$ = $string;
                  }
                ;

target:         TARGET target_name dependencies files depfile apply_rules_or_nothing
                  {
                    struct uniqstr_array outfiles;

                    VECTOR_INITIALIZE_EMPTY (outfiles);

                    VECTOR_SHRINK ($dependencies);
                    VECTOR_SHRINK ($files);
                    VECTOR_SHRINK ($apply_rules_or_nothing);

                    if (UNLIKELY (!target_create (NULL,
                                                  &context->targets,
                                                  $target_name,
                                                  target_skeleton_create (&context->skeletons,
                                                                          &$dependencies,
                                                                          &$files,
                                                                          &outfiles,
                                                                          &$apply_rules_or_nothing),
                                                  yyget_extra (yyscanner)->qs)))
                      YYABORT;

                    VARIABLE_REMOVE (context->target);
                    VARIABLE_REMOVE (context->files);
                    VARIABLE_REMOVE (context->dependencies);
                  }
                ;

file_target_name:
                string
                  {
                    VARIABLE_TEMPORARY_DEFINE_STRING (context->target, "target", $string);
                    VECTOR_INITIALIZE ($$);
                    VECTOR_PUSH ($$, $string);
                  }
        |       '[' strings ']'
                  {
                    /* Do not define target name
                       if many filenames specified.  */
                    $$ = $strings;
                    VARIABLE_TEMPORARY_DEFINE_ARRAY (context->target, "target", $$);
                  }
                ;

file_target:    FILE TARGET file_target_name dependencies files depfile apply_rules_or_nothing
                  {
                    struct target_skeleton *skeleton;
                    struct uniqstr_array outfiles;

                    outfiles = $file_target_name;

                    skeleton = target_skeleton_create (&context->skeletons,
                                                       &$dependencies,
                                                       &$files,
                                                       &outfiles,
                                                       &$apply_rules_or_nothing);

                    for (size_t i = 0; i < VECTOR_SIZE (outfiles); i++)
                      if (UNLIKELY (!target_create (NULL,
                                                    &context->targets,
                                                    VECTOR_AT (outfiles, i),
                                                    skeleton,
                                                    yyget_extra (yyscanner)->qs)))
                        YYABORT;

                    VARIABLE_REMOVE (context->target);
                    VARIABLE_REMOVE (context->files);
                    VARIABLE_REMOVE (context->dependencies);
                  }
                ;

map_in:         STRING
                  {
                    $$ = $STRING;
                  }
                ;

map_out:        STRING
                  {
                    $$ = $STRING;
                  }
                ;

map_rule:       MAP name strings '(' map_in ARROW map_out ')'
                  {
                    struct pattern pattern = pattern_from_string ($map_in);

                    for (size_t i = 0; i < VECTOR_SIZE ($strings); i++)
                      {
                        uniqstr percent;

                        if (pattern_match (&pattern, VECTOR_AT ($strings, i), &percent))
                          {
                            struct uniqstr_array empty;
                            struct uniqstr_array in;
                            struct uniqstr_array outfiles;
                            struct apply_rule rule;
                            struct apply_rule_array apply_rules;
                            struct target_skeleton *skeleton;
                            uniqstr out;
                            uniqstr infile;

                            out = replace_percent ($map_out, percent);

                            if (out == NULL)
                              YYABORT;

                            infile = replace_percent ($map_in, percent);

                            if (infile == NULL)
                              YYABORT;

                            VECTOR_INITIALIZE_EMPTY (empty);
                            VECTOR_INITIALIZE (outfiles);
                            VECTOR_PUSH (outfiles, out);
                            VECTOR_INITIALIZE (in);
                            VECTOR_PUSH (in, infile);
                            rule.name = $name;
                            rule.in = in;
                            rule.out = out;
                            VECTOR_INITIALIZE (apply_rules);
                            VECTOR_PUSH (apply_rules, rule);

                            skeleton = target_skeleton_create (&context->skeletons,
                                                               &empty,
                                                               &in,
                                                               &outfiles,
                                                               &apply_rules);

                            if (UNLIKELY (!target_create (NULL,
                                                          &context->targets,
                                                          out,
                                                          skeleton,
                                                          yyget_extra (yyscanner)->qs)))
                              YYABORT;
                          }
                      }

                    VECTOR_FINALIZE ($strings);
                  }
                ;

strings_duplicate:
                value
                  {
                    VECTOR_DUPLICATE ($$, $1.array);
                  }
        |       '[' strings ']'
                  {
                    $$ = $strings;
                  }
                ;

map_strings:    strings_duplicate '.' MAP '(' map_in ARROW map_out ')'
                  {
                    struct pattern pattern = pattern_from_string ($map_in);
                    struct uniqstr_array map;

                    VECTOR_INITIALIZE_EMPTY (map);

                    for (size_t i = 0; i < VECTOR_SIZE ($strings_duplicate); i++)
                      {
                        uniqstr percent;

                        if (pattern_match (&pattern, VECTOR_AT ($strings_duplicate, i), &percent))
                          {
                            uniqstr string = replace_percent ($map_out, percent);

                            if (string == NULL)
                              YYABORT;

                            VECTOR_PUSH (map, string);
                          }
                      }

                    VECTOR_FINALIZE ($strings_duplicate);
                    $$ = map;
                  }
                ;

filter_strings:
                strings_duplicate '.' FILTER '(' STRING ')'
                  {
                    struct pattern pattern = pattern_from_string ($STRING);
                    struct uniqstr_array result;

                    VECTOR_INITIALIZE_EMPTY (result);

                    for (size_t i = 0; i < VECTOR_SIZE ($strings_duplicate); i++)
                      {
                        uniqstr percent;

                        if (pattern_match (&pattern, VECTOR_AT ($strings_duplicate, i), &percent))
                          VECTOR_PUSH (result, VECTOR_AT ($strings_duplicate, i));
                      }

                    VECTOR_FINALIZE ($strings_duplicate);
                    $$ = result;
                  }
                ;

%%

static inline const char *
error_format_string (int argc)
{
  switch (argc)
    {
    default: /* Avoid compiler warnings.  */
    case 0: return _("%@: syntax error");
    case 1: return _("%@: syntax error: unexpected %u");
      /* TRANSLATORS: '%@' is a location in a file, '%u' is an
         "unexpected token", and '%0e', '%1e'... are expected tokens
         at this point.

         For instance on the expression "1 + * 2", you would get

         '1.5: syntax error: expected - or ( or number or function
         or variable before *'.  */
    case 2: return _("%@: syntax error: expected %0e before %u");
    case 3: return _("%@: syntax error: expected %0e or %1e before %u");
    case 4: return _("%@: syntax error: expected %0e or %1e or %2e before %u");
    case 5: return _("%@: syntax error: expected %0e or %1e or %2e or %3e before %u");
    case 6: return _("%@: syntax error: expected %0e or %1e or %2e or %3e or %4e before %u");
    case 7: return _("%@: syntax error: expected %0e or %1e or %2e or %3e or %4e or %5e before %u");
    case 8: return _("%@: syntax error: expected %0e or %1e or %2e or %3e or %4e or %5e etc., before %u");
    }
}

/* Get line with number "number".
   line_offset (if not equal -1) is an
   offset of '\n' character before line we need.  */
static inline char *
getlinebynumber (FILE *in, int number, file_offset_t line_offset)
{
  file_offset_t old_offset = file_get_offset (in);
  char *line = NULL;
  size_t size = 0;
  int current = 1;

  if (UNLIKELY (old_offset < 0))
    return NULL;

  if (UNLIKELY (file_set_offset (in, line_offset >= 0 ? line_offset : 0, SEEK_SET) < 0))
    return NULL;

  if (UNLIKELY (line_offset < 0))
    {
      /* Find required line by ourselves.  */
      int c = '\0';

      while (current != number && (c = getc (in)) != EOF)
        if (c == '\n')
          current++;

      if (UNLIKELY (c == EOF))
        return NULL;
    }

  if (UNLIKELY (getline (&line, &size, in) < 0))
    return NULL;

  if (UNLIKELY (file_set_offset (in, old_offset, SEEK_SET) < 0))
    {
      free (line);
      return NULL;
    }

  return line;
}

/* Return the number of digits of 'x'
   when converted to string in radix 10.  */
static inline NODISCARD int CONST
int_width (int x)
{
  assert (x >= 0);

  if (x < 10) return 1;
  if (x < 100) return 2;
  if (x < 1000) return 3;
  if (x < 10000) return 4;
  if (x < 100000) return 5;
  if (x < 1000000) return 6;
  if (x < 10000000) return 7;
  if (x < 100000000) return 8;
  if (x < 1000000000) return 9;
  static_assert (1000000000 < INT_MAX
              && 10000000000u > INT_MAX,
                 "int is not 32-bit");
  return 10;
}

static int
yyreport_syntax_error (const yypcontext_t *yyctx,
                       yyscan_t yyscanner,
                       UNUSED struct parse_context *context)
{
  enum { ARGS_MAX = 6 };
  yysymbol_kind_t arg[ARGS_MAX];
  int argsize = yypcontext_expected_tokens (yyctx, arg, ARGS_MAX);
  YYLTYPE *yylocation = yypcontext_location (yyctx);
  char *line;
  bool too_many_expected_tokens;

  if (UNLIKELY (argsize < 0))
    return argsize;

  too_many_expected_tokens = argsize == 0 && arg[0] != YYSYMBOL_YYEMPTY;

  if (UNLIKELY (too_many_expected_tokens))
    argsize = ARGS_MAX;

  const char *format = error_format_string (1 + argsize + (too_many_expected_tokens ? 1 : 0));
  const char *p = format;
  const char *s = format;
  size_t n = strlen (format);
  const char *const end = format + n;

  while ((s = memchr (s, '%', n)) != NULL)
    {
      if (LIKELY (s != p))
        fwrite (p, sizeof (char), (size_t) (s - p), stderr);

      s++;

      if (s[0] == '@')
        { /* %@: location.  */
          yylocation_print (stderr, yylocation);
          s += 1;
        }
      else if (s[0] == 'u')
        { /* %u: unexpected token.  */
          fputs (yysymbol_name (yypcontext_token (yyctx)), stderr);
          s += 1;
        }
      else if (s[0] >= '0' && s[0] <= '9' && s[1] == 'e')
        { /* %0e, %1e...: expected token.  */
          if (((unsigned int) s[0] - '0') < (unsigned int) argsize)
            warning (_("expected token index out of the range: %d"), s[0] - '0');
          else
            {
              fputs (yysymbol_name (arg[s[0] - '0']), stderr);
              s += 2;
            }
        }

      n -= (size_t) (s - p);
      p = s;
    }

  if (p != end)
    fwrite (p, sizeof (char), (size_t) (end - p), stderr);

  fputc ('\n', stderr);

  /* Quote the source line.  */
  line = getlinebynumber (yyget_in (yyscanner), yylocation->first_line,
                          yylocation->newline_offset);

  if (LIKELY (line != NULL))
    {
      const int width = int_width (yylocation->first_line);
      const int offset = yylocation->first_column;

      fprintf (stderr, "  %*d | %s", width, yylocation->first_line, line);
      fprintf (stderr, "  %*s | %*s", width, "", offset, "^");
      free (line);

      for (int i = yylocation->last_column - offset - 1; i > 0; i--)
        fputc ('~', stderr);

      fputc ('\n', stderr);
    }

  return 0;
}
