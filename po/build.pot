# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the build package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: build 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-11 11:49+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/build.c:77
#, c-format
msgid "%s: this variable name is reserved"
msgstr ""

#: src/build.c:97 src/build.c:597
#, c-format
msgid "variable %s is not defined"
msgstr ""

#: src/build.c:160
#, c-format
msgid "redefinition of target %s"
msgstr ""

#: src/build.c:166
#, c-format
msgid "target %s depends on files, but no rules provided"
msgstr ""

#: src/build.c:202
#, c-format
msgid "redefinition of rule %s"
msgstr ""

#: src/build.c:790
#, c-format
msgid "failed to execute %s"
msgstr ""

#: src/build.c:796
#, c-format
msgid "execution failed of %s"
msgstr ""

#: src/build.c:824
#, c-format
msgid "can not find rule %s"
msgstr ""

#: src/build.c:860
#, c-format
msgid "%s: failed to parse rule description"
msgstr ""

#: src/build.c:952
#, c-format
msgid "there is no %s target"
msgstr ""

#: src/build.c:958
#, c-format
msgid "dependence loop detected: %s depends on itself"
msgstr ""

#: src/build.c:1047
#, c-format
msgid "target %s must have created file %s"
msgstr ""

#: src/build.c:1066
#, c-format
msgid "nothing to be done for %s"
msgstr ""

#: src/build.c:1074
#, c-format
msgid "%s: was successfully built"
msgstr ""

#: src/build.c:1079
#, c-format
msgid "%s: building failed"
msgstr ""

#: src/build.c:1180
#, c-format
msgid "number is too big: %s"
msgstr ""

#: src/build.c:1191
#, c-format
msgid "failed to parse number, invalid string: %s"
msgstr ""

#: src/build.c:1262
#, c-format
msgid "failed to initialize flex scanner"
msgstr ""

#: src/build.c:1270
#, c-format
msgid "%s: can not open file: %s"
msgstr ""

#: src/build.c:1291
#, c-format
msgid "%s: parsing failed"
msgstr ""

#: src/build.c:1346
#, c-format
msgid "failed to set base directory for text domain"
msgstr ""

#: src/build.c:1349
#, c-format
msgid "failed to set current message domain"
msgstr ""

#: src/build.c:1366
#, c-format
msgid "failed to change directory to %s: %s"
msgstr ""

#: src/build.c:1377
#, c-format
msgid "-r option is useless with -g option"
msgstr ""

#: src/build.c:1380
#, c-format
msgid "-d option is useless with -g option"
msgstr ""

#: src/build.c:1411
#, c-format
msgid "failed to create build tree"
msgstr ""

#: src/build.c:1432
#, c-format
msgid "failed to build"
msgstr ""

#: src/cli-option-parser.c:258 src/cli-option-parser.c:282
#, c-format
msgid "option `%s' is too long"
msgstr ""

#: src/cli-option-parser.c:302
#, c-format
msgid "option `%s' is ambiguous (could be `--%s' or `--%s')"
msgstr ""

#: src/cli-option-parser.c:326
#, c-format
msgid "invalid option -- `-%c'"
msgstr ""

#: src/cli-option-parser.c:377
#, c-format
msgid "argument required for option `--%s'"
msgstr ""

#: src/cli-option-parser.c:379
#, c-format
msgid "argument required for option `-%c'"
msgstr ""

#: src/diagnostic.c:97
msgid "bug"
msgstr ""

#: src/diagnostic.c:98
msgid "fatal"
msgstr ""

#: src/diagnostic.c:99 src/parse.c:781
msgid "error"
msgstr ""

#: src/diagnostic.c:100
msgid "warning"
msgstr ""

#: src/diagnostic.c:101
msgid "info"
msgstr ""

#: src/libquote/quotearg.c:418
msgid "`"
msgstr ""

#: src/libquote/quotearg.c:419
msgid "'"
msgstr ""

#: src/parse.c:781
msgid "end of input"
msgstr ""

#: src/parse.c:781
msgid "invalid token"
msgstr ""

#: src/parse.c:781
msgid "string"
msgstr ""

#: src/parse.c:782
msgid "command"
msgstr ""

#: src/parse.c:782
msgid "name"
msgstr ""

#: src/parse.c:782
msgid "value"
msgstr ""

#: src/parse.c:784
msgid "'description' keyword"
msgstr ""

#: src/parse.c:784
msgid "'let' keyword"
msgstr ""

#: src/parse.c:784
msgid "'rule' keyword"
msgstr ""

#: src/parse.c:785
msgid "'default' keyword"
msgstr ""

#: src/parse.c:785
msgid "'file' keyword"
msgstr ""

#: src/parse.c:785
msgid "'target' keyword"
msgstr ""

#: src/parse.c:786
msgid "'directory' keyword"
msgstr ""

#: src/parse.c:786
msgid "'filter' keyword"
msgstr ""

#: src/parse.c:786
msgid "'map' keyword"
msgstr ""

#: src/parse.c:981
msgid "syntax error: cannot back up"
msgstr ""

#: src/parse.c:1707 src/parse.c:2761
msgid "memory exhausted"
msgstr ""

#: src/parse.y:202
#, c-format
msgid "variable is not declared: %s"
msgstr ""

#: src/parse.y:262
#, c-format
msgid "expected percent sign in string: %s"
msgstr ""

#: src/parse.y:280
#, c-format
msgid "too many percent signs in string: %s"
msgstr ""

#: src/parse.y:416
#, c-format
msgid "default target name is already specified"
msgstr ""

#: src/parse.y:438
#, c-format
msgid "unexpected string array, expecting string"
msgstr ""

#: src/parse.y:965
msgid "%@: syntax error"
msgstr ""

#: src/parse.y:966
msgid "%@: syntax error: unexpected %u"
msgstr ""

#: src/parse.y:975
msgid "%@: syntax error: expected %0e before %u"
msgstr ""

#: src/parse.y:976
msgid "%@: syntax error: expected %0e or %1e before %u"
msgstr ""

#: src/parse.y:977
msgid "%@: syntax error: expected %0e or %1e or %2e before %u"
msgstr ""

#: src/parse.y:978
msgid "%@: syntax error: expected %0e or %1e or %2e or %3e before %u"
msgstr ""

#: src/parse.y:979
msgid "%@: syntax error: expected %0e or %1e or %2e or %3e or %4e before %u"
msgstr ""

#: src/parse.y:980
msgid "%@: syntax error: expected %0e or %1e or %2e or %3e or %4e or %5e before %u"
msgstr ""

#: src/parse.y:981
msgid "%@: syntax error: expected %0e or %1e or %2e or %3e or %4e or %5e etc., before %u"
msgstr ""

#: src/parse.y:1095
#, c-format
msgid "expected token index out of the range: %d"
msgstr ""

#: src/scan.c:4786 src/scan.c:4796 src/scan.l:233
msgid "input in flex scanner failed"
msgstr ""

#: src/scan.c:5257
msgid "fatal flex scanner internal error--no action found"
msgstr ""

#: src/scan.c:5280
msgid "fatal flex scanner internal error--end of buffer missed"
msgstr ""

#: src/scan.c:5349
msgid "fatal error - scanner input buffer overflow"
msgstr ""

#: src/scan.c:5393
msgid "out of dynamic memory in yy_get_next_buffer()"
msgstr ""

#: src/scan.c:5617 src/scan.c:5626
msgid "out of dynamic memory in yy_create_buffer()"
msgstr ""

#: src/scan.c:5785 src/scan.c:5805
msgid "out of dynamic memory in yyensure_buffer_stack()"
msgstr ""

#: src/scan.c:5908
msgid "yyset_column called with no buffer"
msgstr ""

#: src/scan.l:156
#, c-format
msgid "end of file encountered in the comment"
msgstr ""

#: src/scan.l:203
#, c-format
msgid "unexpected token: %s"
msgstr ""

#: src/scan.l:211
#, c-format
msgid "unexpected invalid UTF-8 token: %s"
msgstr ""

#: src/scan.l:217
msgid "flex scanner jammed"
msgstr ""

#: src/scan.l:248
#, c-format
msgid "failed to get offset of the newline: %s"
msgstr ""

#: src/thread.c:58 src/thread.c:66
#, c-format
msgid "failed to create thread: %s"
msgstr ""

#: src/thread.c:80 src/thread.c:93
#, c-format
msgid "failed to join thread: %s"
msgstr ""

#: src/thread.c:108
#, c-format
msgid "failed to create mutex: %s"
msgstr ""

#: src/thread.c:119
#, c-format
msgid "failed to acquire mutex: %s"
msgstr ""

#: src/thread.c:130
#, c-format
msgid "failed to release mutex: %s"
msgstr ""

#: src/thread.c:152
#, c-format
msgid "failed to create condition variable: %s"
msgstr ""

#: src/thread.c:162 src/thread.c:165
#, c-format
msgid "failed to wait condition variable: %s"
msgstr ""

#: src/thread.c:176 src/thread.c:187
#, c-format
msgid "failed to signal condition variable: %s"
msgstr ""

#: src/thread.c:198
#, c-format
msgid "failed to destroy condition variable: %s"
msgstr ""

#: src/thread.c:309 src/thread.c:312
#, c-format
msgid "failed to create semaphore: %s"
msgstr ""

#: src/thread.c:321 src/thread.c:325
#, c-format
msgid "failed to wait semaphore: %s"
msgstr ""

#: src/thread.c:334 src/thread.c:337
#, c-format
msgid "failed to post semaphore: %s"
msgstr ""

#: src/thread.c:348
#, c-format
msgid "failed to destroy semaphore: %s"
msgstr ""

#: src/thread.c:360
#, c-format
msgid "failed to create read-write lock: %s"
msgstr ""

#: src/thread.c:371
#, c-format
msgid "failed to acquire for reading read-write lock: %s"
msgstr ""

#: src/thread.c:382 src/thread.c:404
#, c-format
msgid "failed to release read-write lock: %s"
msgstr ""

#: src/thread.c:393
#, c-format
msgid "failed to acquire for writing read-write lock: %s"
msgstr ""

#: src/thread.c:415
#, c-format
msgid "failed to destroy read-write lock: %s"
msgstr ""

#: src/utils.c:105 src/utils.c:116
#, c-format
msgid "%s: can not get modification time: %s"
msgstr ""

#: src/yy.c:53
#, c-format
msgid "expected UTF-8 continuation byte (got 0x%X)"
msgstr ""

#: src/yy.c:65
#, c-format
msgid "string is too short"
msgstr ""

#: src/yy.c:143
#, c-format
msgid "invalid UTF-8 start byte (0x%X)"
msgstr ""
